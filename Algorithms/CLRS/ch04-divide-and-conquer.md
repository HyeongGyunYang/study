- Method
- Recurrences
- Technicalities in recurrences

## 4.0. problem sources

- kakao fest
- kakao blind
- scpc
- baek joon
- top coder / code force

## 4.1. The maximum-subarray problem

### 4.1.1 A brute-force solution

### 4.1.2 A transformation

### 4.1.3 A solution using divide-and-conquer

- find-max-crossing-subarray
  - [python code](ch04/find-max-crossing-subarray.py)
  - [c++ code](ch04/find-max-crossing-subarray.cpp)
- find maximum subarray
  - [python code](ch04/find-maximum-subarray.py)
  - [c++ code](ch04/find-maximum-subarray.cpp)

### 4.1.4 Analyzing the divide-and-conquer algorithm

### 4.1.5 Exercises

#### 4.1-1 what does *findMaximumSubarray* return when all elements of *array* are negative?

- min(array)

#### 4.1-2 Write pseudocode for the brute-force method of solving the maximumSubarray problem.

- [python code](ch04/brute-force.py)

#### 4.1-3 Implement : brute-force and recursive algorithm

#### 4.1-4 change condition : allow empty subarray

- init from Ninf to 0

#### 4.1-5 NonRecursion and Linear algorithm

- [python DP](ch04/subarrayDP.py)

## 4.2. Strassen's algorithm for matrix multiplication

- square matrix multiply
  - [python code](ch04/square-matrix-multiply.py)

### 4.2.1 A simple divide-and-conquer algorithm

- square matrix multiply recursive
  - [python code](ch04/square-matrix-multiply-recursive.py)
    - 뭔가 괴랄한 코드가 나왔다 : 동작 안할 거 같은데, 일단 고치기 귀찮으니 패스. 어차피 재귀라 동작 안함ㅋ

### 4.2.2 [Strassen's method](https://ko.wikipedia.org/wiki/%EC%8A%88%ED%8A%B8%EB%9D%BC%EC%84%BC_%EC%95%8C%EA%B3%A0%EB%A6%AC%EC%A6%98)

1. divide the input matrices and output matrix C into $`n/2 \times n/2`$ sub-matrices
  $`\Theta(1)`$
2. Create 10 matrices S1 to S10 each of which is $`n/2 \times n/2`$ and
  is the sum or difference of two matrices created in step 1
  $`\Theta(n^2)`$
  - $`S_1 = B_{12} - B_{22}`$ ... bla bla ... $`S_{10} = B_{11}+B_{12}`$
3. recursively compute seven matrix products P1 to P7.
  Each matrix Pi is $`n/2\times n/2`$
  - $`P_1 = A_{11}\cdot S_1 = A_{11}\cdot - A_{11}\cdot B_{22}`$ ... bla bla
    $`P_7 = S_9\cdot S_10 = A_{11}\cdot B_{11}+A_{11}\cdot B_{12} - A_{21}\cdot B_{11} - A_{21}\cdot B_{12}`$
4. Compute C11 to C22 of the result matrix C by adding and subtracting various combinations of the Pi matrices.

### 4.2.3 Exercises

#### 4.2-1 Use Strassen's algorithm

- do it urself

#### 4.2-2 write pseudocode for Strassen's algorithm

- do it urself

#### 4.2-3 how modify if n by b matrix in which n is not power of 2

- padding 0

#### 4.2-4 compare running time btween large and small

- if small, do normal way

#### 4.2-5 omg

#### 4.2-6 $`kn\times n matrix by n\times kn matrix`$

- i don't know

#### 4.2-7 show complex multipy ($`(a+bi)\times (c+di)`$) using only 3 multiplications

- 뭐 계산해보면 나옴 just calculating!

## 4.3. The substitution method for solving recurrences

- ***Subtitution method***
  1. Guess the form of the solution
  2. Use mathematical induction to find the constants and show that the solution works

### 4.3.1 Making a good guess

### 4.3.2 Subtleties

### 4.3.3 Avoiding pitfalls

### 4.3.4 Changing variables

### 4.3.5 Exercises

#### 4.3-1 show $`T(n) = T(n-1) + n`$ is $`O(n^2)`$

- $`O(n)`$ 아닌가?

#### 4.3-2

이하는 굳이 풀 필요가

## 4. The recursion-tree method for solving recurrences

### 4.1 Exercises

## 5. The master method for solving recurrences

### 5.1 The master theorem

- ***Master Theorem***
  - condition : $`a \ge 1, b>1, f(n), T(n) be defined nonnegative integers`$
  - $`T(n) = aT(n/b)+f(n)`$
  - $`T(n)`$ following asymptotic bounds
    1. If $`f(n)=O(n^{\log_b a-\epsilon}), \epsilon >0, then, T(n) = \Theta(n^{\log_b a})`$
    2. If $`f(n)=\Theta(n^{\log_b a}), then, T(n)=\Theta(n^{\log_b a} \lg n)`$
    3. If $`f(n) = \Omega(n^{\log_b a+\epsilon}), \epsilon >0, af(n/b)\le cf(n),c<1, then, T(n)=\Theta(f(n))`$

### 5.2 Using the master method

### 5.3 Exercises

## 6. \*Proof of the master theorem

## 7. Problems
