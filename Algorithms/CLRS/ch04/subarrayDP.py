rawData = [...] # any list
def subArray(rawData):
    diffData = [0]
    for i,d in enumerate(rawData[1:]):
        diffData.append(rawData[i]-d)
    length = len(rawData)
    # below : DP algorithm
    dp = [0 for i in range(length)]
    for i in range(length-2,-1,-1):
        dp[i] = max(dp[i+1]+diffData[i+1],0)
    return max(dp)
'''
if u need more information such as start and end dates,
it has to be modified.
'''
