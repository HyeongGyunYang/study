import sys

def findMaxCrossingSubarray(array, low, mid, high):
    leftSum = rightSum = -1 * sys.maxsize
    sum = 0
    for i in range(mid, low-1, -1):
        sum += array[i]
        if sum > leftSum:
            leftSum = sum
            maxLeft = i
    for j in range(mid+1, high+1):
        sum += array[j]
        if sum > rightSum:
            rightSum = sum
            maxRight = j
    return maxLeft, maxRight, leftSum+rightSum
