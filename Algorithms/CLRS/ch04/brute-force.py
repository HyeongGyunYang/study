import sys
def bruteForce(array):
    gsum = -1 * sys.maxsize
    for s, start in enumerate(array):
        lsum = start
        for e, end in enumerate(array[s+1:]):
            lsum += end
            if lsum > gsum :
                gsum = lsum
                gright = s+e+1
                gleft = s
    return gsum, gright, gleft
