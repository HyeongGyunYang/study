def findMaximumSubarray(array, low, high):
    if high == low : return low, high, array[low]
    else:
        mid = (low+high)//2
        ret = []
        ret.append(findMaximumSubarray(array, low, mid))
        ret.append(findMaximumSubarray(array, mid+1, high))
        ret.append(findMaxCrossingSubarray(array, low, mid, high))
        return max(ret, key=lambda k:k[2])
