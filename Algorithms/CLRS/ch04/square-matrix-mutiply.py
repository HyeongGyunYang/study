def squareMatrixMultiply(mat1, mat2):
    n = len(mat1)
    c = [[0 for j in range(n)] for i in range(n)]
    for i in range(n):
        for j in range(n):
            for k in range(n):
                c[i][j] = mat1[i][k]*mat2[k][j]
    return c
