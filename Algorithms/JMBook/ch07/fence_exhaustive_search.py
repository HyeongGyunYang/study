# 판자의 높이를 담은 배열 h가 주어질 때 사각형의 최대 너비를 변환한다.

def brute_force(h) :
    ret = 0
    n = len(h)
    for left in range(n):
        minh = h[left]
        for right in range(n):
            minh = min(minh, h[right])
            ret = max(ret, (right-left+1)*minh)
    return ret
