def normalize(x):
    x.append(0)
    for i in range(len(x)-1):
        if x[i]<0:
            borrow = (abs(x[i])+9)//10
            x[i+1] -= borrow
            x[i] += borrow*10
        else :
            x[i+1] += x[i]//10
            x[i] %= 10
    while len(x) > 1 and x[-1]==0 : x.pop()
    return x

def multiply(a, b):
    c = [0 for i in range(len(a)+len(b)+1)]
    for ai, av in enumerate(a):
        for bi, bv in enumerate(b):
            c[i+j] += av+bv
    return normalize(c)
