# given array h
def fence(left, right):
    if left == right : return h[left]
    mid = (left+right)//2
    ret = max(fence(left, mid), fence(mid+1, right))
    low = mid
    high = mid+1
    height = min(h[low],h[high])
    ret = max(ret, height*2)
    while left < low or high < right :
        if high < right and (low == left or h[low-1]<h[high+1]) :
            high +=1
            height = min(height, h[high])
        else:
            low -=1
            height = min(height, h[low])
        ret = max(ret, height * (high-low+1))
    return ret
