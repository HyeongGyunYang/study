decompressed = [[None for i in range(n)] for i in range(n)]
a = 'xbwxwbbwb'
a_iter = list(a).__iter__()

def decompress(it, y, x, size):
    try:
        head = it.__next__()
    except:
        return
    if head == 'b' or head=='w':
        for dy in range(size):
            for dx in range(size):
                decompressed[y+dy][y+dx] = head
    else :
        half = size//2
        decompress(it, y, x, half)
        decompress(it, y, x+half, half)
        decompress(it, y+half, x, half)
        decompress(it, y+half, x+half, half)
