# n
# triangle
cache = [[0]*n]*n
def path(y, x):
    if y == (n-1) : return triangle[y][x]
    if cache[y][x] > 0: return cache[y][x]
    if x == (n-1) : return path(y+1, x)+triangle[y][x]
    return max(path(y+1, x), path(y+1, x+1))+triangle[y][x]
