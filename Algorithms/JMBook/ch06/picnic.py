import copy

c = 1
'''
2 1
0 1
4 6
0 1 1 2 2 3 3 0 0 2 1 3
6 10
0 1 0 2 1 2 1 3 1 4 2 3 2 4 3 4 3 5 4 5
'''
def pair(visit):
    if sum(visit)==n: return 1
    ret = 0
    for i in range(n):
        if not visit[i]:
            first = i
            break
    for i in range(first+1,n):
        if not visit[i] and friend[first][i]:
            temp = copy.deepcopy(visit)
            temp[first] = temp[i] = True
            ret += pair(temp)
    return ret

for i in range(c):
    n = 6
    m = 10
    a = list(map(int, '0 1 0 2 1 2 1 3 1 4 2 3 2 4 3 4 3 5 4 5'.split()))
    friend = [list(False for j in range(n)) for i in range(n)]
    for i in range(0,len(a),2): 
        friend[a[i]][a[i+1]] = friend[a[i+1]][a[i]] = True
    visit = list(False for i in range(n))
    print(pair(visit))
