import copy

h = 8
w = 10
a = ['##########',
     '#........#',
     '#........#',
     '#........#',
     '#........#',
     '#........#',
     '#........#',
     '##########']

row = [[0,0,1], [0,0,1], [0,1,1], [0,1,1]]
col = [[0,1,1], [0,1,0], [0,0,1], [0,0,-1]]

black = [[j=='#' for j in list(i)] for i in a]

def color(x):
    k = sum(list(sum(i) for i in x))
    if k == h*w: return 1
    if (h*w - k) % 3 !=0 : return 0
    ret = 0
    for i in range(h):
        for j in range(w):
            if not x[i][j] :
                z = i
                y = j
                break
    for p in range(4):
        temp = copy.deepcopy(x)
        for q in range(3):
            temp[z+row[p][q]][y+col[p][q]] = True
        ret += color(temp)
    return ret

print(color(black))
