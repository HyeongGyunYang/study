p = q = 2
a = [['N','N','N','N','S'],
     ['N','E','E','E','N'],
     ['N','E','Y','E','N'],
     ['N','E','E','E','N'],
     ['N','N','N','N','N']]
cor = list("YES")

row = [-1,-1,-1,0,1,1,1,0]
col = [-1,0,1,1,1,0,-1,-1]

def boggle(r,c):
    if len(cor)==0: return True
    if cor.pop(0) != a[r][c] : return False
    for i in range(8):
        tr = r+row[i]
        tc = c+col[i]
        if boggle(tr, tc) : return True

print(boggle(p,q))
