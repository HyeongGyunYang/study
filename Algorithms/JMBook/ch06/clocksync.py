import sys, copy

a = [[0,1,2],
     [3,7,9,11],
     [4,10,14,15],
     [0,4,5,6,7],
     [6,7,8,10,12],
     [0,2,14,15],
     [3,14,15],
     [4,5,7,14,15],
     [1,2,3,4,5],
     [3,4,5,9,13]]

c = '12 6 6 6 6 6 12 12 12 12 12 12 12 12 12 12'
c = list(map(int, c.split()))

b = [[False for j in range(16)] for i in range(10)]
for i in a:
    for j in i:
        b[i][j] = True

inf = sys.maxsize

def click(switch, clock):
    for i in b[switch]:
        if i:
            if clock[i] == 12: clock[i] = 3
            else: clock[i]+=3
    return clock

def isit(x):
    for i in x:
        if i!=12: return False
    return True

def search(clock, switch):
    if switch == switches : return 0 if isit(clock) else inf
    ret = inf
    for i in range(4):
        ret = min(ret, i+solve(clock, switch+1))
        clock = click(clock,switch)
    return ret
