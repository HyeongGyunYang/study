Part 3 알고리즘 설계 패러다임
=============================

Chapter 06 무식하게 풀기
------------------------

-	알고리즘 설계는 한순간의 영감보다 여러 전략적인 선택에 의해 좌우된다.
-	해결할 문제의 특성을 이해, time/space complexity의 상충 관계, 적절한 자료구조
-	알고리즘 설계 패러다임: 주어진 문제를 해결하기 위해 알고리즘이 채택한 전략이나 관점
	-	패러다임: 과학적 사상과 믿음을 지배하는 이론의 틀, 혹은 결정체

### Section 01 도입

-	가장 많이 하는 실수: 쉬운 문제를 어렵게 풀기
	-	복잡하지만 우아한 답안을 만드려는 욕심에 쉽고 간단하며 틀릴 가능성이 낮은 답안을 간과
-	brute-force: exhaustive search: 사실 컴퓨터의 장점을 가장 잘 사용하는 기법
-	실제 대회에서도 프로그램을 빠르고 정확하게 구현하는 능력을 검증하기 위해 입력의 크기를 작게 제한한 문제들이 흔히 출제된다.
-	완전탐색은 더 빠른 알고리즘의 기반이 되기도 한다.

### Section 02 재귀 호출과 완전 탐색

-	좋은 재귀코드 짜는 습관
	-	잘못된/실패한 사례를 먼저 처리해서 절약하자
	-	입력이 잘못되거나, 범위에서 벗어난 경우도 기저 사례로 사용하여 맨 처음 처리한다.

#### sub-section 02-1 재귀 호출

-	rescursion
-	base case: 더이상 쪼개지지 않는 최소 사례

#### sub-section 02-2 예제: 중첩 반복문 대체하기

#### sub-section 02-3 예제: 보글 게임

-	문제의 어려운 점
	-	다음 글자가 될 수 있는 칸이 여러 개 있을 때, 이 중 어느 글자를 선택해야 할지 미리 알 수 없다.
-	문제의 분할
	-	각 글자를 하나의 조각으로
	-	첫글자를 찾으면, 인접칸이 다음 글자와 같은지 화인
	-	반복
-	기저 사례의 선택
	-	위치의 글자가 원하는 글자가 아닌 경우 -> 실패
	-	원하는 글자일 경우 -> 성공
	-	입력이 범위를 벗어났을 경우
-	<a href="ch06/boggle.py">Implementation</a>

#### sub-section 02-4 시간복잡도 분석

-	후보의 최대 수를 세어본다.

#### sub-section 02-5 완전 탐색 레시피

-	최대 입력 시, 최악의 수행시간을 확인해서 사용 가능한지 판단.
-	단계를 쪼갠다
-	답을 선택하고, recursion
-	기저 사례를 제대로 설정

#### sub-section 02-6 이론적 배경: 재귀 호출과 부분 문제

-	problem
-	subproblem
-	recursion = 수행해야할 작업 + 그 작업을 적용할 자료

### Section 03 문제: 소풍

-	<a href="ch06/picnic.py">Implementation</a>

### Section 04 풀이: 소풍

#### sub-section 04-1 완전탐색

-	가장 중요한 아이디어는 친구쌍을 받았을 때, 전체 array로 만든다는 것.
-	그리고, 현재 친구쌍 조합에 선택되었는지 아닌지를 판별하는 taken list를 사용해서 for문을 돌리는 것.
-	전체 문제를 n/2개의 조각으로 나눈다.
	-	한 조각마다 두 학생을 짝지어 준다: 전체 nC2는 n(n-1)/2인데??
-	아직 짝을 찾지 못한 학생들의 명단이 주어질 때 친구끼리 둘씩 짝을 짓는 경우
-	recursion

#### sub-section 04-2 중복으로 세는 문제

-	같은 답 중, 사전순으로 올바른 것만 센다.
-	각 단계에 남아 있는 학생들 중 가장 번호가 빠른 학생의 짝을 찾아준다.

#### sub-section 04-3 답의 수의 상한

-	n!

### Section 05 문제: 게임판 덮기

-	<a href="ch06/boardcover.py">Implementation</a>

### Section 06 풀이: 게임판 덮기

-	애초에 블럭의 넓이가 3이므로, 흰칸이 3의 배수가 아니면 채우기 불가능
-	블럭을 내려놓고, 남는 곳에 블럭을 내려놓는 방식을 recursion구현
-	중복을 방지하기 위해 내려놓는 순서를 강제할 필요가 있음 -> 비어있는 곳 중 좌상을 우선으로 내려놓기
-	한 번 블록을 내려놓을 때, 4가지 방법으로 내려놓을 수 있는데, 이걸 함수로 구현하는 게 아니라 상대좌표를 array로 저장하자.
-	덮었던 블록을 치워야한다!! 그러므로 겹쳐 덮더라도 일단 덮고, 치울 때 빼주자!

### Section 07 최적화 문제

#### sub-section 07-1 예제: 여행하는 외판원 문제 Traveling Salesman Problem, TSP

-	<a href="ch06/traveling_salesman.py">Implementation</a>

#### sub-section 07-2 무식하게 풀 수 있을까?

-	완전 탐색으로 시간 안에 풀 수 있는가?
-	(n-1)!

#### sub-section 07-3 재귀 호출을 통한 답안 생성

-	n 조각내서 recursion

### Section 08 문제: 시계 맞추기

-	<a href="ch06/clocksync.py">Implementation</a>

### Section 09 풀이: 시계 맞추기

#### sub-section 09-1. 문제 변형하기

-	스위치를 누르는 순서는 중요하지 않다.
-	어떤 스위치건 4번 누르면 원래대로 돌아옴으로, 4번 이상 누를 필요가 없다.

#### sub-section 09-2. 완전 탐색 구현하기

-	문제를 10조각으로 나눈 뒤, recursion
-	10중 for 중첩문이랑 같지만, recursion이 구현이 쉽고 디버깅이 편하다.
-	어떤 스위치가 어떤 시계에 연결되어 있는지 2D-array로 구현

### Section 10 많이 등장하는 완전 탐색 유형

#### sub-section 10-1. 모든 순열 만들기

-	완전 탐색의 경우 O(N!): n>10이면 제 시간 안에 수행하기 힘들다
-	표준 라이브러리에서 permutation을 지원할 수도

#### sub-section 10-2. 모든 조합 만들기

-	combination

#### sub-section 10-3. 2^n가지 경우의 수 만들기

-	yes/no로 n개의 질문에 대답하기
