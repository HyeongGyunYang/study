### Section 1 Intro

-	각개 격파
-	재귀와 다른 점
	-	재귀 : 한 조각 + 나머지 전체
	-	분할정복 : 반 + 반
-	풀이 단계
	-	divide : 문제를 더 작은 문제로 분할하는 과정
	-	merge : 각 문제에 대해 구한 답을 원래 문제에 대한 답으로 병합하는 과정
	-	base case : 더이상 답을 분할하지 않고 곧장 풀 수 있는 매우 작은 문제
-	문제의 특성
	-	문제를 둘 이상의 부분 문제로 나누는 자연스러운 방법이 있어야 한다.
	-	부분 문제의 답을 조합해 원래 문제의 답을 계산하는 효율적인 방법이 있어야 한다.
-	장점 : 같은 작업을 더 빠르게 처리해준다.

#### Section 1-1. Eg) 수열의 빠른 합

-	1~n의 합 재귀보다 빠르게 구하기
	-	<a href="ch07/fastsum.py">Implementation</a>
-	시간 복잡도 분석
	-	재귀는 $n$번 호출됨
	-	fastsum은 $lgN$

#### Section 1-2. Eg) 행렬의 빠른 제곱

-	Core Idea : $A^m = A^{m/2} \times A^{m/2}$
-	<a href="ch07/square_matrix.py">Implementation</a>
-	나누어 떨어지지 않을 때의 분할과 시간 복잡도
	-	$n*n$행렬을 $m$번 곱할 때: $O(n^3m)$
	-	홀수일 때 1과 나머지 전체로 나누는게 반반에 가깝게 나누는 것보다 빠르다.
		-	왜냐면, 반반에 가깝게 나누면 중복호출되는 함수들이 존재하기 때문에
		-	overlapping subproblems라고 부른다.
		-	이 문제에 DP가 고안됨

#### Section 1-3. Eg) 병합 정렬과 퀵 정렬

-	시간이 오래 걸리는 정렬을 분할 단계에서 수행하느냐, 병합 단계에서 수행하느냐의 차이.

#### Section 1-4. Eg) 카라츠바의 빠른 곱셈 알고리즘

-	<a href="ch07/multiply.py">기존의 곱셈 알고리즘</a>
-	Core Idea :
	-	$a = a*{1} \times 10^{m/2} + a*{0} \\ b = b*{1} \times 10^{m/2} + b*{0} \\ a \times b = a*{1} \times b*{1} \times 10^{m/2} + (a*{1} \times b*{0} + a*{0} \times b*{1}) \times 10^{m/2} + a*{0} \times b*{0} \\ (a*{0} + a*{1}) \times (b*{0}+b*{1}) = a*{0} \times b*{0} + a*{1} \times b*{0}+a*{0} \times b*{1}+a*{1} \times b*{1}$
	-	<pre><code> z2 = a1 * b1 z0 = a0 * b0 z1 = (a0 + a1) * (b0 + b1) - z0 - z2</code></pre>
-	귀찮아서 구현하지 않는다. 솔직히 무슨 의미가 있나 싶다.
-	$O(n^{\log 3})$

### Section 2 Quad tree

-	quad tree : 대량의 좌표 데이터를 메모리 안에 압축해 저장하는 기법
	-	주어진 공간을 4개로 분할해 재귀적으로 표현
	-	흑백 그림을 압축해 표현할 때 유용
-	무식하게 풀기 : 주어진 그림의 쿼드 트리의 압축을 풀어서 실제 이미지를 얻고 상하 반전한 뒤 다시 쿼드 트리 압축하는 것
-	두 가지 접근법
	-	큰 입력에 대해서도 동작하는 효율적인 알고리즘을 처음부터 새로 만들기
	-	\*작은 입력에 대해서만 동작하는 단순한 알고리즘으로부터 시작해서 최적화해 나가기
-	작은 입력에서 동작하는 풀이
	-	쿼드 트리 압축 풀기
	-	압축 문자열 분할하기
	-	<a href="ch07/quad_tree1.py">작은 입력에서 동작</a>
-	압축 다 풀지 않고 뒤집기
	-	Core Idea : 전체가 검정이나 하양이면 뒤집던지 말던지 같다.
	-	<a href="ch07/quad_tree2.py">Sol) quad tree</a>
-	$O(n)$

### Section 3 Fence

-	<a href="ch07/fence_exhaustive_search.py">exhaustive search (brute force)</a>
	-	$O(n^2)$
-	분할 정복 알고리즘의 설계
	-	n개의 판자를 절반으로 나눠 세 개의 부분 문제를 만든다
	-	왼쪽, 오른쪽, 둘에 걸쳐있는 사각형 중 가장 큰 크기의 사각형을 구하자
-	양쪽 부분 문제에 걸친 경우의 답
	-	가운데 두 개의 판자는 항상 포함하므로, 맞닿아 있는 두 개의 판자 중 큰 쪽으로 확장해 나가면서 크기를 비교
-	<a href="ch07/fence_divide_and_conquer.py">divide and conquer</a>
-	$O(n\log{n})$
-	15.11의 스위핑 기법, 19.3의 스택을 결합한 선형시간 알고리즘, 25의 상호 배타적 집합

### Section 4 Fanmeeting

-	두 큰 수의 곱셈으로 이 문제를 바꾼다.
-	0, 1로 표현하면 곱으로 알 수 있음
-	<a href="ch07/fanmeeting.py">카라츠바 알고리즘을 이용한 구현</a>
