'''
n
triangle
'''
cache = [[0]*n]*n
def path(y,x,sum):
    if y==(n-1) : return sum + triangle[y][x]
    ret = cache[y][x][sum]
    if ret>0 : return ret
    sum += triangle[y][x]
    ret = max(path(y+1, x+1, sum), path(y+1, x, sum))
    return ret
