cache = [[None] * 101] * 101
# 패턴 W, 파일명 S
def match(w, s):
    # 패턴 W와 파일명 S가 일치하는지 알려주는 함수
    # memoization 적용
    ret = cache[w][s]
    if ret != None : return ret
    # match 여부 확인 : match 시에 다음 글자를 검사
    while s < len(S) and w < len(W) and (W[w]=='?' or W[w]==S[s]):
        w+=1
        s+=1
    # match하지 않을 경우 :
    if w == len(W): # 패턴이 끝났을 경우, 파일명도 검사가 끝나 있으면 true를 반환
        cache[w][s] = (s==len(S))
        return s==len(S)
    if W[w]==' * ': # 패턴이 *일 경우, *에 해당하는 부분을 skip한 뒤 재귀호출
        for skip in range(len(S)-s+1):
            if match(w+1, s+skip):
                cache[w][s] = True
                return True
            else: cache[w][s]=False
    cache[w][s]=False
    return False
