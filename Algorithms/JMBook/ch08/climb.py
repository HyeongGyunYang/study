n
m
cache = [[-1] * (2*n+1)]*(m+1)
def climb(days, climbed):
    if days == m : return (1 if climbed >= n else 0)
    if cache[days][climbed] != -1 : return cache[days][climbed]
    cache[days][climbed] = climb(days+1, climbed+1)+climb(days+1, climbed+2)
    return cache[days][climbed]
