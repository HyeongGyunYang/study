'''
n = len(A)
m = len(B)
'''
import sys
ninf = -1 * sys.maxsize
cache = [[-1] * (m+1)] * (n+1)
def jlis(ai, bi):
    if cache[ai][bi] != -1 : return cache[ai][bi]
    cache[ai][bi] = 2
    maximum = max(A[ai],B[bi])
    for an in range(ai+1, n):
        if maximum < A[an]: cache[ai][bi] = max(cache[ai][bi], jlis(an, bi)+1)
    for bn in range(bi+1, m):
        if maximum < B[bn]: cache[ai][bi] = max(cache[ai][bi], jlis(ai, bn)+1)
    return cache[ai][bi]
