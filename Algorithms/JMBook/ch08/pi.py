import sys
n = list(map(int,list(input())))
inf = sys.maxsize
def classify(a, b):
    m = n[a:b]
    if m == ('m[0]'*len(m)) : return 1
    progressive = True
    for i in range(len(m)-1):
        if (m[i+1] - m[i]) != m[1]-m[0] :
            progressive = False
            break
    if progressive:
        if abs(m[1]-m[0]) == 1: return 2
        return 5
    alternating = True
    for i in range(len(m)):
        if m[i] != m[i%2] :
            alternating = False
            break
    if alternating : return 4
    return 10

cache = [-1]*len(n)
def memo(begin):
    if begin == len(n) : return 0
    if cache[begin] != -1 : return cache[begin]
    ret = inf
    for l in range(3,6):
        if begin+l <= len(n):
            ret = min(ret, memo(begin+l)+ classifiy(begin, begin+l))
    return ret
