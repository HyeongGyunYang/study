def lis(a):
    ret = 0
    for i in range(len(a)):
        b = list()
        for j in range(i+1, len(a)):
            if a[i] < a[j]: b.append(a[j])
        ret = max(ret, 1+lis(b))
    return ret
