cache = [[None] * 101] * 101
# 패턴 W, 파일명 S
def match(w, s):
    # 패턴 W와 파일명 S가 일치하는지 알려주는 함수
    # memoization 적용
    ret = cache[w][s]
    if ret != None : return ret
    # match 여부 확인 : match 시에 다음 글자를 검사
    # 이부분에서 반복을 없애고 메모이제이션을 추가로 적용
    if s < len(S) and w < len(W) and (W[w]=='?' or W[w]==S[s]):
        cache[w][s] = match(w+1,s+1)
        return cache[w][s]
    # match하지 않을 경우 :
    if w == len(W): # 패턴이 끝났을 경우, 파일명도 검사가 끝나 있으면 true를 반환
        cache[w][s] = (s==len(S))
        return s==len(S)
    if W[w]==' * ': # 패턴이 *일 경우, *에 해당하는 부분을 skip한 뒤 재귀호출
    '''
    이부분을 발전시킴 : 몇 글자가 대응되어야 하는지를
    아무 글자도 대응하지 않을지, 아니면 한글자를 더 대응시킬지를 결정한다.
    '''
        if match(w+1, s) or (s<len() and match(w, s+1)):
            cache[w][s] = True
            return True
        else: cache[w][s]=False
    cache[w][s]=False
    return False
