n = int(input())
cache = [[None] * n] * n
def jump2(y, x):
    if y>=n or x>=n : return 0
    if y==(n-1) and x==(n-1) : return 1
    ret = cache[y][x]
    if ret!=None : return ret
    jumpSize = board[y][x]
    return ret = (jump2(y+jumpSize, x) or jump2(y, x+jumpSize))
