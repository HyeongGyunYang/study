import sys
n, s = map(int,input())
a = list(map(int, input().split()))
inf = sys.maxsize
psum = [a[0]]*n
pSqSum = [a[0]**2]*n

a.sort()
for i in range(1, n):
    psum[i] = psum[i-1] + a[i]
    pSqSum[i] = pSqSum[i-1] + a[i]**2

def min_error(low, high):
    # a[low:high+1]를 하나의 숫자로 표현할 때 최소 오차 합
    Sum = psum[high] - (0 if low==0 else psum[low-1])
    sqSum = pSqSum[high] - (0 if low==0 else pSqSum[low-1])
    m = int(0.5+sum/(high-low+1))
    ret = sqSum-2*m*Sum+m*m*(high-low+1)
    return ret

cache = [[-1]*n]*s
def quantize(from, parts):
    if from == n : return 0 # base case : 모든 숫자를 다 양자화
    if parts == 0 : return inf # base case : 숫자가 남았는데 양자화 불가
    if cache[from][parts] != -1 : return cache[from][parts]
    ret = inf
    for part_size in range(1,n+1-from):
        ret = min(ret, min_error(from, from+part_size-1) + quantize(from+part_size, parts-1))
    cache[from][parts] = ret
    return ret
