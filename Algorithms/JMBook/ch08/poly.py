n = 100
mod = 10*1000*1000
cache = [[-1]*n]*n
def poly(n, first):
    if n == first : return 1
    if cache[n][first] != -1 : return cache[n][first]
    ret = 0
    for second in range(1, n-first):
        add = second+first-1
        add *= poly(n-first, second)
        add %= mod
        ret += add
        ret %= mod
    cache[n][first] = ret
    return ret
