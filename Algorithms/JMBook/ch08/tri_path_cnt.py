n = 100
triangle

cache = [[0]*n]*n
def path(y, x):
    if y == (n-1) : return triangle[y][x]
    if ret > 0: return cache[y][x]
    if x == (n-1) : return path(y+1, x)+triangle[y][x]
    return max(path(y+1, x), path(y+1, x+1))+triangle[y][x]

cache_cnt = [[-1]*n]*n
def count(y,x):
    if y == (n-1) : return 1
    if cache_cnt[y][x] != -1 : return cache_cnt[y][x]
    cache_cnt[y][x] = 0
    if path(y+1, x+1) >= path(y+1, x) : cache_cnt[y][x] += count(y+1, x+1)
    if path(y+1, x+1) <= path(y+1, x) : cache_cnt[y][x] += count(y+1, x)
    return cache_cnt[y][x]
