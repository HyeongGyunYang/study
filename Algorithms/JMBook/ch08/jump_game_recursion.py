n = int(input())
board = [[None] * n] * n
def jump(y,x):
    if y>=n or x>=n: return False
    if y==(n-1) and x==(n-1): return True
    jumpSize = board[y][x]
    return jump(y+jumpSize, x) or jump(y, x+jumpSize)
