n = 100
mod = 10**10+7
cache = [-1]*n
def tiling(width):
    if width <= 1 : return 1
    if cache[width] != -1 : return cache[width]
    cache[width] = (tiling(width-2)+tiling(width-1))%mod
    return cache[width]

cache2 = [-1]*n
def asy(width):
    if width <= 2 : return 0
    if cache2[width] != -1 : return cache2[width]
    ret = asy(width-2) % mod
    ret = (ret+asy(width-4)) % mod
    ret = (ret+tiling(width-3)) % mod
    ret = (ret+tiling(width-3)) % mod
    cache2[width] = ret
    return ret
