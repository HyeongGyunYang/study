# n
# s
cache = [-1] * n
def lis(start):
    # s[start]에서 시작하는 증가 부분 수열 중 최대 길이를 반환한다.
    if cache[start] != -1 : return cache[start]
    cache[start] = 1
    for next in range(start+1, n):
        if s[start] < s[next] :
            cache[start] = max(cache[start], lis(next)+1)
    return cache[start]
