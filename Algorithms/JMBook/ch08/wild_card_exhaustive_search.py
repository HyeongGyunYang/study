def match(w, s):
    # 패턴 w가 문자열 s에 대응되는지 여부를 반환하는 함수
    pos = 0
    while pos<len(s) and pos<len(w) and (w[pos]=='?' or w[pos]==s[pos]):
        pos+=1
    if pos==len(w): return pos==len(s)
    if w[pos]=='*':
        for skip in range(len(s)-pos):
            if match(w[pos+1:], s[pos+skip:]): return True
    return False
