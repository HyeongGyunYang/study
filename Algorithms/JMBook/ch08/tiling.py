n = 100
mod = 10**10+7
cache = [-1]*n
def tiling(width):
    if width <= 1 : return 1
    if cache[width] != -1 : return cache[width]
    cache[width] = (tiling(width-2)+tiling(width-1))%mod
    return cache[width]
