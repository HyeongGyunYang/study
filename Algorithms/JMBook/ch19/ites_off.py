def simple(signals, k):
    ret = 0
    for head in range(len(signals)):
        sum = 0
        for tail in range(head, len(signals)):
            sum += signals[tail]
            if sum == k: ret += 1
            if sum >=k: break
    return ret
