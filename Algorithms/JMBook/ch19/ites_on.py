from collections import deque
# a[0] = 1983
# a[i] = (a[i-1]*214013+2531011)%(2**32)
def cnt_ranges(k, n):
    ran = deque()
    ret = 0
    rsum = 0
    for i in range(n):
        new_signal = a[i]
        rsum += new_signal
        ran.push(new_signal)
        while rsum > k:
            rsum -= ran.popleft()
        if rsum == k: ret+=1
    return ret
