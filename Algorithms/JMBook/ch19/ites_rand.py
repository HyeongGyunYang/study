import random
# a[0] = 1983
# a[i] = (a[i-1]*214013+2531011)%(2**32)
class rng:
    def __init__(self):
        seed = 1983
    def next():
        self.seed = ((self.seed*214013+2531011)%(2**32))%10001
        return self.seed
