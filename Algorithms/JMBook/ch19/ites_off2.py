def opt(signals, k):
    ret = tail = 0
    rsum = signals[0]
    for head in range(len(signals)):
        while rsum <k and (tail+1)<len(signals):
            tail +=1
            rsum += signals[tail]
        if rsum ==k: ret+=1
        rsum -= signals[head]
    return ret
