b = '({}[(){})]'
s = list()
m = set(['(','{','['])
n = {'(':')','{':'}','[':']'}
r = True
for i in b:
    if i in m: s.append(i)
    elif n[s.pop()] != i :
        r = False
        break
if len(s) != 0: r = False
print(r)
