Algorithmic Problem Solving Strategies
======================================

Part 3 알고리즘 설계 패러다임
-----------------------------

chapter 08 Dynamic Programming
------------------------------

### section 01 도입

#### section 1-1. 중복되는 부분 문제

-	처음 주어진 문제를 더 작은 문제로 나눈 뒤, 각 조각의 답을 계산하고, 이 답들로부터 원래 문제에 대한 답을 계산

-	DP에서 특정 부분 문제가 여러 번 사용될 수 있기 때문에 재활용한다.

-	cache : 부분 문제의 연산 결과를 저장해두는 메모리

-	overlapping subproblems : 두 번 이상 계산되는 부분 문제

-	combinatorial explosion : divide and recursion을 할 때, 분할이 깊어 질수록 중복 연산 수가 지수적으로 증가

#### section 1-2. Memoization을 적용할 수 있는 경우

-	Memoization: 함수 결과를 저장하는 장소를 마련해 두고, 한 번 계산한 값을 저장해 뒀다 재활용

-	referential transparency : 함수 return 값이 input으로만 결정되는 경우

-	memoization은 referential transparency에만 적용 가능

-	자신이 가장 좋다고 생각하는 방식 하나를 정해 일관되게 사용하는 것이 좋다.

#### section 1-3. memoization 구현 패턴

<a href="ch08/memoization.py">memoization 사용의 예</a>

1.	base case : out of range 등을 미리 처리하면 당해 오류가 발생 X

2.	init: 가능하지 않은 함수 반환 값으로 초기화한다.

3.	return이 cache에 대한 reference 형태이다

4.	cache 초기화를 어떻게 할 것인가: c++에서 memset을 이용해서

#### section 1-4. memoization의 big-O 분석

-	(주먹구구) 존재하는 부분 문제의 수 * 한 부분 문제를 풀 때 필요한 반복문의 수행 횟수

#### section 1-5. eg) 외발 뛰기

-	recursion
	-	재귀적으로 해결하는 완전 탐색 알고리즘을 만든다.
	-	<a href="ch08/jump_game_recursion.py">외발 뛰기 재귀적 풀이</a>
-	memoization 적용
	-	원하는 답은 없는데 전체 답의 갯수는 너무 많을 때, reference transparency면 중복을 없앨 수 있다.
	-	<a href="ch08/jump_game_memoization.py">외발 뛰기 메모이제이션 적용</a>
-	다른 해법: graph
-	DP recipe
	1.	완전 탐색
	2.	중복된 부분을 memoization
-	반복적 DP -> ch09.21

### section 2 와일드 카드

#### section 2-1. \* 가 문제

-	몇 글자나 오는지 알 수가 없다.
-	이럴 때 완전탐색이지~
	1.	패턴을 \* 를 기준으로 쪼갠다.
	2.	첫 조각부터 만족하는 조각을 검색한다.
	3.	각 만족하는 조각들을 두번째... 검사한다.
-	<a href="ch08/wild_card_exhaustive_search.py">wild card exhaustive search</a>

#### section 2-2. 중복되는 문제

-	입력으로 주어질 수 있는 패턴 W의 부분 문자열 w와 파일명 S의 s는 제한되어 있다.
-	recursion 때 w,s의 앞에서만 글자를 떼어내기 때문에 w,s는 항상 입력에 주어진 패턴 W와 파일명 S의 접미사가 된다.
-	따라서 w,s는 각각 최대 101개 밖에 되지 않는다.
-	<a href="ch08/wild_card_memoization.py">wild card memoization</a>
-	$O(n^3)$

#### section 2-3. 다른 분해 방법

-	<a href="ch08/wild_card_advanced.py">wild card 발전된 방식</a>

### section 3 전통적 최적화 문제들

-	DP가 원래는 최적화 문제를 풀기 위해 고안
-	그래서 특정 조건을 만족시키면 memoization보다 더 효율적인 방법으로 해결 가능

#### section 3-1. EG) 삼각형 위의 최대 경로

-	완전 탐색으로 시작하기

	-	경로를 각 가로줄로 조각 낸 뒤, 각 조각에서 아래로 내려갈지, 오른쪽으로 내려갈지를 선택하면서 모든 경로를 만들자
	-	재귀함수에는 현재 위치와 지금까지 만난 숫자들의 합을 전달한다

<pre><code>
path(y,x,sum) = max(path(y+1,x,sum+triangle[y][x]), path(y+1, x+1, sum+triangle[y][x]))
</code></pre>

-	무식하게 memoization 적용

	-	<a href="ch08/triangle_path_memoization.py">triangle path memoization</a>
	-	단점
		-	메모 배열의 크기가 입력의 크기에 종속적이라서 너무 크다
		-	특정 입력에 대해 완탐처럼 동작함

-	입력 걸러내기

	-	재귀함수 입력을 두 종류로 나눈다.
		-	y, x : 재귀호출이 풀어야할 부분 문제를 지정한다.
		-	sum : 지금까지 어떤 경로로 이 부분 문제에 도달했는지를 나타낸다.
	-	리턴 값을 전체 경로의 최대치 -> 부분 경로의 최대치로 변경
	-	<a href="triangle_path_dp.py">triangle path DP</a>

<pre><code>
path(y,x) = triangle[y][x] + max(path(y+1, x), path(y+1, x+1))
</code></pre>

#### section 3-2. 이론적 배경: 최적 부분 구조

-	Optial substructure : 어떤 경로로 부분 문제에 도달했던지에 상관없이 남은 부분 문제를 최적으로 풀면 된다.

#### section 3-3. eg) 최대 증가 부분 수열

-	완전 탐색에서 시작하기
	-	숫자를 하나씩 조각낸 뒤, 한 조각에서 숫자 하나씩을 선택하는 완전 탐색 알고리즘을 만들어보
	-	<a href="ch08/longest_increasing_subsequence_exhaustive.py">Logest Increasing Sub-sequence exhaustive</a>
-	입력 손보기
	-	a를 더 간단하게 만들어 보자 : a는 다음과 같이 나타낼 수 있다.
		-	원래 주어진 수열 s
		-	원래 주어진 수열의 원소 s[i]에 대해, s[i+1] 부분 수열에서 s[i]보다 큰 수들만을 포함하는 부분 수열
	-	<a href="ch08/longest_increasing_subsequence_2.py">Logest Increasing Sub-sequence 2</a>
-	시작 위치 고정하기
	-	???
-	더 빠른 해법
	-	텅 빈 수열에서 시작해 숫자를 하나씩 추가해 나가며 각 길이를 갖는 증가 수열 중 가장 마지막 수가 작은 것은 무엇인지를 추적한다.
	-	$C[i] =$ 지금까지 만든 부분 배열이 갖는 길이 i인 증가 부분 수열 중 최소의 마지막 값

#### section 3-4. 최적화 문제 DP recipe

-	모든 답을 만들어 보고 그 중 최적해를 반환하는 완전 탐색 알고리즘
-	전체 답의 점수가 아니라 앞으로 남은 선택들에 해당하는 점수만을 반환
-	재귀호출 입력에 이전까지의 선택에 관한 정보가 있다면, 꼭 필요한 것만 남기고 줄인다.
-	입력이 배열이거나 문자열인 경우 가능하다면 적절한 변환을 통해 메모를 하도록
-	메모 시작

### section 4 합친 LIS

-	탐욕법으로는 안된다.
-	비슷한 문제를 풀어 본 적이 있군요
	-	lis(start) =s[start]에서 시작하는 최대 증가 부분 수열의 길이
	-	jlis(ai, bi) =a[ai]와 b[bi]에서 시작하는 합친 증가 부분 수열의 최대 길이
	-	$jlis(ai, bi) = max(max*{nextA \in NEXT(ai)}jlis(nextA, bi)+1, max*{nextB\in NEXT(bi)}jlis(ai, nextB)+1)$
-	<a href="ch08/joined_lis.py">Joind LIS</a>

### section 5 원주율 외우기

-	완탐 불가
-	메모 : min(길이가 3, 4, 5인 조각의 난이도 + 나머지 수열의 난이도)
-	<a href="ch08/pi.py">PI</a>

### section 6 Quantization

-	quantization : 더 넓은 범위를 갖는 값들을 작은 범위를 갖는 값들로 근사해 표현함으로써 자료를 손실 압축하는 과정
-	하던 대로는 안된다
	-	최적 부분 조건이 성립하지 않는다.
	-	quantize(a, u) = u가 지금까지 한 번 이상 사용한 숫자들의 집합일 때, a에 속한 수를 양자화해서 얻을 수 있는 최소 오차 제곱의 합
-	답의 형태 제한하기
	-	주어진 수열을 s개의 묶음으로 나누는 작업
-	한 개의 구간에 대한 답 찾기
	-	주어진 구간을 어떤 수로 표현해야 할지 결정하기
	-	결정한 수 m으로 해당 구간을 표현했을 때 오차를 계산하기
	-	$\sum*{i=a}^b (A[i]-m)^2 = (b-a+1) \times m^2 - 2 \times (\sum*{i=a}^b A[i]) \times m + \sum_{i=a}^b (A[i]^2)$
-	<a href="ch08/quantization.py">Quantization</a>

### section 7 경우의 수와 확률

-	오버플로에 유의하기
	-	14.8의 modular operation에 대해 알아두기
-	EG) 타일링 방법의 수 세기
	-	부분 문제 분할의 조건
		-	분할된 부분 문제가 나머지 경우를 모두 포함한다
		-	분할된 부분 문제 외의 다른 경우는 없다.
	-	<a href="ch08/tiling.py">Tiling</a>
-	EG) 삼각형 위의 최대 경로 개수 세기
	-	<a href="ch08/tri_path_cnt.py">Tri_Path_cnt</a>
-	EG) 우물을 기어오르는 달팽이
	-	경우의 수로 확률 계산하기
		-	우물을 끝까지 기어오르는 경우 / 전체 경우
	-	완전 탐색 알고리즘
		-	<a href="ch08/climb.py">Climb</a>
-	EG) 장마가 찾아왔다
	-	부분문제에 확률을 적용한다.
-	경우의 수 계산하기 레시피
	-	완탐 설계 : 두 가지 조건을 만족해야 함
	-	재귀 함수에 이전 입력을 최소화
	-	메모

### section 8 비대칭 타일

-	완전 탐색의 함정 : <a href="ch08/asym_tiling.py">Asym Tiling</a>
-	직접 비대칭 타일링의 수 세기 : <a href="ch08/asym_tiling2.py">Asym Tiling 2</a>
-	스캐폴딩으로 테스트하기
	-	입력을 생성하기 쉽고
	-	느리지만 정답임을 확실히 알 수 있는 알고리즘이 존재한다.

### section 9 폴리오미노

-	완탐
	-	어떤 순서대로 정사각형들을 붙여나갈지가 중요하다.
	-	<a href="ch08/poly.py">Poly</a>

### section 10 두니발 박사의 탈옥

-	일단 손으로 풀어보면서 감을 익히기
-	완전 탐색에서 시작해보자
-	메모하기
-	조건부 확률
-	구현
-	반대 방향에서 풀기
-	이론적 배경 : 마르코프 연쇄
	-	유한 개의 상태가 있다
	-	매 시간마다 상태가 변경된다
	-	어떤 상태 a에서 다른 상태 b로 옮겨갈 확률은 현재 상태 a에만 좌우된다. a 이전에 어느 상태에 있었는지, 현재 시간은 얼마인지 등은 영향을 주지 않는다.
