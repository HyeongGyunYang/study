def postOrder(preorder, inorder):
    if len(preorder)==0: return
    root = preorder[0]
    rootIndex = inorder.index(root)
    postOrder(preorder[1:rootIndex+1], inorder[:rootIndex]) #left
    postOrder(preorder[rootIndex+1:], inorder[rootIndex+1:]) #right
    print(root, end=" ")

preO = [27,16,9,12,54,36,72]
inO = [9,12,16,27,36,54,72]
postOrder(preO, inO)
