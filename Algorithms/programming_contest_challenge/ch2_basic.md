Programming Contest Challenge
=============================

Chapter 2 Basic
---------------

### Section 1 complete search

#### Section 1-1. recursion

#### Section 1-2. Stack

#### Section 1-3. Queue

#### Section 1-4. Depth First Search

-	EG) Subset Sum Problem

	-	Q: 정수 n개, 이 중 몇 개의 수를 골라 그 합이 k가 될 수 있는지 판단.
	-	<a href="ch2/section1/DFS_subset_sum_problem.py">Sol) Subset Sum Problem</a>

-	EG) Lake Counting (POJ no.2386)

	-	<a href="ch2/section1/DFS_lake_counting.py">Sol) Lake Counting</a>

#### Section 1-5. Breadth First Search

-	EG) 미로 최단거리
	-	<a href="ch2/section1/BFS_shortest_mase.py">Sol) 미로 최단거리</a>

#### Section 1-6. 특수한 상태의 열거

#### Section 1-7. 가지치기

### Section 2 Greedy Algorithm

-	Core Idea : '하나의 룰'을 가지고 '현재 상황'에서의 최선을 선택하는 것을 반복
-	해가 구해지는 탐욕알고리즘이 존재한다면 보통 성능이 좋다.

-	EG) 구간 스케줄링 문제

	-	idea
	-	0번 선택할 수 있는 강의 중에 시작시간이 가장 빠른 것을 선택
	-	1번* 선택할 수 있는 강의 중에 종료시간이 가장 빠른 것을 선택하는 것을 반복
	-	2번 선택할 수 있는 강의 중에 시간이 가장 짧은 것을 선택하는 것을 반복
	-	3번 선택할 수 있는 강의 중에 한 강의를 선택했을 경우, 선택할 수 없는 다른 강의의 수가 가장 적어지는 것을 선택하는 것을 반복
	-	<a href="ch2/section2/scheduling.py">Sol) 구간 스케줄링 문제</a>

-	EG) Best Cow Line (POJ #3617)

	-	<a href="ch2/section2/best_cow_line_wrong.py">Wrong Answer</a>
	-	선두와 말미의 글자가 같은 글자라면, 두번째 글자를 비교할 필요가 있다.
		-	이를 극한으로 밀어붙이면 다음과 같은 탐욕알고리즘을 고안할 수 있다.
		-	1단계 S와 S의 반전 문자열S'을 사전순으로 비교
		-	2단계 둘 중 작은 걸 선택해서 선두/말미를 결정
		-	3단계 반복
	-	<a href="ch2/section2/best_cow_line.py">Sol) Best Cow Line</a>

-	EG) Saruman's Army (POJ #3069)

	-	<a href="ch2/section2/sarumans_army.py">Sol) Saruman's Army</a>

-	EG) Fence Repair (POJ #3253)

	-	<a href='ch2/section2/fence_repair.py'>Sol) Fence Repair</a>
	-	0단계 : bin tree의 성질을 이용
	-	1단계 :가장 짧은 널빤지 노드와 그 다음으로 짧은 널빤지의 노드는 형제
	-	2단계 : Implementation
	-	응용 : 하프만 부호

### Section 3 Dynamic Programming

#### Section 3-1. 탐색의 메모화 및 동적 설계법

-	EG) Knapsack problem

	-	Q) 무게 w, 가격v인 n개의 물건. 무게의 총합이 W를 초과하지 않을 때 가격 총합의 최대치
	-	<a href='ch2/section3/knapsack_problem.py'>Sol) Knapsack Problem</a>
	-	<a href='ch2/section3/knapsack_problem_recurrence.py'>Sol) Knapsack Problem : 점화식</a>

-	EG) 갯수 제한이 있는 부분 합

	-	Q) n종류의 수 ai가 각각 mi개씩 있을 때, 이들 중 몇 개를 골라 그 합이 k가 될 수 있는지 판정
	-	<a href="ch2/section3/subsum.py">Sol) 갯수 제한이 있는 부분 합</a>

### Section 4 Data Structure

#### Section 4-1. Priority Queue

-	EG) Expedition (POJ #2431)
	-	<a href='ch2/section4/expedition.py'>Sol) Expedition</a>

### Section 5 Graph

#### Section 5-3. Search Graph

-	EG) Bipartite Graph
	-	<a href='ch2/section5/bipartite_graph.py'>Sol) Bipartite_graph</a>

#### Section 5-4. Shortest Path

-	최단경로
	-	Bellman-Ford Algorithm) 단일 시작점 최단경로 문제 1
		-	<a href='ch2/section5/bellman_ford.py'>Implementation) Bellman-Ford Alogrithm</a>
		-	weight가 negative일 때도 사용가능
	-	Dijkstra Algorithm
		-	<a href='ch2/section5/dijkstra.py'>Implementation) Dijkstra Algorithm</a>
	-	Floyd-Warshall Algorithm) 전체 노드에 대한 최단경로 문제
		-	<a href='ch2/section5/floyd_warshall.py'>Implementation) Floyd-Warshall Algorithm</a>
-	경로 복원
-	최소 전역 트리 MST
	-	Prim
	-	Kruska

#### Section 5-5. exercise

-	Roadblocks

	-	Q) r개의 도로와 n개의 교차로. 1번 교차로에서 n번 교차로로 향하는 2번째 최단경로의 길이
	-	<a href='ch2/section5/roadblocks.py'>Wrong Answer) Roadblocks</a>
	-	Core Idea: 2개의 거리를 memo

-	Conscription

	-	<a href='ch2/section5/conscription.py'>Conscription</a>

-
