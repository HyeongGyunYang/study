from collections import deque

graph = [[1,2],
         [0,2],
         [0,1]]
n = 3
color = list([0])*n

def dfs(row, c):
    color[row] = c
    for i in graph[row]:
        if color[i] == c: return False
        if color[i] == 0 and not dfs(i, -c): return False
    return True

ret = True
for i in range(n):
    if not color[i]: ret *= dfs(i, 1)

if ret: print('YES')
else: print('NO')
