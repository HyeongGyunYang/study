n = 5
m = 5
r = 8

relation = [list([10000])*n for i in range(m)]
for i in range(r):
    x,y,d = map(int,input())
    relation[x][y] -= d

visit = [[False for i in range(m)], [False for i in range(n)]]
previous = [[None for i in range(m)],[None for i in range(n)]]

for i in range(m):
    if not visit[0][i]:
        previous[0][i] = relation[i].index(min(relation[i]))
