import sys
n = 4
r = 4
g = [[(1,100)],
     [(0,100), (2,250), (3,200)],
     [(1,240), (3,100)],
     [(1,200),(2,100)]]
inf = sys.maxsize
d = [list([inf])*n for i in range(n)]
for i, val in enumerate(g):
    for j in val:
        d[i][j[0]] = j[1]

for k in range(n):
    for i in range(n):
        for j in range(n):
            d[i][j] = min(d[i][j], d[i][k]+d[k][j])
