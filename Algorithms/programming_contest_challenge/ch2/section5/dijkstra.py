import sys

'''
graph = 2d-adjacency list
n = number of nodes
s = start point
'''
visit = [False for i in range(n)]
dist = [sys.maxsize for i in range(n)]
# prev = [None for i in range(n)]
dist[s] = 0
# prev[s] = s

for k in range(n):
    m = -1
    min_val = sys.maxsize
    for i in range(n):
        if not visit[i] and dist[i] < min_val:
            min_val = d[i]
            m = i
    visit[m] = True
    for w, wt in g[m]:
        if not visit[w] and dist[m]+wt < dist[w]:
            dist[w] = dist[m] + wt
            # previous[w] = m
