import sys

row_num, col_num = map(int, input())
garden = list(list(i) for i in sys.stdin.read().splitlines())
row_move = [-1,-1,-1,0,0,1,1,1]
col_move = [-1,0,1,-1,1,-1,0,1]

def linked(row, col):
    garden[row][col] = '.'
    for i in range(8):
        temp_row = row+row_move[i]
        temp_col = col+col_move[i]
        if garden[temp_row][temp+col]=='W':
            linked(temp_row, temp_col)
    return True

ret = 0
for i in range(row_num):
    for j in range(col_num):
        if garden[i][j] == 'W': ret+=linked(i,j)
print(ret)
