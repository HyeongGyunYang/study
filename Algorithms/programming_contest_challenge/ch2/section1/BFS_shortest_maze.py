import sys
from collections import deque

row_num, col_num = map(int, input())
maze = list(list(i) for i in sys.stdin.read().splitlines())
move_row = [-1,0,1,0]
move_col = [0,1,0,-1]
inf = row_num*col_num+100
distance = list(list([inf])*col_num for i in range(row_num))

def search(row, col):
    queue = deque()
    distance[row][col] = 0
    queue.append(tuple(row,col))
    while len(queue) != 0:
        curr = queue.leftpop()
        if maze[curr[0]][curr[1]] == 'G':
            return distance[curr[0]][curr[1]]
        for i in range(4):
            next_row = curr[0]+move_row[i]
            next_col = curr[1]+move_col[i]
            if maze[next_row][next_col] !='#'\
            and next_row<row_num and next_row>=0\
            and next_col<col_num and next_col>=0:
                queue.append(tuple(next_row, next_col))
                distance[next_row][next_col] = distance[curr[0]][curr[1]]+1
print(search(0,0))
