n = 4
w = [2,1,3,2]
v = [3,2,4,2]
W=5
a = list(zip(w, v))
dp = list(list([-1])*(n+1) for i in range(W+1))

def rec(i,j):
    if dp[i][j] >= 0: return dp[i][j]
    if i==n: ret = 0
    elif j<w[i]:
        ret = rec(i+1,j)
    else:
        ret = max(rec(i+1,j),rec(i+1, j-w[i])+v[i])
    return dp[i][j] = ret

print(rec(0,W))
