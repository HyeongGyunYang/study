n = 6
s = "ACDBCB"
rev = s[::-1]
ret = ''
for i in range(n):
    if s>rev:
        ret+=s[-1]
        s = s[:-1]
    else:
        ret+=s[0]
        s=s[1:]
    rev = s[::-1]
print(ret)
