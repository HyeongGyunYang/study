n = int(input())
for _ in range(n):
    a = input()
    if a[0]==')' or a[-1]=='(':
        r='NO'
    else:
        l = 0
        r = 0
        for i in a:
            if i=='(':
                l+=1
            else:
                r+=1
                if r>l:
                    r='NO'
                    break
        if l==r:
            r='YES'
        else:
            r='NO'
    print(r)
