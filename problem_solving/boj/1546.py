import sys
a = int(sys.stdin.readline())
b = sys.stdin.readline().split()
b = list(map(int, b))
c = [i/max(b)*100 for i in b]
print(sum(c)/a)
