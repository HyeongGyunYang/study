m,n = map(int,input().split())
ret = [True]*(n+1)
for i in range(2,n+1):
    if ret[i]:
        for j in range(i+i,n+1,i):
            ret[j]=False
        if i>=m: print(i)
