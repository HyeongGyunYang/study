import heapq
from collections import Counter
a = input().upper()
if len(a)==1:
    print(a)
else:
    b = Counter(a)
    c = heapq.nlargest(2, zip(b.values(), b.keys()))
    if c[0][0] == c[1][0]:
        print('?')
    else:
        print(c[0][1])
