n = int(input())
cache = [-1]*n
mod = 10**4+7
def tiling(x):
    if x == n+1 : return 0
    if x == n: return 1
    ret = cache[x]
    if ret != -1 : return ret
    ret = (tiling(x+1)+tiling(x+2)*2)%mod
    cache[x] = ret
    return ret

print(tiling(0))
