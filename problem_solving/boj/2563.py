import sys
tf = list()
for i in range(100):
    tf.append(list([False]*100))
def black(r, c):
    for i in range(r, r+10):
        for j in range(c, c+10):
            if not tf[i][j]: tf[i][j]=True
n = int(sys.stdin.readline().strip())
lst = list([list(map(int,i.split())) for i in sys.stdin.read().splitlines()])
for r, c in lst: black(r,c)
print(sum([sum(i) for i in tf]))
