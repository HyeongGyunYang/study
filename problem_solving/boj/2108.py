import statistics as stat
import sys, heapq
from collections import Counter
_ = int(input())
a = list(map(int, sys.stdin.read().splitlines()))
print(int('{0:.0f}'.format(stat.mean(a))))
print(int(stat.median(a)))
def mode(x):
    counts = Counter(x)
    max_count = max(counts.values())
    return [i for i, count in counts.items() if count == max_count]
b = mode(a)
if len(b)==1:
    print(b[0])
else:
    print(heapq.nsmallest(2,b)[1])
print(max(a)-min(a))
