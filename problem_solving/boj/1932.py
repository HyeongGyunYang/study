import sys
# n, *tri = sys.stdin.read().splitlines()
# n = int(n)
# tri = [list(map(int, i.split())) for i in tri]
n = 5
tri = [[j for j in range(i+1)] for i in range(n)]
cache = [[-1]*n]*n
cache[n-1] = tri[n-1]
print(tri)
print(cache)

def Sum(y,x):
    if y==(n-1): return tri[y][x]
    if cache[y][x] != -1 : return cache[y][x]
    a = Sum(y+1, x)
    b = Sum(y+1, x+1)
    cache[y][x] = max(a, b)+tri[y][x]
    print(y, cache)
    return cache[y][x]

if n==1: print(tri[0][0])
else: print(Sum(0,0))
