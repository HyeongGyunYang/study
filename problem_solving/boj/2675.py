import sys
n = int(sys.stdin.readline())
for _ in range(n):
    a, b = sys.stdin.readline().split()
    a = int(a)
    c = ''
    for i in b:
        c+=i*a
    print(c)
