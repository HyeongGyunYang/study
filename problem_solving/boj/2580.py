import sys
sdoku = [list(map(int,i.split())) for i in sys.stdin.read().splitlines()]
tf = [[i==0 for i in j] for j in sdoku]
all = set(range(1,10))
rows = list()
cols = list()
rects = list()
for i in range(9):
    rows.append(set(sdoku[i]))
    col = list()
    for j in range(9):
        col.append(sdoku[j][i])
    cols.append(set(col))
def rect(r,c):
    rect = list()
    for i in range(3):
        for j in range(3):
            rect.append(sdoku[r+j][c+i])
    rects.append(set(rect))
for i in range(0,7,3):
    for j in range(0,7,3):
        rect(i, j)

def search_rect(r, c):
    tr = r//3
    tc = c//3
    for i in range(3):
        if i==tr:
            for j in range(3):
                if j==tc: return i*3+j
zeros =
while sum([sum(i) for i in tf])>0:
    for i in range(9):
        for j in range(9):
            if tf[i][j]:
                t1 = all-rows[i]
                t2 = all-cols[j]
                t3 = all-rects[search_rect(i,j)]
                t = t1&t2&t3
                if len(t):
                    tf[i][j] = False
                    sdoku[i][j] = list(t)[0]
for i in sdoku:
    for j in i[:-1]:
        print(j,end=' ')
    print(i[-1])
