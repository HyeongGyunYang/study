import sys
n = int(input())
for _ in range(n):
    a = sys.stdin.readline()
    l = len(a)
    if a[0]=='O':
        r = 1
        t = 1
    else:
        r = 0
        t = 0
    for i in range(1,l):
        if a[i]=='O':
            t += 1
            r += t
        else:
            t = 0
    print(r)
