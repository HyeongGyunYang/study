import sys, math
m, n = map(int, sys.stdin.read().splitlines())
r=0
t = int(math.sqrt(m))
if t*t<m: t+=1
for i in range(t, int(math.sqrt(n))+1):
    r+=i*i

if r==0:print(-1)
else:
    print(r)
    print(t*t)
