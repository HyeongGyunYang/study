from itertools import combinations
import sys
lst = sys.stdin.read().splitlines()
lst = list(map(int, lst))
def combination(lst):
    comb = combinations(lst,7)
    for i in comb:
        s = sum(i)
        if s==100: return i
re = sorted(combination(lst))
for i in re:
    print(i)
