import sys
com = int(sys.stdin.readline().strip())
graph = list()
for _ in range(com+1):
    graph.append(list())
edge = int(sys.stdin.readline().strip())
for _ in range(edge):
    a, b = sys.stdin.readline().split()
    graph[int(a)].append(int(b))
    graph[int(b)].append(int(a))

visit = [False]*(com+1)
def dfs(v):
    visit[v] = True
    global re
    re+=1
    for w in graph[v]:
        if not visit[w]:
            dfs(w)
re = 0
dfs(1)
print(re-1)
