import sys
_, *t = sys.stdin.read().splitlines()
for i,val in enumerate(t):
    a,b = map(int, val.split())
    print("Case #{}: ".format(i+1), end="")
    print(a+b)
