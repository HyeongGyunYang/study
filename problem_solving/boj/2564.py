import sys
breadth, length = sys.stdin.readline().split()
breadth=int(breadth)
length=int(length)
total = 2*(breadth+length)
n = int(sys.stdin.readline().strip())
*lst, loc = sys.stdin.read().splitlines()
lst = [tuple(map(int,i.split())) for i in lst]
loc = tuple(map(int,loc.split()))
'''
left
1: north
2: south <- origin == south origin
upper
3: west
4: east
'''
# def newloc(x):
#     t = x[0]
#     if t==1: return list(length, x[1])
#     elif t==2: return list(0, x[1])
#     elif t==3: return list(length-x[1], 0)
#     else: return list(length-x[1], breadth)
# newlst = list()
# for i in lst:
#     newlst.append(newloc(i))
# newloc = newloc(loc)

def dist(x):
    if loc[0]==1:
        if x[0]==1:
            t = loc[1]-x[1]
            if t<0: t += total
        elif x[0]==2:t = loc[1]+length+x[1]
        elif x[0]==3: t = loc[1]+x[1]
        else: t = loc[1]+total/2+length-x[1]
    elif loc[0]==2:
        if x[0]==1: t = breadth-loc[1]+length+breadth-x[1]
        elif x[0]==2:
            t = x[1]-loc[1]
            if t<0: t+=total
        elif x[0]==3: t = breadth-loc[1]+total/2+x[1]
        else: t = breadth-loc[1]+length-x[1]
    elif loc[0]==3:
        if x[0]==1: t = length-loc[1]+total/2+breadth-x[1]
        elif x[0]==2: t = length-loc[1]+x[1]
        elif x[0]==3:
            t = x[1]-loc[1]
            if t<0: t+=total
        else: t = length-loc[1]+breadth+length-x[1]
    else:
        if x[0]==1: t = loc[1]+ breadth-x[1]
        elif x[0]==2:t = loc[1]+total/2+x[1]
        elif x[0]==3:t = loc[1]+breadth+x[1]
        else:
            t = loc[1]-x[1]
            if t<0: t+=total
    if t>=total/2: t=total-t
    return t
newlst = [dist(i) for i in lst]
print(int(sum(newlst)))
