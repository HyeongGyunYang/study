import sys
lst = list(map(int, sys.stdin.read().splitlines()))
lst = [i for i in lst if i%2==1]
if len(lst)==0: print(-1)
else:
    print(sum(lst))
    print(min(lst))
