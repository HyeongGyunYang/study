import sys
from collections import Counter
n, *words = sys.stdin.read().splitlines()
ret = []
for word in words:
    c = Counter(word)
    try:
        ret.index(c)
    except:
        ret.append(c)
print(len(ret))
