from collections import deque
import sys
n, *a = sys.stdin.read().splitlines()
d = deque()
for i in a:
    c, *k = i.split()
    if c=='size': print(len(d))
    elif (c=='pop_front' or c=='pop_back' or c=='front' or c=='back') and len(d)==0: print(-1)
    elif c=='empty':
        if len(d)==0: print(1)
        else: print(0)
    elif c=='push_front':  d.appendleft(int(k[0]))
    elif c=='push_back':  d.append(int(k[0]))
    elif c=='pop_front': print(d.popleft())
    elif c=='pop_back': print(d.pop())
    elif c=='front':
        t = d.popleft()
        print(t)
        d.appendleft(t)
    elif c=='back':
        t = d.pop()
        print(t)
        d.append(t)
