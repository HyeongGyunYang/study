n = input().split()
n = list(map(int, n))
if n[0]<n[1]:
    r = 'ascending'
    for i in range(1,7):
        if n[i]>=n[i+1]:
            r = 'mixed'
            break
else:
    r = 'descending'
    for i in range(1,7):
        if n[i] <=n[i+1]:
            r = 'mixed'
            break
print(r)
