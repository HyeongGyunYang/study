import sys
_, *a = sys.stdin.read().splitlines()
t = [0,1,1,1]+[0]*97
for i in range(4,101):
    t[i] = t[i-2]+t[i-3]
for i in a:
    print(t[int(i)])
