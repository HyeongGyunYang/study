n,k = map(int,input().split())
pascal = [[0 for j in range(k)] for i in range(n)]
pascal[0][0] = 1
for y in range(1,n):
    for x in range(k):
        pascal[y][x] += pascal[y-1][x]
        if x != 0: pascal[y][x] += pascal[y-1][x-1]
print(pascal[-1][k-1])
