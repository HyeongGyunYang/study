import sys
t = int(input())
for i,v in enumerate(sys.stdin.read().splitlines()):
    a,b = map(int, v.split())
    print("Case #{}: {} + {} = {}".format(i+1,a,b,a+b))
