e, s, m = map(int, input().split())
t = [15, 28, 19]
k = t[0]*t[1]*t[2]
y = [[0,0,0] for i in range(k+1)]
for i in range(1,k+1):
    for j in range(3):
        y[i][j] = y[i-1][j]+1
        if y[i][j] == (t[j]+1) : y[i][j] = 1
print(y.index([e,s,m]))
