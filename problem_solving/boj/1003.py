import sys

fibo = [0 for i in range(41)]
fibo[1] = 1
for i in range(2,41):
    fibo[i] = fibo[i-1]+fibo[i-2]

t = int(input())
a = list(map(int, sys.stdin.read().splitlines()))
for i in a:
    if i ==0 : print('1 0')
    elif i==1: print('0 1')
    else: print('{} {}'.format(fibo[i-1], fibo[i]))
