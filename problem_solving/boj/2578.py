import sys
lst = list()
for _ in range(5): lst.append(list(map(int,sys.stdin.readline().split())))

check = list()
for _ in range(5):
    check.append(list([False]*5))

host = list()
for _ in range(5): host+=list(map(int,sys.stdin.readline().split()))

def call(x):
    for i in range(5):
        for j in range(5):
            if lst[i][j]==x: return i, j

def bingo():
    b=0
    for i in range(5):
        if sum(check[i])==5: b+=1
        t = 0
        for j in range(5):t+=check[j][i]
        if t==5: b+=1
    if check[0][0] and check[1][1] and check[2][2] and check[3][3] and check[4][4]: b+=1
    if check[0][4] and check[1][3] and check[2][2] and check[3][1] and check[4][0]: b+=1
    return b

re = 0
for i in host:
    re+=1
    a,b = call(i)
    check[a][b] = True
    if bingo()>2:
        print(re)
        break
