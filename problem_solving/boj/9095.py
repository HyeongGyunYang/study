import sys
_, *a = sys.stdin.read().splitlines()

def comb(x):
    if x==0 : return 1
    if x == 1: return 1
    if x == 2: return 2
    if cache[x] != -1 : return cache[x]
    cache[x] = comb(x-3)+comb(x-2)+comb(x-1)
    return cache[x]

for n in a:
    cache = [-1]*(int(n)+1)
    print(comb(int(n)))
