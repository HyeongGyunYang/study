import sys
_, *a = sys.stdin.read().splitlines()
for i in a:
    h, w, n = map(int, i.split())
    p = n//h+1
    q = n%h
    if q==0 :
        q = h
        p -= 1
    if p<10: print('{}0{}'.format(str(q),str(p)))
    else: print('{}{}'.format(str(q),str(p)))
