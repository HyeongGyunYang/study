import sys
c = int(sys.stdin.readline())
for _ in range(c):
    a, *n = sys.stdin.readline().split()
    a = int(a)
    n = list(map(int, n))
    b = [i>sum(n)/a for i in n]
    print('{0:.3f}%'.format(sum(b)/a*100))
