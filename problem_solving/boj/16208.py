n = int(input())
a = list(map(int, input().split()))
a.sort()
s = sum(a)
r = 0
for i in a[:-1]:
    s-=i
    r+=s*i
print(r)
