class calc(object):
    def __init__(self,express):
        for i,v in enumerate(express):
            try:
                x = int(v)
            except:
                self.operator = v
                t = i
                break
        self.int1 = express[:t]
        self.int2 = express[t+1:]

    def cal(self):
        operator=self.operator
        if operator =='+':return int(self.int1)+int(self.int2)
        if operator == '-':return int(self.int1)-int(self.int2)
        if operator == '*':return int(self.int1)*int(self.int2)
        if operator == '/':return int(self.int1)//int(self.int2)

first = calc(input())
second = calc(first.int2)
first.int2 = second.int1

retFirst = first.cal()
retSecond = second.cal()
first.int2 = retSecond
second.int1 = retFirst
retFirst = first.cal()
retSecond = second.cal()
print(min(retFirst, retSecond))
print(max(retFirst, retSecond))
