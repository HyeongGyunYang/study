n = int(input())
r = 0
def f(i):
    s = str(i)
    r = 1
    for j in range(1,len(s)-1):
        t1 = int(s[j])-int(s[j+1])
        t2 = int(s[j-1])-int(s[j])
        if t1 != t2:
            r = 0
            break
    return r
if n<100:
    r += n
else:
    r += 99
    for i in range(100,n+1):
        if f(i):
            r += 1
print(r)
