import sys
n = int(sys.stdin.readline().strip())
lst = [int(i) for i in sys.stdin.readline().split()]

class sll:
    class node:
        def __init__(self, item, prev, link):
            self.item = item
            self.prev = prev
            self.next = link
    def __init__(self, head):
        n = self.node(head, None, None)
        self.head = n
        self.size = 1
    def insert(self, item, idx):
        n = self.node(item, None, None)
        t = self.head
        if self.size==idx:
            t.prev = n
            n.next = t
            self.head = n
        elif idx==0:
            for i in range(self.size-1): t = t.next
            t.next = n
            n.prev = t
        else:
            for i in range(self.size-idx): t = t.next
            p = t.prev
            n.prev = p
            p.next = n
            t.prev = n
            n.next = t
        self.size+=1
    def print_all(self):
        t = self.head
        for _ in range(self.size):
            print(t.item, end=' ')
            t = t.next

re = sll(1)
for i in range(1,n):
    re.insert(i+1, lst[i])
re.print_all()
