import sys
# map
length, breadth = sys.stdin.readline().split()
length = int(length)
breadth = int(breadth)
Map = sys.stdin.read().splitlines()
Map = [list(map(int,list(i.replace('W','0').replace('L','1')))) for i in Map]
# graph
graph = list()
for _ in range(length*breadth):
    graph.append(list())
for i in range(length):
    for j in range(breadth):
        if Map[i][j]:
            t = i*breadth+j
            if j>0 and Map[i][j-1]: graph[t].append(t-1)
            if j<breadth-1 and Map[i][j+1]: graph[t].append(t+1)
            if i>0 and Map[i-1][j]: graph[t].append(t-breadth)
            if i<length-1 and Map[i+1][j]: graph[t].append(t+breadth)
# search
