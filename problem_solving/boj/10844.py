# n = int(input())
a = list(range(1,10))

def nxt(d):
    t = dict()
    t[0] = d[1]
    for i in range(1,9):
        t[i]=d[i-1]+d[i+1]
    t[9] = d[8]
    return t

for n in a:
    if n==1: ret = 9
    if n>1:
        in_d = {0:0,1:1,2:1,3:1,4:1,5:1,6:1,7:1,8:1,9:1}
        for i in range(n-1):
            in_d = nxt(in_d)
        ret = sum(in_d.values())
    print(n, ':', ret%10**9)
