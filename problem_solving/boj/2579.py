import sys
n, *steps = sys.stdin.read().splitlines()
n= int(n)
steps = list(map(int, steps))
cache = [-1]*(n+1)
ninf = -1 * sys.maxsize

def s(x, t):
    if t >= 3: return ninf
    if x >= n : return ninf
    if t == 2 and x == (n-2) : return ninf
    if x == (n-1) : return steps[-1]
    if cache[x] != -1 : return cache[x]
    cache[x] = max(s(x+1, t+1), s(x+2, 1))+steps[x]
    return cache[x]

print(s(-1, 0)-steps[-1])
