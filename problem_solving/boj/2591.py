import sys
'''
1. 자릿수 단위로 숫자를 받아오기.
2. if 자릿수가 1,2,3이면: 묶는다.
    1. 1,2면
    2. 3이고 다음 자리가 1,2,3,4이면
3. 묶는 경우를 
5. (중복방지)전체 숫자 조합을 가져온 뒤, count해서 key값의 길이를 센다.
'''
n = list(sys.stdin.readline().strip())
print(n)
print(len(n))
array = list()
for i in range(len(n)-1):
    if n[i]=='1' or n[i]=='2' or\
    (n[i]=='3' and (n[i+1]=='1' or n[i+1]=='2' or n[i+1]=='3' or n[i+1]=='4')):
        t = n
        t[i] = t[i]+t[i+1]
        del t[i+1]
        array.append(t)
print(array)
