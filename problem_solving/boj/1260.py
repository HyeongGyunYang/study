import sys
from collections import deque
n, m, v = map(int, input().split())
adj_list = list(list() for i in range(n+1))
take = sys.stdin.read().splitlines()
for i in take:
    p,q = map(int,i.split())
    if q not in adj_list[p] :
        adj_list[p].append(q)
        adj_list[q].append(p)
for i in range(n):
    adj_list[i].sort()
visit = list(False for i in range(n+1))
ret_dfs = list()
ret_bfs = list()
def dfs(v):
    visit[v] = True
    ret_dfs.append(v)
    for i in adj_list[v]:
        if not visit[i] : dfs(i)

def bfs(v):
    q = deque()
    q.append(v)
    visit[v] = True
    ret_bfs.append(v)
    while len(q)!=0:
        t = q.popleft()
        for i in adj_list[t]:
            if not visit[i]:
                visit[i] = True
                ret_bfs.append(i)
                q.append(i)

dfs(v)
for i in ret_dfs:
    print(i, end=" ")
print()
visit = list(False for i in range(n+1))
bfs(v)
for i in ret_bfs:
    print(i, end=" ")
