import sys
temp = list()
stack = list()
brackets = sys.stdin.read()
re = 0
for bracket in brackets:
    if bracket == ')':
        if temp == []:
            re = 0
            break
        t = temp[-1]
        if t=='[':
            re = 0
            break
    elif bracket == ']':
        if temp == []:
            re = 0
            break
        t = temp[-1]
        if t=='(':
            re = 0
            break
    else: stack.append(bracket)
if temp != []: re = 0
print(re)

'''
( temp=1
( temp=2
) temp=1 *2
[ 2
[ 3
] 2 *3
] 1 *3
) 0 
( 1
[ 2 *
] 1
) 0
'''
