import sys
'''
k : 과자 한 개 가격
n : 과자 갯수
m : 현재 가진돈
if 현재 가진돈 < 필요한 돈: 부모님께 돈 받기

'''
k, n, m = map(int, sys.stdin.read().split())
def money(k,n,m):
    a = k*n
    if m<a: return a-m
    else: return 0
print(money(k,n,m))
