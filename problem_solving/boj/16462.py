import sys
n, *scores = sys.stdin.read().splitlines()
n = int(n)
ret = 0
for s in scores:
    temp = list(s)
    for i,v in enumerate(temp):
        if v in ['0','6','9']:
            temp[i] = '9'
    if len(temp)==1: temp = '00'+temp[0]
    elif len(temp)==2: temp = '0'+temp[0]+temp[1]
    elif temp[1:]!=['0','0']: temp='100'
    else : temp = temp[0]+temp[1]+temp[2]
    ret += int(temp)
print('{:0.0f}'.format(ret/n))
