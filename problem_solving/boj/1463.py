import sys
n = int(input())
inf = sys.maxsize
cache = [inf]*(n+1)

def search(x):
    if x == 1 : return 0
    if cache[x] < inf : return cache[x]
    if x%3==0: cache[x] = search(x//3)+1
    if x%2==0: cache[x] = min(cache[x], search(x//2)+1)
    cache[x] = min(cache[x], search(x-1)+1)
    return cache[x]

for i in range(100,n,100):
    search(i)

print(search(n))
