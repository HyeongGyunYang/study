import sys
n,k = map(int, input().split())
coins = list(map(int,sys.stdin.read().splitlines()))
coins.append(-1)
m = n
ret = 0
def bin_search(left, right):
    mid = (left+right)//2
    while (coins[mid]>k or coins[mid+1]<k) or mid<(n-1):
        if coins[mid]<k:
            left = mid
        else:
            right = mid
        mid = (left+right)//2
    return mid

while k>0:
    m = bin_search(0,m)
    while coins[m]<=k:
        k-=coins[m]
        ret+=1
print(ret)
