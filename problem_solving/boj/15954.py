import sys, math
n, k = map(int, sys.stdin.readline().split())
lst = list(map(int, sys.stdin.read().split()))
re = list()
def std(x):
    length = len(x)
    m = sum(x)/length
    return math.sqrt(sum([(i-m)**2 for i in x])/length)
for i in range(n-k+1):
    re.append(std(lst[i:k+i]))
print(min(re))
