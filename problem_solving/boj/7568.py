import sys
n = int(input())
a = list(list(map(int,val.split())) for i,val in enumerate(sys.stdin.read().splitlines()))
for i in range(n):
    rank = 1
    height = a[i][0]
    weight = a[i][1]
    for j in a:
        if j[0]>height and j[1]>weight:rank+=1
    print(rank, end=' ')
