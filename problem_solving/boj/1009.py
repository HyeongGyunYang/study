import sys
_, *a = sys.stdin.read().splitlines()
b = [list() for i in range(10)]
for i in range(10):
    t = i
    while 1:
        b[i].append(t)
        t = int(str(t*i)[-1])
        if t==i: break
for i in a:
    p, q = i.split()
    y = int(p[-1])
    x = int(q)%len(b[y])-1
    if b[y][x]==0 : print(10)
    else: print(b[y][x])
