import sys
a = sys.stdin.readline().strip()
b = sys.stdin.readline().strip()

dict = {'M':1000,'D':500,'C':100,'L':50,'X':10,'V':5,'I':1}
def rome(a):
    s = []
    re = 0
    for i in a:
        if s==[]:
            if i=='I' or i=='X' or i=='C': s.append(i)
            else: re+=dict[i]
        else:
            t = s.pop()
            if t=='I':
                if i=='V': re+=4
                elif i=='X': re+=9
                else: re+=1+1
            elif t=='X':
                if i=='L': re+=40
                elif i=='C': re+=90
                elif i=='I':
                    re+=10
                    s.append(i)
                else: re+=dict[i]+10
            else:
                if i=='D':re+=400
                elif i=='M':re+=900
                elif i=='X':
                    re+=100
                    s.append(i)
                else: re+=dict[i]+100
    if s!=[]: re+=dict[s[0]]
    return re
c = rome(a)+rome(b)

def rome2(c):
    re = ''
    t = c//1000
    if t>0: re+='M'*t
    c -= 1000*t
    t = c//100
    if t==9: re+='CM'
    elif t==8 or t==7 or t==6 or t==5:re+= 'D'+'C'*(t-5)
    elif t==4: re+='CD'
    elif t>0: re+='C'*t
    c-= 100*t
    t=c//10
    if t==9: re+='XC'
    elif t==8 or t==7 or t==6 or t==5:re+= 'L'+'X'*(t-5)
    elif t==4: re+='XL'
    elif t>0: re+='X'*t
    t = c-10*t
    if t==9: re+='IX'
    elif t==8 or t==7 or t==6 or t==5:re+= 'V'+'I'*(t-5)
    elif t==4: re+='IV'
    elif t>0: re+='I'*t
    return re
print(c)
print(rome2(c))
