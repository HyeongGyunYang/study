import sys
t, *nums = sys.stdin.read().splitlines()
def gcd(x,y):
    if y>x : x,y = y,x
    while y>0: x,y=y,x%y
    return x
for i in nums:
    a,b = map(int, i.split())
    print(a*b//gcd(a,b))
