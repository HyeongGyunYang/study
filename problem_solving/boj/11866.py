from sys import stdin
n, m = stdin.readline().strip().split()
n = int(n)
m = int(m)
l = list(range(n+1))
r = []
p=m
while len(r)<n:
    r.append(l[p])
    del l[p]
    p=p-1+m
    q=len(l)
    while q-1<p:
        if q==1:
            break
        p=p-q+1
print('<',end='')
print(r[0],end='')
for i in r[1:]:
    print(', ',end='')
    print(i,end='')
print('>')
