n = int(input())
a = list(map(int, input().split()))
cache = [-1]*n
def lis(x):
    if cache[x] != -1 : return cache[x]
    cache[x] = 1
    for i in range(x+1, n):
        if a[x]<a[i]:
            cache[x] = max(cache[x], 1+lis(i))
    return cache[x]
for i in range(n):
    lis(i)
print(max(cache))
