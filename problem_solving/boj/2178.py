import sys
n,m = map(int, input().split())
maze = [[j=="1" for j in list(i)] for i in sys.stdin.read().splitlines()]
inf = n*m*2
dist = [[-1]*m]*n
my = [-1, 1, 0, 0]
mx = [0, 0, -1, 1]

def path(y, x):
    if y==(n-1) and x==(m-1) : return 0
    if not maze[y][x] : return inf
    if dist[y][x] != -1 : return dist[y][x]
    ret = inf
    for i in range(4) :
        ty = y+my[i]
        tx = x+mx[i]
        if ty >= n or tx >= m or ty < 0 or tx < 0: continue
        ret = min(ret, path(ty, tx)+1)
    dist[y][x] = ret
    return ret

print(path(0,0))
