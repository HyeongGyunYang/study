import sys
n = list(map(int,sys.stdin.read().splitlines()))
re = n[5]+n[4]+n[3]+n[2]//4
t51 = n[0]-n[4]*11
t42 = n[1]-n[3]*5
t33 = n[2]%4
if t33!=0: re+= 1
#2짜리 남는 자리에 1짜리 넣기
if t51>0:
    if t42<0:
        t51 += t42*4
        t42=0
#3짜리 남는 자리에 1,2 넣기
t=0
if t42>0:
    if t33==1: t=1
    elif t33==2: t=3
    elif t33==3: t=5
    a = 0
    while t42>0 and t>0:
        t-=1
        t42 -= 1
        a+=1
if t51>0: t51 -= t33*9-4*a
# 그래도 1,2가 남았으면
# 2를 채우고 나머지에 1을 채우자
if t42>0:
    re+= t42//9
    t22 = t42%9
    t42 = t22-9
    if t22!=0: re+=1
if t51>0:
    if t22==0: re+=1
    if t42<0: t51 += t42*4
# 그래도 1이 남았으면 1을 채우자
if t51>0:
    re+=t51//36
    if t51%36!=0: re+=1
print(re)
