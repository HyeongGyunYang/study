n, m = map(int, input().split())
if m>n : n,m = m,n
def gcd(p,q):
    while q!=0:
        t = p%q
        p, q = q, t
    return abs(p)
k = gcd(n,m)
print(k)
print(n*m//k)
