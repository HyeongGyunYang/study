a = 0
b = 1
t = 1
n = int(input())
if n==1:
    print(b)
else:
    for _ in range(n):
        if t:
            a = a+b
            t=0
        else:
            b = a+b
            t=1
    if t:
        print(a)
    else:
        print(b)
