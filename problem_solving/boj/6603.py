import sys, itertools
a = sys.stdin.read().splitlines()
for i in a[:-1]:
    t, *b = i.split()
    b = list(map(int, b))
    c = itertools.combinations(b, 6)
    for j in c:
        for k in j:
            print(k,end=" ")
        print()
    print()
