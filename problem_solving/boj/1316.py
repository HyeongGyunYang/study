import sys
from collections import Counter
n = int(input())
a = sys.stdin.read().splitlines()
b = 1
for i in a:
    c = Counter(i)
    d = max(c.values())
    if d>b:
        b = d
print(b)
