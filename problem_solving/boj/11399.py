n = int(input())
lines = list(map(int, input().split()))
lines.sort()
r = lines[-1]
for i in range(n-1):
    r += lines[i]*(n-i)
print(r)
