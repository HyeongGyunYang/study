import sys
n, a, m, b = sys.stdin.read().splitlines()
a = set(map(int, a.split()))
b = list(map(int, b.split()))
for i in b:
    if i in a: print(1)
    else: print(0)
