import sys
array = sys.stdin.read().splitlines()
maximum = max(len(i) for i in array)
for i in range(maximum):
    for j in array:
        if len(j)>i: print(j[i], end='')
