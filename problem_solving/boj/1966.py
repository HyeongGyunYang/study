import sys
testCase = int(input())
cases = sys.stdin.read().splitlines()
for case in range(0,testCase*2,2):
    n,m = map(int,cases[case].split())
    docs = list(map(int,cases[case+1].split()))
    prior = docs[m]
    ret = 1
    for j, doc in enumerate(docs):
        if doc>prior: ret+=1
        elif doc==prior and (j<m or ): ret+=1
    print(ret)
