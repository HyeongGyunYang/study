import sys
t, *a = sys.stdin.read().splitlines()
b = [[0 for i in range(15)] for j in range(15)]
b[0] = list(range(15))
for i in range(1,15):
    for j in range(1,15):
        for k in range(j+1):
            b[i][j] += b[i-1][k]
for i in range(int(t)):
    print(b[int(a[2*i])][int(a[2*i+1])])
