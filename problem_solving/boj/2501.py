import sys
n, k = map(int,sys.stdin.read().split())
measures = list()
for i in range(1,n+1):
    if n%i==0:
        measures.append(i)
        if len(measures)==k:
            print(i)
            break
if len(measures)<k: print(0)
