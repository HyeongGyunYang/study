import sys
# n = n-1+n-2
d, k = map(int, sys.stdin.read().split())
'''
k3 = k1+k2
k4 = k1+k2*2
k5 = k1*2+k2*3
k6 = k1*3+k2*5
k = k1*p+k2*q

4: q = p+q
5: p = p+q
6: q = p+q
'''
p = 1
q = 1
for i in range(4, d+1):
    if i%2: p = p+q
    else: q = p+q

print(p,q)
k1 = 1
k2 = 1
if d%2:
    t = p
    p = q
    q = t
print(p, q)
t = k1*p+k2*q
while 1:
    while t<k:
        k2 += 1
        t = k1*p+k2*q
    if t==k:
        print(k1)
        print(k2)
        print(p,q)
        break
    k1 += 1
    k2 = 1
    t = k1*p+k2*q
