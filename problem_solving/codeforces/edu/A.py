from collections import Counter
n = int(input())
s = input()
d =Counter(s)
length = len(d)
if length==1: print('NO')
else:
    print('YES')
    minimum = d.most_common(length)[-1][0]
    idx = s.index(minimum)
    if idx!=0:
        print(s[idx-1:idx+1])
    else:
        t = ''
        for i in s:
            t+=i
            if Counter(t)[minimum] <= len(t)/2:
                print(t)
                break
