import sys
n, a, b = sys.stdin.read().splitlines()
n = int(n)
a = a.split()
b = b.split()
k = 0
for i in b:
    if k >=len(a): print(0, end=' ')
    elif i not in a[k:]: print(0,end=' ')
    else:
        for j in range(len(a[k:])):
            if a[k+j]==i:
                k += j+1
                print(j+1, end=' ')
                break
