'''
questions
첫번째는 2*(w-1+h-1)
두번째는 각각에서 -4
'''
width, height, k = map(int, input().split())
re = 0
for i in range(k):
    w = width-4*i
    h = height-4*i
    re += 2*(w+h)-4
print(re)
