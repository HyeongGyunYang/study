'''
ti랑 ti+1이
a의 값이 가능한 경우
or 00 01 10 11
00 00 01 10 11
01 01 01 11 11
10 10 11 10 11
11 11 11 11 11
3: 3+a, 1+2
2: 0+2, 2+2
1: 0+1, 1+1
0: 0+0
b의 값이 가능한 경우
&& 00 01 10 11
00 00 00 00 00
01 00 01 00 01
10 00 00 10 10
11 00 01 10 11
3: 3+3
2: 3+2, 2+2
1: 3+1, 1+1
0: 0+a, 1+2

a와b가 가능한 경우
b\a 3  2   1   0
3 3+3 X   X   X
2 3+2 2+2 X   X
1 3+1 X   1+1 X
0 1+2 0+2 0+1 0+0
a는 or
b는 and
'''
tlist = [[[0,0],None,None,None],
        [[0,1],[1,1],None,None],
        [[0,2],None,[2,2],None],
        [[1,2],[1,3],[2,3],[3,3]]]
n = int(input())
a = list(map(int,input().split()))
b = list(map(int,input().split()))

if tlist[a[0]][b[0]]==None:
    print('NO')
else:
    re=[None]
    temp = tlist[a[0]][b[0]]
    for i in range(1,n-1):
        new = tlist[a[i]][b[i]]
        if new==None:
            re = None
            break
        inter = set(temp)&set(new)
        if len(inter)==0:
            re = None
            break
        del re[-1]
        idx = temp.index(list(inter)[0])
        re.append(temp[int(not idx)])
        re.append(temp[idx])
        temp = new
    if re == None:
        print('NO')
    else:
        re.append(temp[int(not temp.index(re[-1]))])
        print('YES')
        for i in re:
            print(i, end=' ')
