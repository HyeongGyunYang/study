1. All of this repo are just summaries whatever I've studied.
  - It means they're usually unkind.
2. They're written by Korean or English.

---
---

- [학교 공지](http://www.konkuk.ac.kr/jsp/Plaza/plaza_01_01.jsp?src=http://www.konkuk.ac.kr:80/do/MessageBoard/ArticleList.do?forum=notice&cat=0000300006)

## 공모전

- [인크루트 공모전](http://gongmo.incruit.com/)
- [잡코리아 공모전](http://campusmon.jobkorea.co.kr/)
- [씽굿](https://thinkcontest.com/)
- [스펙업 - 주간 공모전](https://cafe.naver.com/specup)
- [캠퍼스픽-공모전](https://www.campuspick.com/contest)

## Data Science

- [Dacon](https://dacon.io/) : 매달 열림
- [Kaggle](https://www.kaggle.com)
- [서울시 빅데이터 캠퍼스](https://bigdata.seoul.go.kr/noti/selectPageListNoti.do?r_id=P710)
- [DB guide net](http://www.dbguide.net/index.db)
- [모두의 연구소](http://www.modulabs.co.kr/)

## Development

#### Naver

- [Naver Career](https://recruit.navercorp.com)
- [naver d2](https://d2.naver.com/news)
- [youtube channel](https://www.youtube.com/channel/UCNrehnUq7Il-J7HQxrzp7CA)

#### Kakao

- [Kakao](http://tech.kakao.com/events/)

#### Samsung

- [samsung codeground](https://www.codeground.org/commu/commons/notice)


## Interests

### Jan

- [Dev] [SW maestro](https://ko-kr.facebook.com/swmaestro/)

### Feb

### Mar

- [DS] International Quant Championship
- [DS] [Naver AI Hackathon](https://d2.naver.com/news)

### Apr

- [Con] [Naver HackDay](https://d2.naver.com/news)

### May

- [PS] 삼성전자 대학생 프로그래밍 경진대회
- [PS] SCPC

### Jun

### Jul

- [DS] Big Contest
- [DS] 삼성전자 DS부문 육목SW알고리즘 대회
- [PS] [Kakao Code Fest](https://www.kakaocode.com/)

### Aug

- [PS] [Kakao blind coding test](https://www.welcomekakao.com/)

### Sep

### Oct

- [DS] L.Point BigData Competition
- [PS] Edaily Coding Challenge
- [Con] [Naver HackDay](https://d2.naver.com/news)

### Nov

- [DS] 국제 AI 공모전
- [DS] Naver D2 campus fest

### Dec
