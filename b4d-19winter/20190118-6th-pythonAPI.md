## open API

## 기획서

### 개요

2018년 하반기 졸업작품 공모전에서 6조가 발표한 미세먼지 측정기와 더불어
교내 날씨 데이터를 수집하고, 분석할 수 있는 디바이스를 제작

### 예산

- 동아리 재산
  - raspberry Pi
  - rgb led, r/g/b led

### 참여 인원

- b4d 19 winter 0

### 트렌드 분석

- 중국발 미세먼지에 의해 국내 정부에서는 미세먼지에 대한 구체적인 정책을 수립하고 있음
- 관련 뉴스 :
- 다양한 분야의 IoT 디바이스 시장이 발전되고 있음에 따라 IoT 개발 경험을 가지고 있을 필요성을 느낌

### 개발 환경 설정

- sofrware
  - text editor :
  - computer language : python3
  - git
- hardware
  - raspberry pi

### Dev-note : error log

- trouble shoots
 - error log + solutions
- dev process
  - get some information
    - api, open api에 대한 이해 : [출처 : 00]()
    - ui
  - weather information
    - [abroad api](https://openweathermap.org/)
    - [national api](https://www.data.go.kr/)
