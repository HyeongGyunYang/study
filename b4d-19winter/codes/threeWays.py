import RPi.GPIO as gpio
import time

class nWayLed:
    def __init__(self, rules, leds, pins):
        self._isOn = {k:[] for k in range(1,len(pins))}
        self._leds = leds
        self._pins = pins
        self._rules = rules

    def off(self, piNum):
        for led in self._isOn[piNum]:
            gpio.output(self._pins[piNum][self._leds[led]], False)
        self._isOn[piNum] = []

    def on(self, piNum, rgb):
        if self._isOn[piNum] is not []:
            self.off(piNum)
        self.onSpRule(piNum, rgb)
        gpio.output(self._pins[piNum][self._leds[rgb]], True)
        self._isOn[piNum].append(rgb)

    def onSpRule(self, piNum, rgb):
        if piNum==2 and rgb==blue:
            self.on(piNum, red)

    def timeSleep(self, step):
        if green in self._isOn[1] :
            for s in range(step):
                self.on(4, green)
                time.sleep(0.5)
                self.off(4)
                time.sleep(0.5)
        else : time.sleep(step)

    def letsGetIt(self):
        gpio.setmode(gpio.BCM)
        for pin in self._pins[1:]:
            for p in pin:
                if p != -1: gpio.setup(p, gpio.OUT)
        for pins, leds, timeStep in self._rules:
            for i, pin in enumerate(pins):
                self.on(pin, leds[i])
            self.timeSleep(timeStep)
        gpio.cleanup()

leds = {'red':0,'yellow':1,'blue':2,'green':3}
pins = [[],
        [r, y, -1, g], #1
        [r, y, b, g], #2
        [r, y, b, -1], #3
        [r, -1, -1, g], #4
        [r, -1, -1, g]] #5
rules = [
        [[i for i in range(1,6)], [red for i in range(5)], 1],
        [[1,2,5],[green,green,green,], 10],
        [[1,2,5],[yellow, yellow, red], 3],
        [[1,2],[red, blue], 5],
        [[2,],[yellow], 3],
        [[2,3,4],[red, blue, green], 5],
        [[3,4],[yellow, red], 3],
        ]

led3ways = nWayLed(rules, leds, pins)
led3ways.letsGetIt()
