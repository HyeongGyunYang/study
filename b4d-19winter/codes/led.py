import RPi.GPIO as GPIO
import time

ports = [23,24,25]
GPIO.setmode(GPIO.BCM)

# print("set LED as output")
for port in ports:
    GPIO.setup(port, GPIO.OUT)

for i in range(5):
    for port in ports:
        GPIO.output(port, True)
        time.sleep(1)
        GPIO.output(port, False)
        time.sleep(1)

GPIO.cleanup()
