## Hadrware

### Resistance

- 저항 읽는 법 : android app

## Raspberry Pi

- [raspberrian install](https://acertainlog.wordpress.com/2015/04/29/raspberrypi-raspbian-noobs/)

### Setting

- <code>~$ rasberry-config</code>
- local setting : tz, keyboard etc

## python

- in RasPi, using python2.7
- [Raspberry Pi with Python Official Docs](https://www.raspberrypi.org/documentation/usage/python/)
- [Raspberry Pi GPIO](http://www.rasplay.org/?p=2049)

### RPi.GPIO module basic

#### Input and Ouput

- [LED](2nd/led.py)
- [3ways LED](2nd/led3ways.py)

## Linux

- [예약어](http://www.linuxcertif.com/man/1/bash/ko/)

### text editors

- nano
- vim
