- [examples](https://github.com/darksigma/Fundamentals-of-Deep-Learning)

1. 지능형 기계 만들기
2. 기존 컴퓨터 프로그램의 한계
3. 머신러닝 작동 원리
4. 뉴런
5. 뉴런으로 선형 퍼셉트론 표현하기
6. 전방향 신경망
7. 선형 뉴런과 그 한계
8. 시그모이드, tanh, ReLu 뉴런
9. softmax 출력층
