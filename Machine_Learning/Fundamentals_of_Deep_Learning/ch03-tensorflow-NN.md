## 4. 텐서플로 변수 만들기와 조작하기

- 텐서의 변수가 갖는 속성
  1. 변수는 그래프가 처음 사용되기 전에 명시적으로 초기화되어야 한다.
  2. 모델의 최적 파라미터 설정값을 탐색하므로 반복할 때마다 변수를 수정하기 위한 경사법을 사용할 수 있다.
  3. 추후 사용을 위해 변수에 저장된 값을 디스크에 저장하고 다시 불러올 수 있다.

- <pre><code>
# gaussian으로 초기화
weights = tf.Variable(tf.random_normal([300,200], stddev=0.5), name='weights')

# 기타 초기화 방법
shape = [2,3]
tf.zeros(shape, dtype=tf.float32, name=None)
tf.ones(shape, dtype=tf.float32, name=None)
tf.random_normal(shape, mean=0.0, stddev=1.0, dtype=tf.float32, seed=None, name=None)
tf.truncated_normal()
tf.random_uniform()
</code></pre>

  1. 두 개의 인자를 tf.Variable로 넘긴다.
    1. tf.random_normal : 300개의 뉴런이 있는 층과 200개의 뉴런이 있는 층을 연결한 가중치들을 의미하는  $300 \times 200$ 크기의 텐서를 지정
    2. name : 계산 그래프에서 적절한 노드를 참조할 수 있게 하는 고유한 식별자
    3. trainable : default=True
  2. 기타 여러가지 초기화 방법이 있다.

- <code>tf.Variable()</code> 호출 시, 계산 그래프에 추가되는 연산
  - 변수를 초기화하는 데 사용하는 텐서를 생성하는 연산
  - 변수 사용 전 초기화한 텐서로 변수를 채우는 책임이 있는 <code>tf.assign</code>연산
  - 변수의 현재 값을 유지하는 변수 연산
- <code>tf.initalize_variables()</code> : 그래프 내의 모든 <code>tf.assign</code>연산 실행
- <code>tf.initalize_variabes(*vars)</code> : 선택한 변수만 초기화

### 3.5 텐서플로 연산

- 텐서플로 연산 : 계산 그래프에서 텐서에 적용하는 추상적 변환
- 연산이 가지는 속성 : 이름 속성, 입력형태 속성(int64) 등
- 연산은 하나 또는 그 이상의 커널로 이루어진다
  - 이 커널은 장치 종속적인 구현 : cpu, gpu
- 텐서플로 연산 요약 (pp62)

## 6. placeholder tensor

- 입력 전달

## 7. tensorflow session

- 텐서플로 프로그램은 세션을 사용해 계산 그래프와 상호작용한다.
- 용도
  - 텐서플로 세션은 초반 그래프 생성을 담당
  - 모든 변수를 적절하게 초기화
  - 계산 그래프를 실행

## 8. 변수 범위 탐색과 변수 공유

- 인스턴스화하고 싶은 변수들의 큰 집합을 한 곳에서 함께 재사용하고 공유해야 할 때
- <code>tf.Variable</code>을 사용하면 변수의 사본만들기가 된다.
- 변수 범위 지정 :<code>tf.get_variable(), tf.variable_scope()</code>
  - <code>tf.get_variable()</code> : 주어진 이름의 변수가 인스턴스화되지 않았는지를 확인
    - 변수 범위 내에서 공유될 수 있게 하기
    <pre><code>
    with tf.variable_scope("shared_variables") as scope:
      i_1 = tf.placeholder(tf.float32, [1000, 784], name="i_1")
      my_network(i_1)
      scope.reuse_variables()
      i_2 = tf.placeholder(tf.float32, [1000, 784], name="i_2")
      my_network(i_2)
    </code></pre>

## 9. cpu, gpu

- 계산 그래프가 어떤 장치들을 사용하는지 검사하기 :
 <code>tf.Session(config=tf.ConfigProto(log_device_placement=True))</code>
- 특정 장치를 사용하고 싶다면 : <code>tf.device()</code>
- 텐서플로에서 사용 가능한 다른 장치를 찾기 원하면 :
  - <pre><code>
  with tf.device('/gpu:2'):
    a = tf.constant([1.0, 2.0, 3.0, 4.0], shape=[2,2], name='a')
    b = tf.constant([1.0, 2.0], shape=[2,1], name='b')
    c = tf.matmul(a,b)

  sess = tf.Session(config=tf.ConfigProto(allow_soft_placement=True, log_device_placement=True))
  sess.run()
  </code></pre>
- 다중 gpu 예제 : pas

## 10. 텐서플로에서 로지스틱 회귀 모델 지정하기

1. 추론 : 주어진 미니배치로 출력 분류에 대한 하나의 확률 분포를 만든다.
2. 손실 : 오차 함수의 값을 계산
3. 학습 : 모델 파라미터들의 경사 계산과 모델의 갱신을 담당
4. 평가 : 모델의 효용성을 결정

## 11. 로지스틱 회귀 모델 기록하기와 학습시키기

- <code>tf.summary.~()</code>

## 12. 텐서보드로 계산 그래프와 학습 시각화하기

- <code>$ tensorboard --logdir = <absPath></code>
  - logdir : <code>tf.summary.FileWriter()</code> 설정을 통해 기록

## 13. 텐서플로에서 MNIST를 위한 다층 모델 만들기
