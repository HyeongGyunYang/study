1. Vanishing Gradient / Exploding Gradient
2. 훈련 시간이 길어짐
3. Overfit

## 1. Vanishing/Exploding Gradient

- 정방향 : 각 층의 출력에 대한 분산이 입력에 대한 분산과 같아야 한다.
- 역방향 : 층을 통과하기 전과 후의 그래디언트 분산이 동일해야 한다.

### 1.1 Initialization : Xavier, He

- Xavier
  - 로지스틱일 때,
    - $`mean = 0, \sigma=\sqrt{\frac{2}{n_{inputs}+n_{ouputs}}}`$인 정규분포
    - $`r=\sqrt{\frac{6}{n_{inputs}+n_{outputs}}}`$일 때 -r과 +r 사이의 균등분포
  - 하이퍼볼릭 탄젠트
- He : 렐루와 그 변형에 대한 초기화 방식

### 1.2 수렴하지 않는 활성화 함수

- 수렴하는 활성함수는 죽은 렐루 문제 등이 발생
- LeakyReLU
- RReLU(규제-overfit 방지)
- PReLU(대규모)
- ELU(훈련속도도 빠름)
- ReLU6
- SELU
- Swish

### 1.3 Batch Normalization

- 내부 공변량 변화 Internal Covariate Shift :
  훈련하는 동안 이전 층의 파라미터가 변함에 따라 각 층에 들어오는 입력의 분포가 변화되는 문제
- Batch Normalization
  - 각 층에서 활성화 함수를 통과하기 전에 평균=0인 정규화
  - 각 층에서 스케일 조정 / 이동을 위한 두 개의 파라미터로 결과값의 스케일을 조정하고 이동
  - 모델이 층마다 입력 데이터의 최적 스케일과 평균을 학습한다.
- 보통 moving average로 효율적으로 학습됨
- 스케일, 이동, 평균, 표준편차가 층마다 학습됨 : $`\gamma, \beta, \mu, \sigma`$
- 단점
  - 모델이 복잡해짐
  - 실행 시간이 오래 걸림 (훈련은 빨라질 수 있지만, 예측에 시간이 오래 걸림)

#### 1.3.1. 텐서플로를 사용해 배치 정규화 구현하기

<code>tf.layers.batch_normalization</code>
- momentum
  - BN은 지수감소를 사용해 이동평균을 계산한다.
  - 적절한 모멘텀은 1에 가깝다. (0.9, 0.99 등)
  - 데이터셋이 크고, 마니배치가 작을 경우 1에 더 가깝다.

### 1.4 그래디언트 클리핑 Gradient Clipping

- 그래디언트 폭주를 줄이는 방법 : 일정 임곗값을 넘어서지 못하게 그래디언트를 잘라낸다.
- 일반적으로 배치 정규화를 더 선호한다.

## 2. 미리 훈련된 층 재사용하기

- 전이 학습 transfer learning : 해결하려는 것과 비슷한 유형의 문제를 처리한 신경망이 이미 있는지 찾아보고
 그런 다음 그 신경마의 하위층을 재사용하는 것이 좋다.
- 장점
 - 훈련 속도 향상
 - 필요 훈련 데이터도 적음
- 비슷한 저수준 특성을 가질 때 잘 작동한다.
- 원래 문제에서 사용한 것과 크기가 다른 이미지를 입력으로 사용한다면
 원본 모델에 맞는 크기로 변경하는 전처리 단계를 추가해야 한다.

### 2.1 텐서플로 모델 재사용하기

[텐서플로 모델 재사용](ch11/11-2-1-텐서플로-모델-재사용.py)
- <code>import_meta_graph()</code> : 기본 그래프에 연산을 적재할 수 있다.
  - <code>Saver</code> 변수를 반환 : 나중에 저장된 모델 파라미터를 불러올 때 사용할 수 있다.

- tip : 파이썬의 텐서 객체는 만들어진 연산의 출력을 가리키는 포인터와 같은 역할을 한다.
  - <code>c=tf.constant(1, name='C')</code>에서 상수연산이름(c.op.name)은 'C',
  반환되는 텐서이름(c.name)은 'C:0'

- 필요한 연산 이름 찾기
  - 문서화가 안되어 있으면ㅠ
  - 텐서보드를 사용하여 그래프를 탐색
  - get_operations() method로 모든 연산의 리스트를 볼 수 있다.

- 관심 대상인 층만 재사용하기
  - 미리 훈련된 그래프의 파이썬 코드에 접근할 수 있다면,
  필요한 부분만 재사용하고 나머지는 버리면 된다.
  - 훈련된 모델을 복원하는 Saver 객체 필요
  - 새로운 모델을 저장하는 Saver 객체 필요
  - [하위 은닉층만 재사용하기](ch11/11-2-1-일부층-재사용.py)

- 작업이 비슷할수록 더 많은 층을 재사용한다.

### 2.2 다른 프레임워크의 모델 재사용하기

- 수동으로 모델 파라미터를 읽어 들여 적절한 변수에 할당해야 한다.
- [첫 번째 은닉층으로부터 가중치와 편향 복사하기](ch11/2-2-reuse-other-FW.py)

### 2.3 신경망의 하위층을 학습에서 제외하기

- 하위층 가중치 동결
- 상위층의 가중치를 훈련시키기 쉽다.
- frozen layer :
- 방법1 : 하위층의 변수를 제외하고 훈련시킬 변수 목록을 옵티마이저에 전달
  - [상위층 변수만 전달하는 방법](ch11/2-3-sent-optimizer.py)
- 방법2 : stop_gradient() 층을 추가
  - [stop_gradient()](ch11/2-3-stop-gradient.py)

### 2.4 동결된 층 캐싱하기

### 2.5 상위층을 변경, 삭제, 대체하기

### 2.6 모델 저장소

### 2.7 비지도 사전훈련

### 2.8 보조 작업으로 사전훈련

## 3. 고속 Optimizer

- 훈련 속도를 높이는 4가지 전략 (review)
  1. 연결 가중치에 좋은 초기화 전략 적용하기
  2. 좋은 활성화 함수 사용하기
  3. 배치 정규화 사용하기
  4. 미리 훈련된 신경망의 일부 재사용하기
- 5번째 : Optimizer
  - Momentum Optimization
  - Nesterov Accelerated Gradient
  - AdaGrad
  - RMSProp
  - Adam

### 3.1 Momentum Optimization

- Core Idea : 볼링공이 완만한 경사를 따라 굴러갈 때, 종단속도에 도달할 때까지는 빠르게 가속된다.
- Vs GD
  - GD : $`cost func : J(\theta), GD : &nabla;_{\theta} J(\theta), \eta = learning rate, \theta \leftarrow \theta-\eta \mathbf{GD}`$
    - 이전 GD가 얼마였는지 상관 없다
    - 국부적으로 GD가 아주 작으면 매우 느려진다.
  - momentum : 매 방복에서 현재 GD를 eta를 곱한 후 momentum vector M에 더하고 이 값을 빼는 방식으로 가중치 갱신
    1. $`\mathbf{m} \leftarrow \beta \mathbf{m}+\eta&nabla;_{\theta} J(\theta)`$
    2. $`\theta \leftarrow \theta - \mathbf{m}`$
- 종단 속도(가중치를 갱신하는 최대 크기) : $`\eta \times \mathbf{GD} \times \frac{1}{1-\beta}`$
- momentum이 더 빠르게 평편한 지역을 탈출한다.
- 배치 정규화를 하지 않아서 상위층이 스케일이 매우 다른 입력을 받을 때 도움이 된다.
- local minimum을 건너 뛰도록 하는 데도 도움이 된다.
- 시스템에 마찰저항이 조금 있으면 : optimizer가 진동하지 않고 최적화하는데 도움이 된다.
- <code>optimizer = tf.train.MomentumOptimizer(learning_rate=learning_rate, momentum=0.9)
- 보통 0.9에서 잘 작동한다.
- 단점 : 튜닝할 파라미터가 하나 늘어난다.

### 3.2 Nesterov Momentum Optimization, Nesterov Accelerated Gradient (NAG)

- 기본 momentum보다 거의 항상 더 빠르다.
- 알고리즘
  1. $`\mathbf{m} \leftarrow \beta\mathbf{m}+\eta&nabla;_{\theta}J(\theta-\beta\mathbf{m})`$
  2. $`\theta\leftarrow\theta-\mathbf{m}`$
- 보통 모멘텀 벡터가 올바른 방향을 가리킬 것이기 때문에,
원래 위치보다 조금 더 나아가서 그래디언트를 측정한다.
- 진도을 감소시키고, 수렴을 빠르게 만들어준다.
- <code>optimizer = tf.train.MomentumOptimizer(learning_rate=learning_rate, momentum=0.9, use_nesterov=True)</code>
