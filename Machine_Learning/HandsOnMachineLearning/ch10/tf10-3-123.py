import tensorflow as tf
import numpy as np
n_inputs = 28 * 28
n_hidden1 = 300
n_hidden2 = 100
n_outputs = 10

# 첫 번째 차원을 따라 샘플이 있고, 두 번째 차원을 따라 특성이 있는 2d tensor X
X = tf.placeholder(tf.float32, shape=(None, n_inputs), name="X")
y = tf.placeholder(tf.int64, shape=(None), name="y")

def neuron_layer(X, n_neurons, name, activation=None):
    # 층 이름으로 이름 범위를 만든다. 계산 노드를 정리해 두면 텐서보드로 보기 편하다.
    with tf.name_scope(name):
        # 입력 행렬의 크기에서 두 번째 차원을 사용해 입력 특성의 수를 구한다.
        n_inputs = int(X.get_shape()[1])
        '''가중치 행렬을 담을 W 변수를 만든다(커널)
        이 행렬은 각 입력과 각 뉴런 사이의 가중치를 담는 2d tensor
        절단 가우시안을 사용해 무작위로 초기화'''
        stddev = 2/np.sqrt(n_inputs+n_neurons)
        init = tf.truncated_normal((n_inputs, n_neurons), stddev=stddev)
        W = tf.Variable(init, name="kernel")
        # 뉴런마다 하나의 편향을 갖도록 변수 b를 만들고 0으로 초기화(대칭 문제가 없다.)
        b = tf.Variable(tf.zeros([n_neurons]), name="bias")
        '''Z = X*W+b
        층에 있는 모든 뉴런과 배치에 있는 모든 샘플에 대한 계산을 효율적으로 수행
        브로드캐스팅'''
        Z = tf.matmul(X, W)+b
        # 활성함수가 있으면 적용
        if activation is not None:
            return activation(Z)
        else:
            return Z

with tf.name_scope("dnn"):
    hidden1 = neuron_layer(X, n_hidden1, name="hidden1", activation=tf.nn.relu)
    hidden2 = neuron_layer(hidden1, n_hidden2, name="hidden2", activation=tf.nn.relu)
    logits = neuron_layer(hidden2, n_outputs, name="outputs")

with tf.name_scope("dnn"):
    hidden1 = tf.layers.dense(X, n_hidden1, name="hidden1", activation=tf.nn.relu)
    hidden2 = tf.layers.dense(hidden1, n_hidden2, name="hidden2")
    logits = tf.layers.dense(hidden2, n_outputs, name="outputs")

with tf.name_scope("loss"):
    xentropy = tf.nn.sparse_softmax_cross_entropy_with_logits(labels=y, logits=logits)
    loss = tf.reduce_mean(xentropy, name="loss")

learning_rate = 0.01
with tf.name_scope("train"):
    optimizer = tf.train.GradientDescentOptimizer(learning_rate)
    training_op = optimizer.minimize(loss)

with tf.name_scope("eval"):
    correct = tf.nn.in_top_k(logits, y, 1)
    accuracy = tf.reduce_mean(tf.cast(correct, tf.float32))

init = tf.global_variables_initializer()
saver = tf.train.Saver()

from tensorflow.examples.tutorials.mnist import input_data
mnist = input_data.read_data_sets("/tmp/data/")

n_epochs = 40
batch_size = 50

with tf.Session() as sess:
    init.run()
    for epoch in range(n_epochs):
        for iteration in range(mnist.train.num_examples // batch_size):
            X_batch, y_batch = mnist.train.next_batch(batch_size)
            sess.run(training_op, feed_dict={X: X_batch, y: y_batch})
        acc_train = accuracy.eval(feed_dict={X: X_batch, y: y_batch})
        acc_val = accuracy.eval(feed_dict = {X: mnist.validation.images, y: mnist.validation.labels})
        print(epoch, "train accuracy:", acc_train, "Validation accuracy:", acc_val)
    save_path = saver.save(see, "./my_model_final.ckpt")

with tf.Session() as sess:
    saver.restore(sess, "./my_model_final.ckpt")
    X_new_scaled = [...]
    Z = logits. eval(feed_dict={X: X_new_scaled})
    y_pred = np.argmax(Z, axis=1)
