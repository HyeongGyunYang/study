-	분류, 회귀, 다중출력
-	매우 복잡한 데이터셋도 학습 가능
-	장점
	-	데이터 전처리가 거의 필요치 않다.
	-	특성의 스케일을 맞추거나 평균을 원점에 맞추는 작업이 필요치 않습니다.

### 1. 결정 트리 학습과 시각화

-	DecisionTreeClassifier

<pre><code>
from sklearn.datasets import load_iris
from sklearn.tree import DecisionTreeClassifier

iris = load_iris()
X = iris.data[:, 2:] # petal length and width
y = iris.target

tree_clf = DecisionTreeClassifier(max_depth=2, random_state=42)
tree_clf.fit(X, y)
</code></pre>

-	visualization

<pre><code>
from sklearn.tree import export_graphviz

export_graphviz(
        tree_clf,
        out_file=image_path("iris_tree.dot"),
        feature_names=["꽃잎 길이 (cm)", "꽃잎 너비 (cm)"],
        class_names=iris.target_names,
        rounded=True,
        filled=True
    )
</code></pre>

### 2. 예측하기

-	이진분류노드를 타고 내려가면서 분류
-	Impurit를 측정 : gini속성
-	알고리즘마다 결정 트리의 모양이 다르다.
	-	CART 알고리즘은 이진트리를 만든다
	-	ID3같은 알고리즘은 둘 이상의 자식 노드를 가진 결정 트리를 만들 수 있다.

#### white box, black box

-	white box : dicision tree,
-	black box : RF, NN

### 3. 클래스 확률 추정

-	한 샘플이 특정 클래스 k에 속할 확률을 추정할 수도 있다.
-	이 샘플에 대해 리프노드를 찾아서 그 노드에 있는 클래스 k의 훈련 샘플의 비율을 반환

### 4. CART 훈련 알고리즘

-	Classification And Regression Tree Algorithm
	1.	훈련셋을 하나의 특성 $`k`$의 임곗값 $`t_{k}`$를 사용해 두 개의 서브셋으로 나눈다.
	2.	이 때, $`(k, t_{k})`$는 가장 순수한 서브셋으로 나눌 수 있는 걸로 고른다.
	3.	Recursion
	4.	최대 깊이가 되면 중단, 불순도를 줄이는 분할을 찾을 수 없을 때 중지
-	greed algorithm : 따라서 최적해는 아니다
-	최적해를 찾는 알고리즘은 NP완전문제

### 5. 계산 복잡도

### 6. 지니 불순도 또는 엔트로피

-	어떤 셋이 한 클래스의 샘플만 담고 있다면 엔트로피는 0
-	지니가 좀 더 빠르다
-	지니는 가장 빈도 높은 클래스를 한쪽 가지로 고립시키는 경향 <-> 엔트로피는 균형잡힌 트리

### 7. 규제 변수

-	결정트리는 훈련 데이터에 대한 제약사항이 거의 없다.
-	제한을 두지 않으면 과적합되기 쉽다
-	규제의 종류
	-	max depth
	-	min samples split
	-	min samples leaf
	-	max leaf node
	-	max feature

#### 가지치기

-	Puning
-	순도를 높이는 것이 통계적으로 큰 효과가 없다면 가지치기
-	카이제곱 검정 등

### 8. 회귀

-	Decision Tree Regression
-	각 노드에서 클래스를 예측하는 대신 mse를 계산한다.
-	mse를 최소화하는 방식으로 노드를 분할한다.
-	규제가 없다면 결정트리회귀 또한 결정트리분류처럼 과적합되기 쉽다.

### 9. 불안정성

-	단점 : 계단 모양의 결정 경계를 만든다
	-	훈련셋의 회전에 민감하다.
	-	해결책 : 훈련셋을 적절하게 회전시키는 PCA 기법을 도입한다.
-	주단점 : 훈련셋의 작은 변화에도 매우 민감
