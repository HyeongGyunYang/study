### 1. MNIST

-	training set을 shuffle 해서 모든 CV가 비슷해지게 만든다.
	-	특정 숫자가 누락되는 일이 없도록
	-	어떤 학습 알고리즘은 훈련 샘플의 순서에 민감하다
	-	비슷한 샘플이 연이어 나타나면 성능이 나빠진다.

### 2. 이진 분류기 훈련

-	일단 숫자 하나만 구별해보자
-	분류 모델 선택 : sklearn.linear_model.SGDClassifier
	-	Stochastic Gradient Descent 확률적 경사 하강법
	-	한 번에 하나씩 훈련 샘플을 독립적으로 처리한다.
	-	장점
	-	매우 큰 데이터셋을 효율적으로 처리한다.
	-	온라인 학습에 잘 맞는다.

### 3. 성능 측정

-	분류기 평가는 회귀 평가보다 어렵다

#### 3-1. 교차 검증을 사용한 정확도 측정

-	[교차 검증 구현](03_classification.ipynb)
	-	StratifiedkFold: 클래스별 비율이 유지되도록 계층적 샘플링
-	정확도는 분류 평가에 부적절하다 : 특히! 불균형한 데이터셋을 다룰 때

#### 3-2. 오차행렬 Confusion Matrix

-	기본적인 아이디어는 클래스 A의 샘플이 클래스 B로 분류된 횟수를 세는 것
-	sklearn.model_selection.cross_val_predict(): K-CV를 수행하지만 평가 점수가 아닌 예측을 반환
-	sklearn.metrics.confusion_matrix()
-	Index
	-	Precision
	-	Recall

#### 3-3. Precision & Recall

-	sklearn.metrics.precision_score, recall_score, fi_score
-	F-score : harmonic mean
-	TRADE OFF

#### 3-4. Precision/Rcall TradeOff

-	분류기는 decision function을 사용해서 작동하므로,
-	decision_function() method를 통해 임곗값을 알 수 있다.
-	프로젝트에 따라, 재현율과 정밀도의 트레이드 오프가 결정된다.

#### 3-5. ROC curve

-	수신기 조작 특성 receiver operating characteristic
-	민감도(재현율) - 특이도 그래프
-	sklearn.metrics.roc_curve
-	AUC area under the curve 곡선 아래의 면적 : 분류기 비교 가능
-	RF와의 비교
	-	RF는 클래스가 될 확률을 반환하므로, 양성 클래스에 대한 확률을 점수로 쓰자

### 4. Multi-class Classification

-	RF, naive Bayes : multi class
-	SVM, linear classifier : binary
	-	OvA one-vs-all : 각 클래스마다 훈련시켜서 점수 중 가장 높은 것을 클래스로 선택
	-	OvO one-vs-one : 각 클래스의 조합마다 훈련 시켜서 가장 많이 양성 판정 받은 클래스로 선택
		-	장점 : 훈련에 두 클래스만 필요함
-	훈련 세트의 크기에 민감한 알고리즘은 OvO를 선호
-	대부분은 OvA를 선택

### 5. 에러 분석

-	모델 선택이 끝난 후, 모델 성능을 향상 시키자
-	오차행렬 확인
	-	특정 클래스가 분류가 잘 안되었을 경우,
		-	데이터셋에 해당 클래스가 적거나
		-	해당 클래스 분류를 잘 못하거나
-	에러의 비율을 비교 : 갯수는 각 클래스의 원소 수가 일정하지 않을 경우 오해의 소지가 있다.
-	에러의 특성에서 통찰을 얻기
	-	MNIST의 경우, 동심원의 수를 센다던가
	-	이미지에서 패턴이 드러나도록 전처리 : Scikit-Image, Pillow, OpenCV
-	에러의 이미지를 열어보기
	-	분류기가 이미지의 위치나 회전 방향에 민감하다
	-	따라서 이미지를 중앙에 위치시키고 회전되어 있지 않도록 전처리

### 6. multi-label classification

-	샘플마다 여러 개의 클래스를 출력하는 분류기
-	평가 방법
	-	플젝마다 다르다.
	-	f1 score를 구하고, 평균을 낸다
-	레이블에 가중치를 줘서 분류 중요도를 가할 수 있다.
-	support 지지도 : 클래스의 샘플 수

### 7. multioutput-multiclass classification

-	다중 레이블 분류에서 한 레이블이 다중 클래스(값을 두개 이상 가질 수 있다.)가 될 수 있도록 한 것.
