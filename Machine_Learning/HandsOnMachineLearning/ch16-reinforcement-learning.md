- 정책 그래디언트 policy gradient
- 심층 Q 네트워크 deep Q-network, DQN
- 마르코프 결정 과정 markov decision process, MDP

## 1. 보상을 최적화하기 위한 학습

- 에이전트는 관측을 하고 주어진 환경에서 행동을 한다.
- 결과로 보상/패널티를 받는다.
- 보상의 장기간 기대치를 최대로 만드는 행동을 학습
- 예
  - 보행 로봇 제어
  - 게임
  - 보드게임
  - 온도설정
  - 주식 매매
  - 자율주행
  - 광고 배치

## 2. 정책 탐색

- 정책 policy : 소프트웨어 에이전트가 행동을 결정하기 위해 사용하는 알고리즘
- 확률적 정책 : 무작위성이 있는 정책
- 탐색 방법
  1. 완전 탐색
  2. 유전 알고리즘
  3. 정책 그래디언트 (PG) 최적화 :
   정책 파라미터에 대한 보상의 그래디언트를 평가해서 높은 보상의 방향을 따르는 그래디언트로 파라미터를 수정
- 유전 알고리즘
  - 방법
    1. 1세대 알고리즘 n1개 테스트
    2. 상위 알고리즘들에 무작위성을 추가해 2세대 알고리즘 n2개 테스트
    3. 좋은 정책을 찾을 때까지 여러 세대에 걸쳐 반복
  - 유전자풀에 다양성을 보존하기 위해 낮은 성능의 정책도 조금 살아남을 여지를 주는 것이 나을 때가 많다.
  - 무성/유성 생식 : 여러 부모에서 게놈을 받을 것인가?

## 3. OpenAI 짐

- 시뮬레이션 환경
- [OpenAI gym](https://gym.openai.com)
- [gym 실습](ch16/gym.ipynb)

## 4. 신경망 정책

- 관측을 입력으로 받고 실행할 행동에 대한 확률을 추정해서 행동을 선택
- 확률 기반 행동의 의의 : 새로운 행동의 탐험과 잘 할 수 있는 행동의 활용 사이의 균형을 맞춘다.
- 특별한 환경(각 관측이 환경에 대한 완전한 상태를 나타냄)에서는 과거의 관측은 무시해도 된다.
  - (잡음이 있는 환경) : 과거 관측 참고가 좋다.
- [신경망 정책 구현](ch16/nn.ipynb)

## 5. 행동 평가 : 신용 할당 문제

- 신용할당 문제 credit assignment problem : 어느 행동에 reword/penalty 의 책임이 있는지
- 할인 계수
  - 일반적으로 0.95~0.99 (반감기 13~69 step)

## 6. 정책 그래디언트

- 초기에 훈련 속도를 높이려면
  - 에이전트에 가능한 사전 지식을 많이 주입한다.
  - 이미 검증된 좋은 정책을 따르는 신경망으로 초벌 훈련

## 7. markov decision process

- markov chain : 메모리가 없는 stochastic process
  1.정해진 개수의 상태를 가지고 있으며,
  - 각 스텝마다 한 상태에서 다른 상태로 랜덤하게 전이
  3. 상태 전이 확률은 고정, 과거 상태에 상관없이
- MDP : MC에서 선택한 행동에 따라 보상을 받고, 보상을 최대하는 정책을 찾는다.
- DP(dynamic probramming)로 최적 상태 가치 추정
- Q-value : 상태-행동 가치 추정 알고리즘
- Q-value iteration algorithm

## 8. 시간차 학습과 Q-learning

- 독립적인 행동으로 이루어진 강화 학습 문제는 보통 MDP로 모델링 될 수 있지만,
- 초기에 에이전트는 전이 확률에 대해 알지 못한다.
- (Bayesian) TD learning, temporal difference learning :
  agent가 MDP에 대해 일부 정보만 알고 있을 때 다룰 수 있도록 변형
  1. 에이전트는 탐험정책을 사용해서 MDP 탐색
  2. TDLA는 관측에 근거하여 업데이트 (sgd랑 비슷한 느낌이 있다.)
- Q-learning : 전이 확률과 보상을 초기에 알지 못한 상황에서 q-value iteration algorithm 적용
- model based RL : MDP의 전이 확률과 보상에 대한 모델을 알고 있는 경우
  - model free RL : TD, QL
- off-policy algorithm : 훈련된 정책을 실행에 사용하지 않고 그냥 최대의 q-value를 사용한다.
  - on-policy algorithm : 다음 상태의 행동도 정책에 따라 선택

### 8.1 탐험 정책

- $`\epsilon-greedy policy`$ : e확률로 랜덤하게 행동 또는 1-e확률로 최선의 선택
  - 장점 : 관심 있는 부분을 살피는 데 점점 더 많은 시간을 사용 + 알려지지 않은 지역을 방문하는데도 충분한 시간을 사용
  - 보통 e는 1에서 시작해서 점점 감소시킨다.
- q-val 추정에 보너스를 추가하는 방식 :
  이전에 많이 시도하지 않았던 행동을 시도하도록 강조

### 8.2 근사 q-learning, deep q-learning

- 가능한 경우가 너무 많아서 q-val에 대한 추정값을 기록할 수 있는 방법이 보통 없다.
- approximate q-learning : 상태-행동 쌍의 q-val을 근사하는 함수를 찾아내는 것
  - 옛날에는 수동 선정 특성을 선형 조합하는 방식 선호
  - 딥마인드 이후, 심층신경망
- deep q-net, deep q-learning :
  - 다음 상태와 가능한 모든 행동에 대해 DQN 실행(뭔가 recursion)
  - target q-val로 GD를 실행
- deepmind ver.
  - replay memory에 경험 데이터를 저장
  - online DQN + target DQN
    - online : 훈련 반복마다 플레이하고 학습
    - target : target q-val을 계산할 때만 사용
    - 일정 간격으로 online이 target으로 복사된다.

## 9. DQN으로 미스 팩맨 플레이 학습

- oepnai gym의 모든 환경을 설치 :
  - brew install cmake boost boost-python sdl2 swig wget
  - gym[all]

- [구현]
- 잘 학습시키기
  - 사전지식 : 전처리, 보상
  - 기본 전략을 모방하는 모델을 먼저 훈련시키고, 이 모델을 부트스트래핑
- RL은 여전히 매우 오랜 인내와 튜닝을 필요로 한다.
