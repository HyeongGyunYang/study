- 미래를 예측
- 시계열 데이터
- 임의의 길이를 가진 시퀀스

## 1. 순환 뉴런

- 각 time step(frame)마다 recurrent neuron은 해당 타임의 입력과 이전 타임의 출력을 받는다.
- 두 벌의 가중치 : 해당 타임의 입력, 이전 타임의 출력

### 1.1 메모리 셀

- memory cell : 어떤 상태를 보존하는 신경망의 구성요소

### 1.2 입력과 출력 시퀀스

- sequence to sequence network
- sequence to vector
- vector to sequence
- encoder + decoder :
  - eg) delayed sequence to sequence : S2V + V2S

## 2. 텐서플로로 기본 RNN 구성하기

- [implement](ch14/implementRNN.ipynb)

### 2.1 정적으로 타임 스텝 펼치기

- [implement](ch14/staticRNN.ipynb)

### 2.2 동적으로 타임 스텝 펼치기

- 특징
  - while loop 사용 : 타임 스텝 크기에 상관없이 하나의 행렬곱만 생성된다.
  - cpu memory 사용 : swap_memory 옵션. OOM error를 피할 수 있다.
- [implement](ch14/dynamicRNN.ipynb)

### 2.3 가변 길이 입력 시퀀스 다루기

- rnn 함수 호출시, sequence length 파라미터 지정
- [example](ch14/inputRNN.ipynb)

### 2.4 가변 길이 출력 시퀀스 다루기

- EOS token이라 불리는 특별한 출력의 정의

## 3. RNN 훈련하기

- BPTT, backpropagation through time 전략 : 타임 스텝으로 넽웤을 펼치고, 역전파

### 3.1 시퀀스 분류기 훈련하기

- [implement](ch14/trainRNN.ipynb)

### 3.2 시계열 예측을 위해 훈련하기

- [implement](ch14/timeSeries.ipynb)
- vector 출력을 원함
  - OutputProjectionWrapper
  - 출력의 형태를 바꾸고 출력 크기에 맞게 단일 FC로 출력 텐서 크기 변경
- Cell Wrapper : 보통의 셀과 비슷하게 작동. 모든 메서드 호출을 감싼 셀로 전달

### 3.3 RNN의 창조성

## 4. 심층 RNN

- <code>tf.contrib.rnn.MultiRNNCell()</code>

### 4.1 여러 gpu에 심층 RNN 분산하기

- 각기 다른 device() 블록에서 BasicRNNCell로 만드는 건 불가능
- 자신만의 셀 래퍼를 만들어서 쓴다.

### 4.2 드롭아웃 적용하기

- DropoutWrapper

### 4.3 많은 타임 스텝에서 훈련의 어려움

- 문제점
  1. 긴 훈련시간
  2. 그래디언트 소실과 폭주
  3. 첫번째 입력의 기억이 점차 사라진다.
- 해결책
  1. 파라미터 초기화, 수렴하지 않은 활성화 함수, 배치 정규화, 그래디언트 클리핑, 빠른 옵티마이저
  2. 제한된 타임 스텝에 역전파 : 일정한 타임 스텝만큼만 rnn을 펼치기
    - 장기간의 패턴을 학습할 수 없다
    - 줄어든 시퀀스에 오래된 데이터와 최신의 데이터가 포함되도록 만든다.
  3. 장기간의 메모리를 가진 여러 종류의 셀 : LSTM 등

## 5. LSTM 셀

- <code>tf.contrib.rnn.BasicLSTMCell()</code>
- 기본 개념 : 상태가 두 개의 벡터로 나뉜다 : 장기상태, 단기상태
- Core Idea : 네트워크가 장기 상태에 저장할 것, 버릴 것, 읽어 들일 것을 학습
- 과정
  - 장기기억
    0. 장기기억을 이어받음
    1. 삭제 게이트 forget gate : 일부 기억을 잃는다.
    2. 입력 게이트 input gate : 에서 선택된 새로운 기억을 추가한다.
    3. 출력 행
  - 단기 기억
    1. 이전 타임의 단기기억과 해당 타임의 입력을 FC
    2. 삭제, 입력, 출력 게이트로 보내진다.
    3. 출력게이트에서 장기기억과 만나서 새로운 단기 기억 출력

### 5.1 Peephole connection

- LSTM 변종
- <code>tf.contrib.rnn.LSTMCell()</code>
- 게이트 제어시, 이전 단기기억과 해당 타임의 입력만 보는 게 아니라 장기 상태에도 노출 시키기

## 6. GRU

- LSTM 간소화
- 차이점
  - 장/단기 상태 벡터가 하나의 벡터로
  - 하나의 게이트 제어기가 삭제/입력게이트를 모두 제어
  - 출력 게이트가 없어서 전체 상태 벡터가 출력되고, 어느 부분을 출력할지를 결정하는 새로운 게이트가 있다.

## 7. NLP

### 7.1 워드 임베딩

- 개념 : 비슷한 단어들이 비슷하게 학습
- <code>tf.nn.embedding_lookup()</code>
- 미리 학습된 임베딩 가져다 쓰기
  1. 동결 : 학습속도가 빠름
  2. 수정 가능 : 성능향상
- 임베딩은 많은 수의 다른 값을 가진 (값 사이의 유사성이 복잡) 범주형 특성을 표현할 때도 유용하다.

### 7.2 기계 번역을 위한 인코더-디코더 네트워크

- 뭔가 좀 헷갈리는데
