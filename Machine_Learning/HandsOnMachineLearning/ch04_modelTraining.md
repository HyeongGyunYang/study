-	어떻게 작동하는지 잘 이해하고 있으면
	-	적절한 모델
	-	올바른 훈련 알고리즘
	-	작업에 맞는 좋은 하이퍼 파라미터
	-	디버깅 / 예외 처리
-	모델을 훈련시키는 방법
	-	직접 계산할 수 있는 공식을 사용하여 훈련 세트에 대해 비용 함수를 최소화하는 모델 파라미터를 해석적으로 구함
	-	경사 하강법으로 반복적인 최적화 방식을 사용하여 모델 파라미터를 조금씩 바꾸면서 비용함수를 훈련 세트에 대해 최소화
-	선행 수학
	-	vector, matrix, transpose, dot product, inverse matrix, partial derivative

### 1. Linear Regression

#### 1-1. Normal Equation

-	cost function을 최소화하는 theta를 찾기 위한 해석적인 방법

#### 1-2. 계산 복잡도

-	(n+1) 크기의 $`X^T*X`$의 역행렬을 구함
-	$`O(n^{2.4})~O(n^{3})`$
-	다행히 훈련셋의 샘플 수와 특성 수에는 선형적으로 증가한다.
-	선형회귀는 예측이 매우 빠르다.

### 2. 경사 하강법

-	특성이 매우 많고 훈련 샘플이 너무 많아 복잡도가 너무 클 때 유용하다.
-	Core Idea : min(cost func), 반복해서 파라미터 조정
-	방법
	-	random initialization
	-	calculating gradient
	-	gradient descent
	-	while cost == min(cost func) :
-	hyper parameter
	-	learning rate
		-	너무 크면, 튕긴다
		-	너무 작으면, 학습이 덜 된 채로 종료 / 학습 시간이 너무 오래 걸림
-	문제점
	-	optimize to local minimum
	-	scalig : 튕기거나 너무 오래 걸릴 수 있음
	-	비용함수 최소화 : 모델의 parameter space에서 최적의 파라미터 조합 찾기
	-	고차원이 되면(특성이 늘면) 어렵다
-	해결책
	-	scaling

#### 2-1. 배치 경사 하강법

-	각 모델 파라미터에 대해서 partial derivative 구하기
-	Batch Gradient Descent : 매 훈련 스텝에서 모든 훈련 데이터를 사용한다.
	-	큰 훈련셋에 느리다
	-	특성 수에 둔감해서 좋다
-	적절한 learning rate는 greed search를 해라
	-	이 때, 반복횟수를 제한

#### 2-2. 확률적 경사 하강법

-	Stocastic Gradient Descent
	-	매 스텝에서 딱 한 개의 샘플을 무작위로 선택하고 그 하나의 샘플에 대한 그래디언트를 계산
	-	장점
		-	빠르고
		-	큰 훈련셋도 훈련 가능
		-	오히려 불안정하기 때문에 local minimum의 위험이 적다
	-	단점 : 불안정
	-	해결책 : learning rate을 점진적으로 감소
-	Learning Schedule
	-	매 반복에서 learning rate을 결정하는 함수
-	Epoch
	-	SGD는 한 스텝에 샘플 하나씩 학습하므로, 한 학습 사이클을 의미함
	-	한 에퐄에서 보통 m번(샘플 수) 스텝을 학습한다.
-	샘플을 선택하는 법
	-	에퐄마다 섞은 뒤에 에퐄 훈련시키기
		-	늦게 수렴한다.

#### 2-3. mini-batch gradient descent

-	미니배치 : 임의의 작은 샘플셋
-	mini-batch GD : 한 스텝에 미니배치에 대해 그래디언트를 구한다.
-	장점
	-	GPU 연산
	-	미니배치를 어느 정도 크게 하면, SGD보다 파라미터 공간에서 덜 불규칙하게 움직인다.
-	단점
	-	SGD보다 로컬 미니멈에서 빠져나오기는 힘들 수도

#### - 배치 알고리즘 비교 pp172.

### 3. 다항 회귀

-	각 특성을 power하면 비선형 데이터도 학습할 수 있다.
-	sklearn.preprocessing.PolynomialFeatures
	-	주어진 차수까지 특성 간의 모든 교차항을 추가한다.(a*b 등도 추가함)

### 4. 학습곡선

-	얼마나 복잡한 모델을 사용해야 하는가? overfit? underfit?
-	CV해서 score를 살펴본다.
-	학습 곡선을 살펴본다.
	-	훈련셋과 검증셋의 모델 성능을 훈련셋 크기의 함수로 나타낸다.
	-	그래프 설명
		-	훈련셋에 한두개의 샘플이 있을 때는 훈련셋을 잘 추정하지만, 훈련셋의 수가 늘수록 오차가 커진다.
		-	검증셋은 반대
		-	어느 순간 평평해진다.
	-	underfit
		-	높은 오차에서 완만해진다.
		-	두 곡선 사이에 공간의 없다.
	-	good-fit
		-	훈련데이터의 오차가 선형 모델보다 낮다
		-	두 곡선 사이에 공간이 있다 : 검증셋보다 훈련셋에 더 잘 학습되어 있다. (overfit)
		-	하지만 데이터의 숫자가 커질수록 계속 근접한다. (goot-fit)

#### 편향 / 분산 트레이드오프

-	모델의 일반화 오차는 세 가지 다른 종류의 오차의 합으로 표현할 수 있다.
-	편향
	-	잘못된 가정으로 인한 것. (데이터가 실제는 2차인데, 선형으로 모델링했다던가)
	-	편향이 큰 모델은 훈련 데이터에 과소적합되기 쉽다.
-	분산
	-	훈련셋에 있는 작은 변동에 모델이 과도하게 민감하기 때문에 발생
	-	자유도가 높은 모델이 높은 분산을 가지기 쉽다.
	-	과대적합
-	줄일 수 없는 오차
	-	noise
	-	노이즈를 제거하는 거 외에 방법이 없다.

### 5. 규제가 있는 선형 모델

-	규제 : 과대적합 방지
-	예) 고차 다항 회귀 -> 차수 감소로 규제
-	선형 회귀 모델 : 모델의 가중치를 제한함으로써 규제한다.

#### 5-1. 릿지 회귀

-	Ridge Regression / Tikhonov Regulation
	-	규제가 추가된 선형 회귀
	-	$`\frac{1}{2} * l_{2}`$
	-	규제항이 비용함수에 추가된다
	-	모델의 가중치가 작게 유지되도록 규제된다.
	-	규제항은 훈련하는 동안에만 유지되고, 평가는 규제가 없는 모델로 평가
-	비용함수와 성능지표
	-	비용함수는 훈련에 사용되기 위해 미분 가능해야 한다.
	-	성능지표는 최종 목표에 가까워야 하기 때문에
-	릿지는 입력 특성의 스케일에 민감 : 사전 스케일링이 중요
-	하이퍼 파라미터 $`\alpha`$
	-	모델을 얼마나 많이 규제할지 조절
	-	0 : no regulation
	-	inf : 수평선
	-	값을 줄이면 : 분산은 줄고 편향은 커진다. (반대 아닌가?)
-	정규 방정식
-	SGD

#### 5-2. Lasso Regression

-	Least Absolute Shrinkage and Selection Operator Regression
	-	규제된 선형 회귀
	-	$`l_{1}`$을 사용한다
	-	특성
		-	덜 중요한 특성의 가중치를 완전히 제거하려 한다.
		-	자동으로 특성 선택을 하고 희소 모델을 만든다.
	-	라쏘는 $\theta_{i} = 0$일 때, 미분 가능하지 않다.
		-	subgradient vector를 사용하여 GD를 적용

#### 5-3. Elastic Net

-	릿지 + 라쏘
-	보통은 규제가 약간 있는 것이 좋으니까 릿지가 기본
-	쓰이는 특성이 몇 개뿐인 거 같으면 라쏘
-	특성 수가 샘플 수보다 많거나, 특성 몇 개가 강하게 연관되어 있으면 엘라스틱넷
	-	라쏘는 최대 샘플 수만큼의 특성을 선택한다.
	-	라쏘는 여러 특성이 강하게 연관되어 있으면 이들 중 임의의 특성 하나를 선택한다.

#### 5-4. Early Stopping

-	조기 종료는 검증에러가 최소에 도달하는 즉시 훈련을 멈추는 것
	-	그 이전은 underfit, 이후는 overfit

### 6. Logistic Regression

-	샘플이 특정 클래스에 속할 확률을 추정하는데 많이 쓰인다.
	-	이진분류기

#### 6-1. 확률 추정

-	방법
	1.	입력 특성의 가중치 합을 계산
	2.	결과값의 logistic을 출력 (sigmoid)

#### 6-2. 훈련과 비용 함수

-	log-loss를 사용
	-	정규 방정식이 없다.
	-	convex function이라서 global minimum 찾는 것을 항상 보장한다.

#### 6-3. 결정 경계

-	결정 경계
-	규제 : $`l*{1}, l*{2}`$

#### 6-4. Soft-Max Regression

-	Multinomial Logistic Regression
-	각 class에 대한 점수를 내고, soft-max를 적용해서 multi-class를 분류한다.
-	Soft-max / normalized exponential function
-	Cost Function
	-	Cross Entropy
-	sklearn의 로짓은 기본적으로 OvA
	-	multi_class = 'multinomial' : softmax regression
	-	solver = 'lbfgs' : softmax를 지원하는 알고리즘을 지정해야 한다.

##### Cross Entropy

-	Information Theory
-	KL devergence
