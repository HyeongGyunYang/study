-	프로젝트 진행단계
	1.	큰 그림을 봅니다
	2.	데이터를 구합니다
	3.	데이터로부터 통찰을 얻기 위해 탐색하고 시각화합니다.
	4.	머신러닝 알고리즘을 위해 데이터를 준비합니다.
	5.	모델을 선택하고 훈련시킵니다.
	6.	모델을 상세하게 조정합니다.
	7.	솔루션을 제시합니다.
	8.	시스템을 론칭하고 모니터링하고 유지 보수합니다.

### 1. 실제 데이터로 작업하기

-	[UC Irvine](http://archive.ics.uci.edu/ml/)
-	[Kaggle](http://www.kaggle.com/datasets)
-	[Amazon AWS](http://aws.amazon.com/ko/datasets)
-	공개 데이터 저장소가 나열되어 있음
	-	[dataportal](http://dataportals.org/)
	-	[open data monitor](http://opendatamonitor.eu/)
	-	[quandl](http://quandl.com)
-	인기 있는 공개 데이터 저장소가 나열되어 있는 다른 페이지
	-	[위키백과](https://goo.gl/SJHN2k)
	-	[quora](http:/goo.gl/zDR78y)
	-	[sub reddit](http://www.reddit.com/r/datasets)

### 2. 큰 그림 보기

-	머신러닝 프로젝트 체크리스트를 준비

#### 2-1. 문제 정의

-	질문을 한다.
	1.	비즈니스의 목적이 정확히 무엇인가??
		-	모델을 어떻게 사용해 이익을 얻을 것인가
		-	문제를 어떻게 구성할지
		-	어떤 알고리즘을 선택할지
		-	모델 평가에 어떤 성능 지표를 사용할지
		-	모델 튜닝을 위해 얼마나 노력을 투여할지
	2.	현재 솔루션은 어떻게 구성되어 있나??
		-	문제 해결 방법에 대한 정보 & 참고 성능
-	Pipeline : sequential data process component
	-	보통 비동기적으로 component들이 동작
-	문제 정의
	-	지도 / 비지도 / 강화 ?
	-	어떤 모델?
	-	배치 / 온라인?
-	데이터가 매우 크면 MapReduce 기술을 사용하여 배치 학습을 여러 서버로 분할
	-	온라인 학습

#### 2-2. 성능 측정 지표 선택

-	회귀 문제니까 RMSE(root mean square error)
	-	$`RMSE(X, h) = \sqrt{\frac{1}{m}\sum_{i=1}^m (h(x^{(i)})-y^{(i)})^2}`$
-	이상치가 너무 크면 MAE(mean absolute deviation)
-	회귀는 회귀곡선과 샘플과의 거리를 오차로 삼는다.

#### 2-3. 가정 검사

-	pipeline의 다음 단계에서 cat data로 결과값을 다룬다면, 회귀가 아니라 분류 문제가 된다.

### 3. 데이터 가져오기

#### 3-1. 작업환경 만들기

-	venv

#### 3-2. 데이터 다운로드

-	데이터를 추출하는 간단한 함수를 만들어서 자동화하자
-	주기적으로 데이터가 변한다면, 스케줄링하여 자동 실행할 수도 있다.
-	실제 환경에서는 DB에 데이터가 들어 있고, 보안자격과 접근권한, 법적 제한 등도 살펴야

#### 3-3. 데이터 구조 훑어보기

-	유용한 함수들

	-	info() : overall
	-	value_counts() : cat
	-	describe() : numeric
	-	hist() : nemeric

-	데이터에 대한 이해

	-	전처리된 데이터 : 어떤 방식으로 전처리 되어 있는지 이해
	-	상/하한이 제한된 데이터 핸들링
		-	정확한 값을 가지는 데이터를 다시 구한다.
		-	상/하한에 속하는 데이터만 계산한다.
	-	특성들의 스케일링 다를 경우 -> 뒷장
	-	꼬리가 두꺼운 분포 : 패턴을 찾기 어렵다. -> 종모양이 되도록 정규화

-	데이터를 더 깊게 들여다보기 전에 테스트셋을 떼어야 한다.

#### 3-4. 테스트 세트 만들기

-	Data Snooping Bias
	-	이 단계에서 테스트 셋을 분리하지 않으면, 전체 데이터에 스스로 overfit되어서 모델을 선택한다.
-	단순히 무작위로 분리해놓으면 모델을 학습시킬 때마다 테스트가 바뀐다.
	-	random seed 설정
	-	테스트셋을 따로 저장
	-	샘플의 해시값을 이용해서 test/training set을 나눈다.(온라인에도 적용 가능!!)
-	만약, 데이터가 충분히 크지 않다면...
	-	Straified sampling 계층적 샘플링
	-	중요한 attr을 기준으로 계층을 나눠서 모집단을 잘 대표할 수 있게 하자.

### 4. 데이터 이해를 위한 탐색과 시각화

-	훈련 세트에 대해서만 탐색
-	훈련 세트가 너무 크면 탐색을 위한 조각을 따로 떼어낼 수도 있다.
-	훈련 세트를 손상시키지 않게 복사본을 사용하자

#### 4-1. 지리적 데이터 시각화

-	매개변수를 다양하게 조절해보면서 두드러지는 패턴을 찾아보자

#### 4-2. 상관관계 조사

-	수치조사
	-	상관계수 : 선형적인 관계만 조사
	-	비선형관계를 수치로 조사하려면 다른 방법을 써야한다
	-	머피를 참조하자
-	산점도 그리기 : numeric
-	이상패턴(상/하한이 제한되었다던가)이 있으면 제거해주는 게 좋다.

#### 4-3. 특성 조합으로 실험

-	더 의미있는 특성을 만들어 보자

### 5. 머신러닝 알고리즘을 위한 데이터 준비

-	자동화 해야 하는 이유
	-	데이터셋이 추가되더라도 변환을 손쉽게 할 수 있다.
	-	향후 프로젝트에 사용할 수 있는 변환 라이브러리를 점진적으로 구축
	-	실제 시스템에서 알고리즘에 새 데이터를 주입하기 전에 변환시키는데 이 함수를 사용
	-	여러 가지 데이터 변환을 쉽게 시도해볼 수 있고, 어떤 조합이 가장 좋은지 확인한는데 편리
-	훈련 데이터 복사
-	레이블 분리

#### 5-1. 데이터 정제

-	머신러닝 알고리즘은 대부분 누락된 특성을 다루지 못한다.
-	누락 데이터 핸들링

	-	pd.dropna, pd.drop, pd.fillna
	-	해당 샘플을 제거
	-	해당 특성을 제거
	-	값을 채운다 : 0/mean/median 등
	-	**만약 누락된 값을 사용한다면 이 값을 상수로 테스트와 추가 데이터에도 사용한다.**

	<pre><code>
	imputer = sklearn.preprocessing.Imputer(strategy='median') #imputer 객체 생성
	imputer.fit(trainingSet) # imputer 객체 학습
	transformedData = imputer.transform() # transform
	</code></pre>

##### 사이킷런의 설계철학

-	일관성 : 모든 객체가 일관되고 단순한 인터페이스를 공유
	-	estimator
	-	데이터셋을 기반으로 일련의 모델 파라미터를 추정하는 객체
	-	하나의 매개변수로 하나의 데이터셋만 전달한다
	-	/지도학습 알고리즘에서는 두 개의 매개변수를 전달
	-	추정 과정에 필요한 다른 매개변수들은 모두 하이퍼파라미터로 간주
	-	fit()
	-	transformer
	-	데이터셋을 변환하는 추정기
	-	학습된 모델 파라미터에 의해 변환이 결정
	-	transform(), fit_transform()
	-	predictor
	-	주어진 데이터셋에 대한 예측을 만든다.
	-	predict()
	-	score() : 테스트셋을 사용해 예측의 품질을 점수화
	-	predict_proba(), decision_function() : 예측의 확신을 측정
-	검사 기능
	-	모든 추정기의 하이퍼파라미터는 public
	-	학습된 모델 파라미터도 @\_형태로 public 제공
-	클래스 남용 방지 : numpy array나 scipy sparse matrix로 표현
-	조합성 : 기존의 구성요소를 최대한 재사용
-	합리적인 기본값 : 기본 시스템을 빨리 만들 수 있다.

#### 5-2. 텍스트와 범주형 특성 다루기

-	pd.factorize() : text형 cat -> int형 cat으로 mapping
-	카테고리별 이진특성 one-hot encoding
	-	sklearn.preprocessing.OneHotEncoder().fit_transform()
		-	np.reshape() :
		-	scipy sparse matrix
	-	pd.get_dummies()
	-	sklearn.preprocessing.CategoricalEncoder()

#### 5-3. 나만의 변환기

-	Scikit Learn : 상속이 아닌 duck typing을 지원
	-	덕 타이핑 : 객체의 속성이나 메서드가 객체의 유형을 결정
-	scikit-learn과 연동하는 user defined function을 만드려면, 메소드를 구현하면 된다.
	-	Mixin 상속받기
-	데이터 준비 단계를 자동화하자!!!

#### 5-4. 특성 스케일링

-	트리는 scaling이 안되어있어도 잘 작동한다.
-	min-max scaling / normalization : 신경망이 선호한다.
-	standardization : 이상치에 영향을 덜 받는다.
	-	sklearn.preprocessing.StandardScaler
-	모든 변환기에서 스케일링은 훈련 데이터에 대해서만 fit을 하고, 훈련셋과 테스트셋에 transform해야 한다.

#### 5-5. 변환 파이프라인

-	sklearn.pipeline.Pipeline : 연속된 변환을 순서대로 처리할 수 있도록 도와주는 클래
-	pandas로 이어지는 ColumnTransformer
-	sklearn.pipeline.FeatureUnion

### 6. 모델 선택과 훈련

#### 6-1. 훈련 세트에서 훈련하고 평가하기

-	underfitting과 overfitting 여부에 따라 조정

#### 6-2. 교차 검증을 사용한 파악

-	k-fold cross-validation
	-	k의 subset으로 random split한 뒤, k번 train & validate
	-	k개의 score return: score의 의미를 정확히 확인해두자
	-	sklearn.model_selection.cross_val_score
-	cross validation은 비용(시간)이 비싸다ㅠ
-	hyper-parameter tuning에 시간을 쏟기 전에, 2~5개 정도의 가능성 있는 모델을 선택하는 것이 목적이다.

### 7. 모델 세부 튜닝

#### 7-1. greed search

-	hyper-parameter tuning을 자동으로 모든 방법을 시도해본다.
-	sklearn.model_selection.GridSearchCV

#### 7-2. random search

-	hyper-parameter의 탐색 공간이 커지면
-	장점
	-	랜덤 탐색을 n회 시도하면, 각기 다른 n개의 값을 탐색한다.
	-	단순히 반복 횟수를 조절하는 것만으로 하이퍼파라미터 탐색에 투입할 컴퓨팅 자원을 제어할 수 있다.

#### 7-3. Ensemble

-	개개의 모델이 각기 다른 형태의 오차를 만들 때 더욱 유용하다.

#### 7-4. 최상의 모델과 오차 분석

-	최상의 모델을 분석하면서 문제에 대한 좋은 통찰을 얻는 경우가 많다!!
-	예를 들면, importance feature

#### 7-5. 테스트셋으로 시스템 평가하기

### 8. 론칭, 모니터링, 그리고 시스템 유지 보수

-	입력 데이터 소스를 시스템에 연결하고 테스트 코드를 작성
-	일정 간격으로 시스템의 실시간 성능을 체크하고, 모니터링 코드를 작성
-	오작동뿐만 아니라 성능 감소를 잡아내기 위해서
	-	새로운 데이터를 사용해 주기적으로 훈련시키지 않으면 데이터가 오래됨에 따라 모델도 함께 낙후되는 것이 일반적이다.
-	시스템 성능 평가
	-	시스템의 예측을 샘플링해서 평가
	-	이 과정에는 사람의 분석이 필요하다.
-	시스템 입력 데이터 품질 평가
	-	예) 오작동하는 센서가 예측 불허의 값을 보낸다거나 다른 팀의 출력이 멈춰 있는 경우
	-	시스템의 입력을 모니터링한다.
	-	특히! 온라인 학습 시스템에서는 입력을 모니터링하는 일이 중요하다.
-	새로운 데이터를 사용해 정기적으로 모델을 훈련시켜야 한다.
	-	자동화!
	-	온라인 학습이라면, 일정 간격으로 시스템의 상태를 스냅샷으로 저장해서 이전 상태로 쉽게 되돌릴 수 있도록 해야 한다.

### 9. 직접 해보세요!

-	머신러닝 프로젝트의 단계
	1.	데이터 준비 단계
	2.	모니터링 도구 구축
	3.	사람의 평가 파이프라인 세팅
	4.	주기적인 모델 학습 자동화
-	고수준 알고리즘을 탐색하느라 시간을 모두 허비해서 전체 프로세스 구축에 충분한 시간을 투자하지 못하는 것보다, 서너 개의 알고리즘만으로라도 전체 프로세스를 올바로 구축하는 편이 더 낫다.
