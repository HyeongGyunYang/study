original_w = [...] # 다른 프레임워크로부터 가중치를 읽는다.
original_b = [...] # 다른 프레임워크로부터 편향을 읽는다.

X = tf.placeholder(tf.float32, shape=(None, n_inputs), name="X")
hidden1 = tf.layers.dense(X, n_hidden1, activation=tf.nn.relu, name="hidden1")
[...] # 모델의 나머지 부분을 만든다.

# hidden1 변수에 할당 노드를 구한다.
graph = tf.get_default_graph()
assign_kernel = graph.get_operation_by_name("hidden1/kernel/Assign")
assign_bias = graph.get_operation_by_name("hidden1/bias/Assign")
init_kernel = assign_kernel.inputs[1]
init_bias = assign_bias.inputs[1]

init = tf.global_variables_initializer()

with tf.Session() as sess:
    sess.run(init, feed_dict={init_kernel: original_w, init_bias: original_b})
    [...] # 새로운 작업에 대한 모델을 훈련시킨다.
