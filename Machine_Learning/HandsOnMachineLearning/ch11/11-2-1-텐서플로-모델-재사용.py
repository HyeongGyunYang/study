saver = tf.train.import_meta_graph("./my_model_final.ckpt.meta")

'''
훈련 대상인 연산과 텐서를 직접 지정
'''
X = tf.get_default_graph().get_tensor_by_name("X:0")
y = tf.get_default_graph().get_tensor_by_name("y:0")
accuracy = tf.get_default_graph().get_tensor_by_name("eval/accuracy:0")
training_op = tf.get_default_graph().get_operation_by_name("GradientDescent")

# 연산의 이름 찾기
for op in tf.get_default_graph().get_operations():
    print(op.name)

# 다른 사람의 모델을 재사용할 때
X, y, accuracy, training_op = tf.get_collection("my_important_ops")

# saver 객체를 사용해 모델의 상태를 복원하고 나만의 데이터를 가지고 훈련을 계속한다.
with tf.Session() as sess:
    saver.restore(see, "./my_model_final.ckpt")
    [...] # 자신만의 데이터로 모델 훈련하기
