[...] # 이전과 같은 은닉층 1~3을 가진 새로운 모델을 만든다.

reuse_vars = tf.get_collection(tf.GraphKeys.GLOBAL_VARIABLES, scope="hidden[123]") # re
restore_saver = tf.train.Saver(reuse_vars) # 1~3층 복원

init = tf.global_variables_initializer() # 새 변수, 구 변수 모두 초기화
saver = tf.train.Saver() # 새로운 모델 저장

with tf.Session() as sess:
    init.run()
    restore_saver.restore(sess, "./my_model_final.ckpt")
    [...] # 모델훈련
    save_path = saver.save(sess, "./my_new_model_final.ckpt")
