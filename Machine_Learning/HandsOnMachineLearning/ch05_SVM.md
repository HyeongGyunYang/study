-	매우 강력
-	선형/비선형 분류, 회귀, 이상치 탐색에 사용
-	복잡한 분류 문제에 적합
-	작거나 중간 크기의 데이터셋에 적합

### 1. 선형 SVM 분류

-	Large Margin Classification
	-	클래스 사이에 가장 폭이 넓은 도로를 찾는 일
-	Support Vector : 도로 경계에 위치한 샘플
	-	도로 바깥쪽에 샘플을 아무리 추가해도 도로는 변하지 않는다.
-	클래스에 대한 확률을 제공하지 않는다.
-	단점
	-	특성의 스케일에 민감

#### 1-1. 소프트 마진 분류

-	Hard Margin Classification : 모든 샘플이 도로 바깥쪽에 올바르게 분류
	-	이상치가 있으면 올바르게 작동하지 않는다.
-	Soft Margin Classification
	-	도로의 폭을 가능한 넓게 유지하는 것과 Margin Violation 사이의 균형점 찾기
-	sklearn.svm.LinearSVC
	-	c hyperparameter : 마진오류의 균형 결정
	-	가장 빠름
-	SVC모델 : 큰 훈련셋에서 속도가 매우 느림
-	SGDClassifier(loss='hinge', alpha = 1/(m*C)) 사용
	-	선형 SVM 분류기를 훈련시키기 위해 일반적은 SGD를 적용
	-	데이터셋이 너무 커서 메모리에 적재할 수 없거나 (외부 메모리 훈련)
	-	온라인 학습으로 분류 문제를 다룰 때 유용

### 2. 비선형 SVM 분류

-	다항 특성 등을 추가해서 선형적으로 분류되게 한다.
-	유사도 특성을 추가
-	커널 선택
	-	일단 선형 커널을 우선 시도
	-	훈련셋이 너무 크지 않다면 GRBF 커널
	-	시간과 컴퓨팅 성능이 충분하다면, CV와 greed탐색으로 더 시도해본다.

#### 2-1. 다항식 커널

-	Kernel Trick
	-	실제로는 특성을 추가하지 않으면서 다항식 특성을 많이 추가한 것과 같은 결과를 얻는다.
-	sklearn.svm.SVC(kernel='poly', degree=)
-	overfit, underfit에 따라 차수를 조정

#### 2-2 유사도 특성 추가

-	각 샘플이 특정 랜드마크와 얼마나 닮았는지 측정하는 유사도 함수(Similarity Function)로 계산한 특성을 추가하는 것.
-	랜드마크 선택
	-	데이터셋의 모든 샘플 위치에 랜드마크 설정
		-	장점 : 훈련 세트가 선형적으로 구분 가능
		-	단점 : n개의 특성을 가진 m개의 샘플이 m개의 특성을 가진 m개의 샘플로 변환

#### 2-3. 가우시안 RBF 커널

-	Gaussian Radical Basis Function
-	유사도 특성을 많이 추가하는 것과 같은 결과를 실제로 특성을 추가하지 않고 얻을 수 있다.
-	$\gamma$ : 규제의 역할
	-	작으면 : 넓은 종 모양, 결정 경계가 부드러워짐, overfit이면 감소
-	$C$
-	다른 커널은 거의 사용되지 않는다.
-	STring kernel : 가끔 텍스트 문서나 DNA서열을 분류할 때 사용
	-	string subsequence kernel
	-	Levenshtein distance

#### 2-4. 계산 복잡도

-	LinearSVC : $O(m \times n)$, liblinear 기반
-	SVC : $O(m^2 \times n) ~ O(m^3 \times n)$, libsvm 기반
-	SGDClassifier : $O(m \times n)$

### 3. SVM 회귀

-	제한된 마진오류 안에서 도로 안에 가능한 한 많은 샘플이 들어가도록 학습
-	$\epsilon$ : 도로의 폭 조절
-	$\epsilon$에 민감하지 않다 : 마진 안에서는 훈련 샘플이 추가되어도 모델의 예측에는 영향이 없다.
-	LinearSVR의 적용
-	SVR의 적용 : 커널트릭 제공

### 4. SVM 이론

-	$\theta$ : 편향 $\theta*{0}$과 입력 특성의 가중치 $\theta*{1}$ ~ $\theta_{m}$의 벡터
-	모든 샘플의 편향에 해당하는 입력값 $x_{0} = 1$
-	편향을 b라 하고 특성의 가중치 벡터를 w라 한다.

#### 4-1. 결정 함수와 예측

-	선형 SVM은 단순히 결정함수 $\mathbf{w}^T \times \mathbf{x} + b$를 계산해서 새로운 샘플의 클래스를 예측

#### 4-2. 목적 함수

-	결정함수의 기울이 : 가중치 벡터$||\mathbf{w}||$의 노름
-	가중치 벡터가 작을수록 마진은 커진다.
-	하드 마진 선형 SVM의 목적 함수를 제약이 있는 최적화로 표현할 수 있다.
	-	$min_{w,b} \frac{1}{2} \mathbf{w}^T \times \mathbf{w}$
	-	조건 : $i = 1, 2, \dots, m, t^{(i)}(\mathbf{w}^T \times \mathbf{x}^{(i)}+b)>=1$
-	소프트 마진 분류기의 목적 함수를 구성하려면 각 샘플에 대해 Slack var $\varsigma^{(i)}>=0$를 도입
	-	$\varsigma^{(i)}$는 i번째 샘플이 얼마나 마진을 위반할지 결정
	-	마진 오류를 최소화하기 위해 slack을 최소화 <-> 마진을 크게 하기 위해 목적 함수를 최소화
	-	$C$가 트레이드오프를 정의
	-	$min*{w,b,\varsigma} \frac{1}{2} \mathbf{w}^T \times \mathbf{w}+C\sum_{i=1}^{m} \varsigma^{(i)}$

#### 4-3. 콰드라틱 프로그래밍

-	Quadratic Programming : 선형 제약 조건이 있는 볼록함수의 이차 최적화 문제 (하드 마진, 소프트 마진)

#### 4-4. 쌍대 문제

-	원문제 Primal problem라는 제약이 있는 최적화 문제는 쌍대 문제 dual problem 이라는 깊게 관련된 다른 문제로 표현할 수 있다.
-	쌍대문제
	-	일반적으로 원 문제 해의 하한값
	-	특정 조건에서는 원 문제와 똑같은 해를 제공 : SVM은 조건 만족
		-	목적 함수가 볼록 함수이고, 부등식 제약 조건이 연속 미분 가능하면서 볼록 함수
-	그래서 둘 중 하나를 골라서 풀 수 있다.

#### 4-5. 커널 SVM

-	2차원 데이터셋에 2차 다항식 변환을 적용하고 선형 SVM분류기를 변환된 이 훈련셋에 적용한

#### 4-6. 온라인 SVM

-	gradient descent를 사용하면 QP보다 훨씬 느리게 수렴
	-	SGDClassifier에 loss='hinge'로 설정하면 선형 SVM이 된다.
-	대규모의 비선형 문제라면 신경망 알고리즘을 고려해보는 게 어때?
-	C++, Matlab에 구현되어 있다.

##### hinge loss

-	max(0,1-t)함수
-	$t>=1$이면 0
-	$t<1$이면 도함수의 기울기 -1
-	$t=1$에서 미분 불가능하지만 서브그래디언트를 사용해서 GD가능
