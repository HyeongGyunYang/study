## 1. 연산 그래프

### 1.1 연산 그래프

### 1.2 연산 그래프의 장점

- 의존성
  - 직접 의존
  - 간접 의존
- 그래프를 보면 항상 각 노드의 모든 의존관계를 파악할 수 있다.
- 모델의 수행 단위 간의 의존관계를 찾아내어 컴퓨팅 자원에 연산을 분산시킨다.

## 2. graph, session, fetch

### 2.1 그래프 만들기

### 2.2 세션을 만들고 실행하기

- Session 객체는 텐서플로 API의 일부
  - 파이썬 객체와 데이터, 객체의 메모리가 할당되어 있는 실행 환경 사이를 연결
  - 중간 결과를 저장
  - 최종 결과를 작업 환경으로 리턴
- 연산 작업이 마무리되면 세션을 닫는 습관을 들이자
  - 세션에서 사용하는 자원을 해제

### 2.3 그래프의 생성과 관리

- [with context manager를 통해 여러 그래프 다루기](ch03/graphInit.ipynb)

### 2.4 fetch

- fetch : 연산하고자 하는 그래프의 요소
  - <code>sess.run()</code>에 하나의 특정 노드의 실행을 요청한 것을 떠올린다.
- [<code>sess.run()</code>에 list를 넘기자](ch03/fetch.ipynb)

## 3. 텐서의 흐름

### 3.1 노드는 연산, 변은 텐서 객체

- 노드를 만들 때, 실제로는 연산 인스턴스가 생성된다.
  - 단지 흐름으로서 참조될 뿐
  - 이 "handle"은 그래프에서 변으로 간주, tensor object

#### 3.1.1 소스 연산을 통한 속성 설정

- source operation : 이전에 처리된 입력을 사용하지 않고, ***데이터를 생성***하는 연산

### 3.2 data type

- [dtype, 형변환](ch03/dataType.ipynb)
- [tf에서 사용 가능한 dtype](https://www.tensorflow.org/api_docs/python/tf/dtypes/DType)

### 3.3 텐서 배열과 형태 shape

- tensor (math)
  - $`1\times1`$ : scalar
  - $`1\times n`$ :vector
  - $`n\times n`$ : matrix
  - $`n^n`$: n-d array
- [shape](ch03/shape.ipynb)
- [상수 초기화](ch03/randomInit.ipynb) : 난수 생성, 정규분포, 절단정규분포, 균등 분포
  - .InteractiveSession() : 평가를 한 뒤에, 세션 객체를 계속 참조하지 않고도 데이터를 계속 확인할 수 있다.
    - VS. .Session() : 연산 실행에 필요한 세션을 저장할 변수를 따로 지정하지 않아도 된다.
- [초기화 함수들](ch03/initFunc.ipynb)

#### 3.3.1 행렬곱

- <code>tf.matmul()</code>
- [행렬 연산](ch03/matrixOp.ipynb)

### 3.4 Name

- 텐서 객체의 이름은 단순히 해당 연산의 이름일뿐
- 이름 : 인덱스

#### 3.4.1 name scope

- 용도 : 크고 복잡한 그래프를 처리해야 하는 경우, 이를 쉽게 추적하고 관리하기 위해 노드를 묶는다.
- 의미 : 노드를 이름별로 계층적으로 그룹화

- [name and name scope](ch03/name.ipynb)

## 4. Var, Placeholder, optimizer

### 4.1 variable

- 변수는 그래프에서 고정된 상태를 유지할 수 있다.
  - 다른 텐서 객체는 리필된다.
- 주어진 모델의 매개변수로 사용된다.
- 변수 또한 모델이 실행될 때만 계산된다.
- [변수](variable.ipynb)
- 같은 변수를 재사용 : <code>tf.get_Variables()</code>
  - [설명 - 1.1.1 변수공유](appendixA.md)
  - [코드](A/reuse.ipynb)

### 4.2 placeholder

- ph : 나중에 데이터로 채워질 빈 변수
  - 그래프가 시작되는 시점에 입력 데이터를 밀어 넣는 데 사용
- 입력 데이터는 딕셔너리 형태로 .run()에 전달
  - 딕셔너리의 키는 ph변수의 이름에 대응
  - 딕셔너리의 값은 list, np.array 형태
- [예제](ch03/placeholder.ipynb)

### 4.3 Optimization

#### 4.3.1 예측을 위한 학습

- 모델 선정

#### 4.3.2 손실 함수 정의

#### 4.3.3 MSE, cross entropy

- cross entropy : 두 분포 사이의 유사성을 재는 척도
  - 실제 클래스와 모델에서 제시한 클래스를 비교

#### 4.3.4 gradient descent

#### 4.3.5 데이터 생성 방법

- SGD + mini-batch

#### 4.3.6 텐서플로의 GD

#### 4.3.7 예제

##### 4.3.7.1 [선형회귀](ch03/linearRegression.ipynb)

##### 4.3.7.2 [로지스틱 회귀](ch03/logisticRegression.ipynb)
