## 2. 동사구

### 2.3 동사의 형태와 종류

#### 2.3.1 조동사 + 동사원형

-	조동사 다음에는 반드시 동사원형이 와야한다.

	-	not, 부사가 오는 경우 틀리지 않도록 주의
	-	조동사처럼 쓰이는 표현(역시 동원) : ought to, had better, would like to, used to, have to, be able to, be going to

-	부정문이나 도치 구문을 만드는 조동사 다음에도 반드시 동사원형이 와야 한다.

#### 2.3.2 진행형, 수동형, 완료형

-	조동사 다음 be는 원형 씀
-	be, have 뒤에는 원형이 올 수 없다.
	-	have가 조동사로 쓰일 때만

#### 2.3.3 혼동하기 쉬운 자동사와 타동사, 3형식과 4형식 동사

-	자동사가 목적어를 갖기 위해서는 꼭 전치사가 있어야 한다. 타동사는 전치사 없이 바로 목적어를 갖는다.
	-	자동사 : speak to, talk to, talk about, account for, reply to, react to, respond to, agree with(to, on), object to, consent to, participate in, arrive at, wait for
	-	타동사 : mention, discuss, instruct, explain, address, answer, approve, oppose, attend, reach, await
	-	*목적어 앞에 전치사가 있으면 자동사를 골라라*
-	목적어(that 절)를 하나만 가지는 3형식 동사와, 2개를 가지는 4형식 동사 구분해서 사용
	-	3형식 동사 : say, mention, announce, suggest, propose, recommend, explain, describe (to me)
	-	4형식 동사 : tell, inform, notify, advise, assure, convince

#### 2.3.4 제안, 요청, 의무의 주절을 뒤따르는 that절엔 동사원형이 와야 한다.

-	동사 : suggest, propose, recommend, request, ask, require, demand, insist, command, order
-	형용사 : imperative, essential, necessary, important
-	명사 : advice
-	위의 동형명이 주절에 쓰였더라도, 제안/요청/의무를 의미하지 않는다면, 동원을 쓸 수 없다.

### 2.4 주어와의 수일치

#### 2.4.1 단수 주어 뒤에는 단수동사, 복수 주어 뒤에는 복수 동사

-	단수 주어 뒤에는 단수 동사를, 복수 주어 뒤에는 복수 동사를 쓴다.
	-	단수주어 : 단수 가산명사, 불가산명사
	-	복수주어 : 복수 가산명사
	-	-s로 끝나지만 단수 명사
	-	학문명 : economics, statistics, mathematics
	-	고유 명사 : the United States, Times, world Satellite Atlas
	-	기타 명사 : news, woods, measles
-	동명사구/명사절 주어는 단수 주어로 취급하여 단수 동사를 쓴다.
-	주어와 동사 사이에 있는 수식어 거품은 동사의 수 결정에 아무런 영향을 주지 않는다.

#### 2.4.2 주어로 쓰인 수량 표현, 부분/전체 표현과의 수일치

-	단수 취급되는 수량 표현에는 단수 동사를, 복수 취급되는 수량 표현에는 복수 동사를 쓴다.
	-	단수 취급되는 수량 표현 : one, each, every+n., the number of + 복수, nobody, no one, nothing, somebody, someone, something, anybody, anyone, anything, everybody, everyone, everything
	-	복수 취급되는 수량 표현 : many/several/few/both + (of the +) 복수, a number of + 복수, a couple/variety of + 복수
	-	many, several, few, both 자체로 쓰여도 복수
-	부분이나 전체를 나타내는 표현이 주어로 쓰이면 of 뒤의 명사에 동사를 수일치시킨다.
	-	all, most, any, some, half, a lot(s), part, the rest, the bulk, percent, 분수
-	[수량 표현 부분의 수일치는 형용사와 함께 학습해 둘 것](###4.12-형용사)

#### 2.4.3 접속사로 연결된 주어와의 수일치

- 접속사 and로 연결된 주어는 복수동사를 쓴다.
	- both a and b도 복수
	- 두 사항을 연결하는 의미가 있는 in addition to, along with, together with는 동사의 수에 영향을 주지 않는 수식어 거품

- 접속사 or로 연결된 주어의 수일치는 뒤에 일치시킨다.
	- 뒤에 일치시키는 동사들 :
		either A or B, Neither A nor B, Not A but B, Not only A but (also) B, B as well as A

#### 2.4.4 주격 관계절의 선행사와 동사의 수일치

- 주격 관계절의 동사는 선행사와 수일치한다.
	- 관계절의 동사는 관계절의 주어나 선행사와 수, 태가 맞아야 한다.
- 선행사 뒤에 있는 수식어 거품 속의 명사를 선행사와 혼동해서는 안된다.

### 2.5 능동태, 수동태

#### 2.5.1 능동태와 수동태의 구별

- 능동태에서 반드시 목적어를 가지는 타동사의 경우, 수동태에서는 목적어를 가지지 않는다.
- to 부정사의 동사와 관계절의 동사의 능동태/수동태 구별도 목적어 여부에 따라 결정한다.

#### 2.5.2 4형식/5형식 동사의 수동태

- 목적어를 두 개 가지는 4형식 동사가 수동태가 될 때는, 목적어 중 한 개가 수동태 동사 뒤에 남는다.
	- 일반적인 4형식 동사들(give, send, grand)의 직목이 주어 자리로 가면 **to(전치사) + 간목**
	- (buy, make, get, find, build) : **for(전치사)+간목**
- 목적어와 목적격 보어를 가지는 5형식 동사가 수동태가 될 때는, 목적격 보어가 수동태 동사 뒤에 남는다.
	- 목적격 보어를 가지는 5형식 동사가 수동태가 될 때는, 목적격 보어가 수동태 동사 뒤에 남는다.
	- 명사구를 목적격 보어로 가질 경우,
		- 수동태 문장의 목적어처럼 보일 수가 있으므로 헷갈리지 말 것.
		- 명사구를 목적격 보어로 취하는 5형식 동사: consider, call, elect
	- to 부정사를 목적격 보어로 가지는 5형식 동사 : advise, ask, expect, invite, remind, require
	- consider : 3형식으로 쓰일 때(고려하다), 5형식으로 쓰일 때(여기다, 간주하다)

#### 2.5.3 감정 동사의 능동태/수동태 구별

- 주어가 감정의 원인이면 능동태, 주어가 감정을 느끼면 수동태
- 재미/만족 : interest, excite, amuse, please, fascinate, encourage, satisfy, thrill
- 낙담/불만족 : disappoint, discourage, dissatisfy, depress, tire, trouble, frustrate
- 당황/충격 : bewilder, shock, surprise

#### 2.5.4 수동태 동사 숙어

- 수동태 동사 + 전치사 :
	be amused at, be pleased with, be delighted with, be satisfied with, be gratified with, be disappointed at,
	be surprised at, be alarmed at, be astonished at, be frightened at, be shocked at, be interested in,
	be worried about, be concerned about/over, be bored with, be tired of, be convinced of,
	be involved in, be engaged in, be associated with, be related to, be divided into, be finished with,
	be absorbed in, be indulged in, be devoted to, be dedicated to, be exposed to,
	be equipped with, be covered with, be crowded with, be based on, be credited with
- 수동태 동사 + to 부정사 :
	be asked to, be requested to, be told to, be reminded to, be encouraged to, be invited to,
	be required to, be urged to, be advised to, be warned to, be intended to, be expected to,
	be allowed to, be permitted to, be prepared to, be supposed to, be scheduled to, be projected to

### 2.6 시제와 가정법

#### 2.6.1 현재/과거/미래

- 현재 시제는 현재의 상태나 반복되는 동작, 일반적인 사실을 표현한다.
- 과거 시제는 이미 끝난 과거의 동작이나 상태를 표현한다.
- 미래 시제는 미래의 상황에 대한 추측이나 의지를 표현한다.
	- will, be going to, 현재시제 + 부사(구)
- 시간이나 조건을 나타내는 종속절에서는 미래 시제 대신 현재 시제를 쓴다.
	- when, before, after, as soon as, once, if, unless, by the time
	- 시간,조건을 나타내는 종속절과 함께 쓰인 주절에는 미래 시제를 그대로 쓴다.

#### 2.6.2 현재 진행/과거 진행/미래 진행

- 현재 진행 시제는 현재 시점에 진행되고 있는 일을 표현한다.
	- 반복적으로 일어나는 행동을 나타내기 위해서는 현재 시제를 쓴다.
	- 바로 지금 일어나고 있는 행동을 나타내기 위해 현재 진행을 쓴다
- 과거 진행 시제는 특정한 과거 시점에 진행되고 있던 일을 표현
- 미래 진행 시제는 특정한 미래 시점에 진행되고 있을 일을 표현
- 진행 시제로 쓸 수 없는 동사
	- 감정동사 : surprise, shock, hate, prefer, want, believe
	- 상태동사 : include, need, be, know, exist, consist

#### 2.6.3현재완료/과거완료/미래완료

- 현재완료시제 : 과거에 시작된 일이 현재까지 계속되거나, 지금 막 완료된 일, 과거의 경험, 과거에 발생한 일이 현재까지 영향을 미치는 것
	 - 계속 : for, since
	 - 완료 : already, just
	 - 경험 : ever, never, yet
	 - 현재완료 + 진행 : 현재 그 동작이 진행되고 있다는 의미가 더해진다.
- 과거 완료 시제 : 과거의 특정 지점 이전에 발생한 일
- 미래 완료 시제 : 미래 특정 시점 이전에 발생한 동작이 미래의 그 시점에 완료될 것임을 표현

#### 2.6.4 시제일치

- 과거, 현재, 현재완료, 미래시제와 함께 자주 쓰이는 표현들
	- 과거 완료 : by the time + 과거
	- 과거 : yesterday, recently, 시간표현+ago, last+시간표현, in+과거
	- 현재 : usually, often, each month(year), generally
	- 현재 완료 : since+과거, in the last(past)~, recently
	- 미래, 미래 완료 : tomorrow, next + 시간표현, by/until+미래(미완 x), as of + 미래, by the time+주어+현재, when+주어+현재
- 주절이 과거 시제일 경우, 종속절에는 과거나 과거 완료가 나온다.
	- 주절이 과거 시제라도, 문장이 언급되고 있는 현재 시점까지 종속절의 동작이나 상태가 계속될 경우에는 현재 시제가 올 수 있다.
	- 문장이 언급되고 있는 현재 시점 이후에 예정된 일이라면 미래 시제

#### 2.6.5 가정법 미래/과거/과거완료

- 가정법 미래/과거/과거완료 문장에서, if절과 주절의 시제는 짝을 이룬다.
	- 가능성이 희박한 미래 가정 : if S should R, S will(can, may, should) R
	- 현재의 반대를 가정(과거) : if S 과거동사, S would(could, might, should) R
	- 과거의 반대를 가정(과거완료) : if S had p.p., S would(could, might, should) have p.p

- 혼합가정법에서는 if절과 주절의 시제가 다르다
	- if S had p.p., S would R
	- 주로 주절에 현재 시제를 나타내는 단서(now, today)가 나온다.

#### 2.6.6 if 없는 가정법 : 가정법 도치와 without 구문

- 가정법 문장에서는 if가 생략될 수 있으며, 이때 주어와 조동사의 자리가 바뀌는 도치가 일어난다.
	- 미래 : Should S R, S will(can, may, should) R
	- 과거 : Were S n./a., S would(could, might, should) R
	- 과거 완료 : Had S p.p., S would(could, might, should) have p.p.
- if절 대신 without+n.가 와서 가정법 문장을 만들 수 있다.
	- 과거 : S would R without n. = if it were not for n.
	- 과거완료 : S would have p.p. without n. = if it had not been for n.
	- if절 대신 otherwise를 쓰기도 한다.
