## 4. 품사

### 4.10 명사

### 4.10.1 명사자리

- 명사는 문장 내에서 주어, 목적어, 보어 자리에 온다.
- 명사는 다음의 품사 앞이나 뒤에 주로 붙어 나온다.
	- 관 명 전
	- 형/분 명
	- 소유 (형) 명
	- 명 명
- 명사 자리에 동사, 형용사 등은 올 수 없다.

#### 4.10.2 가산 명사와 불가산 명사

- 가산 명사는 반드시 앞에 관사가 오거나 복수형으로 쓰인다.
- 셀 수 없어 보이지만 가산 명사 : discount, price, purpose, refund, relation, approach, statement, workplace,
	source, result, compliment, request, belongings, measures, savings, standards, funds
- 셀 수 있는 것처럼 보이지만, 불가산 명사 : access, advice, baggage, equipment, information,
	luggage, machinery, news, stationery, consent
- 헷갈리기 쉬운 (불)가산 명사 : 가산/ 불가산
	- account / accounting
	- advertisement / advertising
	- clothes / cloting
	- a fund / funding
	- furnishings / furniture
	- goods / merchandise
	- lender/ lending
	- letter / mail
	- permit / permission
	- process, procedures / processing
	- seat / seating
	- ticket / ticketing
- 가산/불가산일 때 의미가 다른 명사들
	- customs '관세, 세관'/ custom '관습'
	- manners '풍습, 예의범절'/ manner '방법, 방식'
	- savings '저금, 저축'/ saving '절약'

#### 4.10.3 한정사와 명사

- a/an
- the
- 수량 형용사
	- 단수 : one, every, each
	- 복수 : (a) few, fewer, many, several, both, various
	- 불가산 : (a) little, less, much
	- (불)가산 : no, all, more, most, some, any
		- all, more, most, some은 복수로 쓰임

#### 4.10.4 사람명사 vs 사물/추상명사

- 의미에 맞는 단어를 쓸 것
- accountant / account / accounting
- applicant / application / appliance
- attendant / attendance / attendee
- consultant / consultation / consultancy
- employer / employee / employment
- facilitator / facility / facilitation
- journalist / journal / journalism
- occupant / occupancy / occupation
- producer / production / product
- receptionist / reception / receptacle
- recruiter / recruit / recruitment
- analyst / analysis
- architect / architecture
- assembler / assembly
- assistant / assistance
- authority / authorization
- beneficiary / benefit
- committee / commitment
- consumer / consumption
- contributor / contribution
- coordinator / coordination
- correspondent / correspondence
- distributor / distribution
- donor / donation
- editor / edition
- engineer / engineering
- enthusiast / enthusiasm
- founder / foundation
- inspector / inspection
- instructor / instruction
- investor / investment
- licensor / license
- manager / management
- manufacturer / manufacture
- negotiator / negotiation
- operator / operation
- owner / ownership
- participant / participation
- performer / performance
- presenter / presentation
- relative / relation
- representative / representation
- resident / residence
- subscriber / subscription
- supervisor / supervision
- technician / technology

#### 4.10.5 복합명사

- 복합 명사는 명사 + 명사로 되어 있다. 이때, 앞의 명사 자리에 형용사나 분사, 동사는 올 수 없다.
- 복합명사들 : account number, expansion project, reception desk,
	application fee, expiration date, reference letter,
	application form, feasibility sutdy, registration form,
	arrival date, growth rate, research program,
	assembly line, housing department, retail sales,
	attendance record, interest rate, retirement celebration,
	communication skill, investment advice, retirement luncheon,
	conference room, keynote speaker, return policy,
	confidentiality policy, living expenses, safety inspection,
	confirmation number, occupancy rate, safety regulations,
	customer satisfaction, performance appraisals/evaluations, sales representative,
	enrollment form, planning stage, security card,
	exchange rate, product description, service desk,
	exercise equipment, quality requirement, travel arrangement
- 복합 명사를 복수형으로 만들 때는 뒤의 명사를 복수형으로
	- 예외적으로 앞의 명사가 복수형태인 명사들 : customs office, public relations department, electronics company,
		earnings growth, savings account/bank/plan, sales deivision/promotion, human resources department
- 숫자 + 단위 명사 + 명사 : 단위 명사는 복수형으로 쓸 수 없다.
	- 단위명사가 다른 명사를 수식하는 형태가 아니면 복수형으로

#### 4.10.6 혼동하기 쉬운 명사

- 형태는 비슷하지만 의미가 달라 혼동을 주는 명사들
	- coverage, cover, covering
	- competition, competence
	- entry, entrance, entrant
	- identification, identity
	- interests, interest
	- likelihood, likeness
	- objective, objectivity, object, objection
	- percent, percentage
	- permit, permission
	- procedure, proceedings
	- productivity, product, produce, production
	- professionalism, profession, professional
	- remainder, remains
	- responsibility, responsiveness
	- sense, sensation
	- utilization, utility

### 4.11 대명사

#### 4.11.1 인칭대명사의 격

- 인칭대명사의 종류
- 주격은 주어자리
- 소유격은 형용사처럼 명사 앞에
	- 소유격 앞에는 관사를 쓸 수 없다 : my client = a client of mine
- 목적격은 타동사의 목적어, 전치사의 목적어 자리에 쓴다.
	- 전치사만 보고 바로 목적격을 쓰지 않도록 주의 (진짜 목적어의 수식어일 수도)
- 소유대명사는 소유격+명사를 대신한 것.
	- 주어, 목적어, 보어
	- 소유대명사 뒤에는 절대 명사가 나올 수 없다.

#### 4.11.2 재귀대명사

- 목적어가 주어와 같은 사람/사물을 지칭할 때 목적어 자리에 재귀대명사를 쓴다.
	- 재귀대명사는 생략할 수 없다.
- 주어나 목적어를 강조하기 위해 강조하고자 하는 말 바로 뒤나, 문장 맨 뒤에 재귀대명사를 쓴다.
	- 명령문에는 주어 you가 생략되어 있으므로 yourself를 쓴다.
- 재귀대명사 관련 관용 표현 : by oneself = alone = on one's own, for oneself, of itself, in itself

#### 4.11.3 지시대명사, 지시형용사

- 지시대명사 that/those는 앞에 나온 명사를 대신해서 사용한다.
	- that은 사람명사를 대신하지 못한다.
	- that/those 자리에 this/these는 올 수 없다.
- those는 '~한 사람들'이라는 의미로도 쓰인다.
	- those 자리에 they나 them을 쓸 수 없다.
		- they, them 뒤에는 수식어가 나올 수 없다.
- this/that과 these/those는 명사 앞에서 지시형용사로 쓰여 '이-', '저-'의 의미를 갖는다.
	- 지시형용사 those는 '저-'라는 의미를 갖지 않고 마치 'the'처럼 쓰이기도 한다.

#### 4.11.4 부정대명사 / 부정형용사 1 : one/another/other

- one : 정해지지 않은 단수 가산 명사를 대신한다.
	- one의 복수형 ones : 정해지지 않은 복수 가산 명사를 대신한다.
- another : 이미 언급한 것 이외의 또 다른 하나
	- another가 형용사로 쓰일 때 뒤에는 반드시 단수 가산 명사가 나와야 한다.
	- another 앞에는 the를 붙여 쓸 수 없다.
- other/others : 이미 언급한 것 이외의 것들 중 몇몇
	- other
		- 형용사로만 쓰인다.
		- 뒤에 복수 가산 명사가 온다.
	 	- 뒤에는 불가산 명사도 올 수 있다.
	- ohters : 대명사로만 쓰인다.
- the other(s) : 정해진 것 중 남은 것 전부
	- 남아 있는 것이 하나일 때는 the other + 단수명사 / the other로 쓴다.
	- 두 개 이상일 때는, the other + 복수명사 / the others로 쓴다.
- '서로'를 뜻하는 관용표현 : each other, one another

#### 4.11.5 부정대명사 / 부정형용사 2 : some/any, no/none, most/alomost

- some/any
	- some : 몇몇(의), 약간(의)
		- 대명사, 형용사
		- 긍정문에 주로 씀
		- 권유나 요청을 나타내는 의문문 : some
	- any : 몇몇(의), 조금(의)
		- 대명사, 형용사
		- 부정문, 의문문, 조건문
		- any가 긍정문 : '어떤 ~라도'의 의미
	- something, somebody, someone : 긍정문
	- anything, anybody, anyone : 부정문, 의문문, 조건문
- no/none
	- no
		- 형용사 : 뒤에 반드시 명사가 와야 한다.
	- none
		- 대명사 : 혼자 명사자리에 온다.
	- one과의 관계
		- no one은 'of 사람'의 수식을 받을 수 없다.
		- not은 a/an이나 one을 앞에서 강조할 수 있다.
	- nothing = not anything : anything이 nothing자리에 올 수 없다.
- most/almost
	- most : '대부분(의)'
		- 형용사, 대명사
	- almost : '거의'
		- 부사

#### 4.11.6 명사-대명사 일치

- 대명사는 명사와 수일치되어야 한다.
	- 명사 앞에서 형용사처럼 쓰이는 경우도 수일치
- 대명사는 명사와 성/인칭이 일치되어야 한다.

### 4.12 형용사

#### 4.12.1 형용사 자리

- 형용사는 명사를 수식하는 자리에 온다.
	- '-able/-ible' 어미의 형용사는 명사를 주로 뒤에서 수식한다.
- 형용사는 보어 자리에 온다.
- 형용사 자리에 부사나 명사는 올 수 없다.

#### 4.12.2 수량 표현

- 가산 명사, 불가산 명사 앞에 오는 수량 표현
	- 단수 명사 : a(n), each, one, every, another, a single
	- 복수 명사 : one of, (a) few, both, several, various, a couple of, a majority of,
		each of, fewer, many, numerous, a variety of, a number of, a (wide) range of
	- **불가산 명사 : (a) little, less, much, a great deal of, a large amount of**
	- 둘다 : no, more, some, lots of, plenty of, a wealth of, all, most, any, a lot of, other
		- 가산 명사와 쓰일 때 복수명사와 함께 쓰임 : all, more, most, lots of, a lot of, plenty of, other
	- another(~만큼), every(~만큼마다) : 'few/숫자 + 복수명사' 앞에도 올 수 있다.
- 수량 표현과 명사 사이에 of the가 와서 한정된 명사의 부분이나 전체를 나타내는 경우, of the는 생략될 수 없다.
	- some, many, one/two, each, much, any, all, most, (a) few, both, none, several, (a) little
	- all, both : of 없이 쓰일 수 있다.

#### 4.12.3 혼동하기 쉬운 형용사

- 형태가 비슷하지만 의미가 달라 혼동을 주는 형용사들을 주의해야 한다.
	- acceptable, accepting
	- admirable, admiring
	- appreciative, appreciable
	- argumentative, arguable
	- beneficial, beneficent
	- careful, caring
	- considerable, considerate
	- comparable, comparative
	- comprehensible, comprehensive
	- dependent, dependable
	- economic, economical
	- exhaustive, exhausted
	- favorable, favorite
	- impressive, impressed
	- imformed, imformative
	- managerial, manageable
	- persuasive, persuaded
	- practical, practicing
	- probable, probabilistic
	- profitable, proficient
	- prospective, prosperous
	- reliable, reliant
	- respectable, respective
	- responsible, responsive
	- seasonal, seasoned
	- successful, successive
	- understanding, understandable

#### 4.12.4 be + 형용사 숙어

- be about to
- be likely to do = be apt to do = be liable to + n./to do
- be available to do
- be available for
- be aware of = be conscious of = be cognizant of
- be capable of -ing
- be comparable to + n.
- be consistent with
- be eligible for/to do
- be responsible for
- be skilled in/at
- be subject to + n.
- be subjective to + n.
- be willing to do
- be bound to

### 4.13 부사

#### 4.13.1 부사 자리

- '(준)동사 + 목적어' 앞이나 뒤, '조동사+-ing/p.p.' 사이 또는 그 뒤에
- 부사가 동사 이외의 것을 수식하는 경우, 수식 받는 것 앞에 온다.
	 - 형용사 역할을 하는 '수 표현'을 앞에서 수식할 수 있는 것도 형용사가 아닌 부사이다.
- 부사 자리에 형용사는 올 수 없다.
	- 부 형 명 : 부가 형을 수식
	- 형 형 명 : 형이 각각 명사를 수식
	- 원급 구문에서 as ~ as 사이에 형용사 혹은 부사가 올 것인지는 as as 없이 문법상 맞는 걸 선택

#### 4.13.2 비슷한 형태를 갖고 있지만 의미가 다른 부사

- 형태가 비슷하지만 의미가 다른 부사들을 주의해야 한다.
	- hard, hardly
	- high, highly
	- great, greatly
	- late, lately
	- most, mostly
	- near, nearly
- 한 단어가 형/부의 의미를 모두 가지는 경우
	- early, late, hard, high, long, fast, far, near, daily, weekly, monthly, yearly, enough

#### 4.13.3 부사 선택 1 : 시간부사 : already/still/yet, ever/ago/once

- already/still/yet
	- already : '이미, 벌써', 긍정문
	- still : '아직, 여전히', 긍정문/부정문/의문문
	- yet : '아직', 부정문 / '이미, 벌써', dmlansans
	- 부정문에서 still은 not 앞에, yet은 not 뒤에 온다.
	- have yet to R
	- finally : 오랫동안 기다리던 일이 일어났을 때
	- soon : 얼마 지나지 않아 일이 일어날 예정이거나 일어났을 때
- ever/ago/once
	- ever : 막연한 과거의 시점, 부정문/의문문
	- ago : (시간 바로 다음에 와서) 현재를 기준으로 그 시간 이전에 일어난 일
	- once : 막연한 과거의 시점, 형용사를 수식하기도 한다.

#### 4.13.4 부사선택2 : 빈도 부사 : always/usually/often/hardly

- 일반동사의 앞 또는 조동사나 be동사의 뒤에 온다.
- always, almost, often, frequently, usually, once, somtimes, hardly/rarely/seldom/scarcely/barely, never
	- hardly ever
	- usually, often는 문장 맨 앞이나 뒤에도 올 수 있지만, always는 문장 앞이나 뒤에 올 수 없다.
	- 빈도 부사 자리에 시간 부사를 쓰지 않는다.
	- barely 가까스로 ~하다
- hardly/rarely/seldom/scarcely/barely : 부정의 의미
	- 부정 부사들이 강조를 위해 문장 맨 앞으로 나오면 주-동 도치
	- not과 함께 올 수 없다.

#### 4.13.5 부사선택3 : 접속부사 : besides, therfore, however, otherwise

- 접속부사는 앞 뒤 절의 의미를 연결
- besides, moreover, furthemore, then, afterwards, accordingly, therefore/thus, hence,
	consequently, however, nevertheless, nonetheless, otherwise, instead, meantime, meanwhile
	- otherwise가 일반 부사로 사용되어 동사, 형용사, 부사를 꾸밀 때는 '그렇지 않게, 달리, 다른 점에서'
- 접속부사는 부사라서 혼자서는 두 개의 절을 연결할 수 없고, 접속사 역할을 하는 세미콜론이 함께 와야 한다.
- 접속부사는 콤마와 함께 문장의 맨 앞에 위치하여 두 개의 문장을 의미적으로 연결한다.
	- 접속부사 자리에 부사절 접속사는 올 수 없다.
	- 두 단어 이상으로 이루어진 부사구가 문장 맨 앞에 위치하여 접속부사처럼 앞뒤 절의 의미를 연결할 수도 있다.
	- in addition, in fact, as a matter of fact, as a result, in particular, even so, if so, after that,
		since then, to that end, in response, by comparision, on the contrary, in contrast, in this way
- then이 일반 부사로 사용되면 '그렇다면, 그러면'을 의미하면서 if절 뒤에 나올 수 있다.

#### 4.13.6 부사 선택 4 : 강조부사 : just, right, only, well, even, quite

- just/right(바로)는 before나 after를 앞에서 강조한다.
	- just enough
	- just는 동사를 꾸미면서 '막, 방금'
- only/just는 전치사구나 명사구 등을 강조한다.
- well은 전치사구를 강조한다.
- even은 단어나 구를 앞에서 강조한다.
- quite는 a/an+n.를 앞에서 강조한다.
	- not quite '완전히 ~은 아니다'
	- quite가 동/형/부를 꾸밀 때는 '상당히, 매우, 꽤'
- nearly/almost/just는 원급을 강조한다.
- much/even/still/far/a lot/by far은 비교급을 강조한다.
- by far/quite은 최상급을 강조한다.

#### 4.13.7 부사선택 5 : so, such, very, too

- so '매우'인 부사라서 형/부 수식
- such는 형용사라서 명사 수식
	- such가 단수 명사를 수식할 때는 such 다음에 a/an이 온다.
- very는 so와 의미는 같지만, so와 달리 뒤에 that절과 함께 쓰일 수 없다.
- too는 so/such/very와 달리, 너무 ~하다라는 부정적인 의미를 갖는다.
	- too + a./ad. + to부정사 : 너무나 ~해서 ~할 수 없다.
	- too much/many + n. = far too much/many '너무나 많은 ~'
	- much too + a./ad. = far too '너무나 ~한/~하게'

#### 4.13.8 부사 선택 6 : also/too/as well/either, later/thereafter, forward/ahead

- also는 문장 처음이나 중간에 온다. 문장 끝에는 올 수 없다.
- too, as well은 문장 끝에 온다.
- either은 부정문을 언급한 후, 또 다른 부정문을 덧붙일 때 쓰인다.
- later : 시간 표현 바로 다음에 와서 '그 시간 이후에'
	- 시간 표현 없이 오면 '지금 이후에 = after now'
- thereafter '그 이후에 = after that'
- since '그 이래로'
- forward/backword : 방향을 가리킬 때
- ahead/behind : 상태를 가리킬 때

### 4.14 전치사

#### 4.14.1 전치사 선택 1 : 시간과 장소 : in/at/on

- 시간 전치사 in/at/on
	- in : 월, 연도, 계절, 세기, ~(시간) 후에, 아침/오후/저녁
	- at : 시각, 시점
	- on : 날짜, 요일, 특정일
- 장소 전치사 in/at/on
	- in : 큰 공간 내 장소
	- at : 지점, 번지
	- on : 표면 위, 일직선 상의 지점
	- 같은 장소라도, 그 장소의 안을 의미할 때는 in, 장소 자체를 의미할 때는 at을 사용한다.
- in/at/on 숙어 표현
	- in advance, in effect, in one's absence, in time, in particular, in a timely manner,
	 in place, in writing, in the foreseeable future
	- at once, at regular intervals, at the rate of, at times, at a good pace, at the age of, at least, at high speed,
		at a charge of, at all times, at a low price, at one's expense, at the latest, at 60 miles an hour, at your earliest convenience
	- on time, on/upon arrival, on the waiting list, on a regular basis, on/upon request,
		on the recommendation of, on call, on vacation

#### 4.14.2 전치사 선택 2: 시점과 기간

- 시점을 나타내는 전치사 : since, from, until/by, before/prior to
	- until : 상황, 상태가 계속될 때까지,
	- by : 행동이 발생할 때까지
	- 숙어 표현 : two weeks from now, three weeks prior to the date, from 5 o'clock onward(s)
- 기간을 나타내는 전치사 : for/during, over/through/throughout, within
	- for : 숫자를 포함한 기간 표현 앞에서 '얼마나 오랫동안 지속되는지'
	- during : 명사 앞에 와서 '언제 일어나는지', 뒤에 한정사가 나오면 숫자 기간표현이 올 수 있다.
	- in : 기간 표현이 올 경우 '~후에, ~가 지나서', 서수/최상급/부정문에서는 '~만에, ~동안'
- 시점을 나타내는 전치사와 기가을 나타내는 전치사의 구별

#### 4.14.3 전치사 선택 3: 위치

- 위치를 나타내느 전치사
	- above/over, below/under, beside/next to, behind, between/among, near, within, outside, around, past, opposite
	- between : '(위치 또는 시간)둘 사이'
	- among : '셋 이상의 사물/사람 사이'
	- above/over, below/under : '이상', '이하'
- 위치 전치사 숙어 표현
	- above one's expectations
	- have the edge/advantage over
	- under new management, under new policy, under close supervision, under current contract, under control, under pressure,
		under investigation, under review, under consideration, under discussion, under construction, under way
	- a difference/gap between A and B
	- within walking distance of, within the limit
	- around the world, around the corner

#### 4.14.4 전치사 선택 4 : 방향

- 방향을 나타내는 전치사
	- from, to, across, thorugh, along, for, toward, into, out of
	- along with '~와 함께'
- 방향 전치사 숙어 표현
	- from one's view point
	- to the relief of, to a great extent, to my knowledge, to the point, to one's satisfaction, to your heart's content
	- along the shore, along the side of, across the nation, across the street, across from the post office
	- out of date, out of reach, out of order, out of room, our of print, our of stock,
		out of season, out of control, out of town, out of paper

#### 4.14.5 전치사 선택 5 : 이유, 양보, 목적, 제외, 부가

- 이유, 양보, 목적을 나타내는 전치사
	- because of, due to, owing to, on account of
		- thanks to, as a result of
	- despite, in spite of, with all, notwithstanding
	- for
		- for your convenience, for future use, for safety reasons, for further information,
		  articles for sale, money for supplies, a coupon for every $100
- 제외, 부가를 나타내는 전치사
	- except (for), excepting, apart from, aside from, outside
		- except 다음에 [접속사+절]이 오는 경우 for를 쓰지 못한다.
	- barring, without, but for
	- instead of
	- in addition to, besides, apart from, plus

#### 4.14.6 전치사 선택 6 : of, ~에 관하여

- A of B
	- 의미상 A가 동사, B가 주어인 경우 : B가 A하다, B의 A
	- A가 동사, B가 목적어 : B가 A됨
	- A와 B가 동격 : B는 A
	- A가 B의 부분, 소속인 경우 : B에 속한 A
- ~에 관하여
	- about, on/upon, over, as to, as for, concerning, regarding, with/in regard to, with repect to, with/in reference to

#### 4.14.7 전치사 선택 7 : 기타 전치사

- 기타 전치사
	- by, through, throughout
		- by telephone/fax/mail, by cash/check/credit card, by land, by law
		- through the use of, through cooperation
	- with, without
		- with no doubt, with no exception, with the aim of, with emphasis, with regularity
		- without approval
	- as
	- like, unlike
	- against
		- against the law, act against one's will
	- beyond
		- beyond repair, beyond one's capacity
	- following
	- amid

#### 4.14.8 동사, 형용사, 명사와 함께 쓰인 전치사 표현

- 동사+전치사 표현 : advertise on, contribute to, associate A with B, account for, depend/rely/count on(upon),
	congratulate A on B, add to, register for, direct A to B, comply with, wait for, return A to B,
	consist of, keep track of, transfer A to B
- 형용사+전치사 표현 : absent from, equivalent to, identical to, consistent with,
	responsible for, coparable to, comparable with, similar to
- 명사+전치사 표현 : access to, a solution to, an advocate for(of), a cause/reason for, concern over(about),
	a lack of, permission from, a problem with, a dispute over, an effect(impact/influence) on,
	a question about(concerning/regarding), respect for, a decrease/an increase/a rise/a drop in
- 두 단어 이상으로 이루어진 전치사 : ahead of+시간/장소, as of + 시간, in excess of, in place of, contrary to,
  in exchange for, in preparation for, regardless of, in honor of, in respect of, by means of, in keeping with,
	in response to, in celebration of, in light of, in violation of, in charge of, in observance of, on behalf of
