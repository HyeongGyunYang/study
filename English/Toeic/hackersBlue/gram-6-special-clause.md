## 6. 특수 구문

### 6.19 비교구문

#### 6.19.1 원급

- as a./ad. as
  - not as ~ as, not so ~ as
  - 형/부는 as/as를 지우면 구별 가능
- as many/much/few/little n. as : 원급으로 n.만 못 쓴다.
- as same (+n.) as
- 형용사나 부사의 원급을 꾸며주는 표현 : nearly, almost, just
  - nearly as expensive as

#### 6.19.2 비교급

- a./ad.의 비교급 + than
  - less/more a./ad. than
  - less/more than을 지우면 형/부 구별 가능
- more/fewer/less + n. + than
- the를 쓰는 비교급
  - the 비교급, the 비교급
  - the 비교급 + of the two
- 형용사나 부사의 비교급을 강조하는 표현 : much, even, still, far, a lot, by far

#### 6.19.3 최상급

- 최상급 + of/in/that
- 최상급 이후의 명사가 명확할 경우, 생략 가능
- 최상급 표현 : one of the 최상급 + 복수 명사, at least, at most, at best
- 최상급 + 명사 앞에는 the나 소유격이 온다.
  - 서수가 오면 '몇번째로 ~한'
- 형용사나 부사의 최상급을 강조하는 표현으로는 by far, quite, very

#### 6.19.4 기타 원급, 비교급, 최상급 표현

- 비교급 표현
  - more/less than + n.
  - for a later time
  - no later than
  - no longer
  - no sooner ~ than ~
  - other than
  - rather than
  - would rather ~ than ~
- 원급/비교급 형태로 최상급 의미를 지니는 표현
  - 비교급 + than any other ~
  - have never/hardly/rarely been + 비교급
  - no other ~ / nothing + as 원급 as
  - no ohter ~ / nothing + 비교급 than

### 6.20 병치, 도치 구문

#### 6.20.1 병치구문

- 병치 구문에서는 같은 품사끼리 연결되어야 한다. (and 등)
  - 동사의 경우 수/시제 일치
- 같은 구조끼리 연결되어야 한다.
  - 두번째 나오는 to부정사의 to는 생략 가능

#### 6.20.2 도치구문

- 가정법 문장에서 if가 생략되면 도치가 일어난다.
  - Should + S + R, S will R
  - Were S ~, S would R
  - Had S p.p., S would have p.p.
- 부정어(never, nor, hardly, seldom, rarely, little)가 절 맨 앞에 나오면 도치
- so/neither(~도 역시 그러하다(않다))가 절 맨 앞에 나오면 도치
  - 둘은 각각 긍/부정문에서 앞에 나온 어구가 반복될 때 대신해서 사용
- [only + 부사(구, 절)]가 문장 맨 앞에 나오면 도치
  - 도치 구문에서 주어 뒤에 오는 동사의 형태에 주의
  - as나 than뒤에서는 도치가 일어나기도 하고 일어나지 않기도 한다.
  - 보어로 사용된 형용사, 분사가 문장 맨 앞으로 나올 경우, 도치가 일어난다.
