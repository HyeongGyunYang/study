## 5. 접속사와 절

### 5.15 등위접속사와 상관접속사

#### 5.15.1 등위접속사

- and, or, but, yet, so, for
  - so, for는 절-절만 연결 가능, 단어/구 연결 불가
  - 등위접속사 자리에 접속부사(however, therefore, instead)나 일반부사(also)는 올 수 없다.
  - and so, and yet, and then : 등위접속사+부사로 분명한 의미 전달
- 등위접속사는 문맥에 맞는 것을 선택해야 한다.
- 주어가 and로 연결되면 복수 동사, or로 연결되면 마지막 주어에 수 일치

#### 5.15.2 상관접속사

- 상관접속사의 종류 : both A and B, either A or B, neither A nor B, not only A but (also) B = B as well as A,
 not A but B = B but not A = (only) B, not A
- 상관접속사는 맞는 짝끼리 쓰여야 한다.
- 상관접속사로 연결된 주어와 동사의 수일치에 주의
  - B에 일치시키는 경우 : not A but B, either A or B, neither A nor B, not only A but also B
  - 항상 복수 동사를 쓰는 경우 : both A and B

### 5.16 관계절

#### 5.16.1 관계절의 자리와 쓰임

- 관계절은 수식어 거품 자리인, 주어와 동사 사이나 문장의 끝에 온다.
  - 부사절과 달리, 필수성분 앞 수식어 거품 자리에는 올 수 없다.
- 관계절을 이끄는 관계대명사나 관계부사 자리에 대명사나 부사는 올 수 없다.
- 관계절의 동사 자리에 동사가 아닌 형태(준동사, 명사, 부사)는 올 수 없다.
  - 관계절 동사 자리에는 관계절 앞의 명사와 수, 태가 맞는 동사가 들어가야 한다.

#### 5.16.2 관계대명사의 선택과 격

- 선행사가 사람이면 who(m), 사물이면 which를 쓴다.
- that
  - 선행사 종류에 상관 없이 주격/목적격 관계대명사로 쓴다.
  - 콤마나 전치사 바로 뒤에는 쓸 수 없다.
- 주격/목적격/소유격

#### 5.16.3 전치사+관계대명사, 수량 표현 + 관계대명사, 관계대명사의 생략

- 전치사 + 관계대명사
  - 앞 문장과의 공통명사가 뒷 문장에서 목적어일 때
  - 전치사는 선행사 또는 관계절 내의 동사에 따라 결정된다.
  - 뒤에는 완전한 절이 온다.
- 수량 표현 + of + 관계대명사(목적격, 소유격+명사)
  - one/each, some/any, many/much/most, all/both, several, half/the rest
- 관계대명사의 생략
  - 주격 관계대명사 + be동사 생략
  - 목적격 관계 대명사 생략

#### 5.16.4 관계부사

- 선행사의 종류에 따라 각각 다른 관계부사가 쓰인다.
- when, why, where, how
- 관계부사는 '전치사+관계사'로 쓸 수 있다.
- 관계부사 다음에는 완전한 절, 관계대명사 다음에는 주/목이 빠진 불완전한 절

### 5.17 부사절

#### 5.17.1 부사절의 자리와 쓰임

- 부사절은 수식어 거품 자리, 특히 필수성분 앞이나 필수성분 뒤에 온다.
- 부사절은 문장에서 부사 역할을 하며, (관계부사)관계절(형용사 역할)과 구별
- 부사절 접속사는 절 앞에 오는 것이 원칙, 분사구문 앞에 오기도 한다.
  - 전치사를 대신 쓰지 못한다.
  - 분사구문 앞에 자주 오는 부사절 접속사 : when, before, after, since, while, whenever, unless, until, once

#### 5.17.2 부사절 접속사 1: 시간

- 시간을 나타내는 부사절 접속사
  - until, before
    - until after : 보통 2개 연속으로 안 오지만 이건 가능
  - when, as, while, even as
    - as '~이기 때문에'로도 쓰인다.
    - while '~한 반면에'로도 쓰인다.
  - since, after
  - once, as soon as = immediately after
- 시간을 나타내는 부사절 접속사 다음에는, 미래를 나타내기 위해서 현재 시제를 쓴다
  - since 다음에는 과거 시제가 자주온다. 주절에는 현재 완료 시제

#### 5.17.3 부사절 접속사 2 : 조건, 양보

- 조건을 나타내는 부사절 접속사
  - if, assuming (that)
  - unless = if ~ not
  - as long as, providing (that), provided (that), on condition that, only if
    - as long as '~하는 동안, ~만큼 오래'로도 쓰인다
  - in case (that), in the event (that)
- 조건을 나타내는 부사절 접속사 다음에는 미래를 나타내기 위해 현재 시제를 쓴다.
- if/whether는 명사절 접속사와 부사절 접속사 모두로 쓰일 수 있다.
  - 명사절 : if=whether '~인지 아닌지'
  - 부사절 if '만약 ~라면'
  - 부사절 whether '~이든 아니든'
- 양보를 나타내는 부사절 접속사
  - although, though, even if, even though
  - whereas, while

#### 5.17.4 부사절 접속사 3 : 이유, 목적, 결과

- 이유를 나타내는 부사절 접속사 : because/as/since, now that, in that
- 목적을 나타내는 부사절 접속사 : so that, in order that
- 결과를 나타내는 부사절 접속사 : so that/such that
- except that / but that
- as if / as though / (just) as
- given that / considering (that)

#### 5.17.5 부사절 접속사 4 : 복합관계부사 whenever, wherever, however

- 복합관계부사가 이끄는 부사절은 문장 내에서 부사 역할을 한다.
  - whenever, wherever, however
- however
  - 'no matter how'의 뜻일 경우, 'however + a/ad + S + V'의 형태
  - Visit however often you can, however hard I try, however much it costs, however well I perform
  - 접속부사로도 쓰임
- 복합관계대명사 whoever, whatever, whichever도 부사절을 이끌 수 있다.

#### 5.17.6 부사절 접속사 VS. 전치사

- 혼동되는 부사절 접속사와 전치사 (접속사 / 전치사)
  - 시간
    - when / in, at
    - while / during
    - by the time, until / by, until
    - after / following, after
    - before / before, prior to
    - once, as soon as / on(upon) -ing
    - since / since
  - 조건
    - unless / without
    - in case (that), in the event (that) / in case of, in the event of
  - 양보 : altough, even though, while / despite, in spite of
  - 이유 : because, as, since / because of, due to
  - 목적 : so that ~ can -, in order that ~ can - / so as to, in order to R
  - 제외 : except that, but that / except (for), but (for)
  - given that, considering (that) / given, considering
  - whether / regardless of
  - as if, as though / like
  - as(like) / as(자격)
- 부사절 접속사는 '절' 앞에, 전치사는 '구' 앞에 온다.
  - 접속부사는 절을 이끌 수 없으므로, 부사절 접속사 대신 쓰일 수 없다.

### 5.18 명사절

#### 5.18.1 명사절의 자리와 쓰임

- 주, 목, 보
- that 명사절은 4형식의 직목 자리에 오기도 함
- 명사절 접속사 자리에 전치사나 대명사는 올 수 없다.
- 관계절(형용사 역할)과 구별
- 부사절(부사 역할)과 구별

#### 5.18.2 명사절 접속사 2 : that

- that이 이끄는 명사절은 문장에서 주어, 동사의 목적어, 보어, 동격절로 쓰인다.
- 전치사의 목적어로는 쓰일 수 없다.
- '말하다, 보고하다, 생각하다, 알다' 등을 의미하는 동사의 목적어로 쓰인 that은 생략 가능
- that 절을 취하는 형용사
  - be aware that, be glad/happy that, be sure that, be sorry that, be convinced that, be afraid that
- 동격절을 취하는 명사
  - fact that, statement that, opinion that, truth that, news that, report that, idea that,
    (re)assurance that, rumor that, claim that, confirmation that

#### 5.18.3 명사절 접속사 2 : if, whether

- if/whether(or not)이 이끄는 명사절은 '~인지 아닌지'를 의미, 주/목/보
  - whether '~에 상관없이'라는 부사절로도 쓰임
  - if가 '(만약)~라면'의 부사절로 쓰이면 whether를 대신할 수 없다.
- whether A or B, whether or not
- if절은 주어 자리에도, 전치사 다음에도 올 수 없다.

#### 5.18.4 명사절 접속사 3 : 의문사 who, what, which, when, where, how, why

- who(m), whose, what, which는 의문대명사로 명사절을 이끈다.
  - 그 자체로 명사절의 주/목/보 역할
  - 그래서 불완전한 절이 온다.
- whose, what, which는 의문형용사로 뒤에 나온 명사를 꾸미면서 명사절을 이끈다.
  - (의문형용사 + 명사)가 명사절의 주/목/보
  - 불완전한 절
- when, where, why는 의문부사로 명사절을 이끈다.
  - 완벽한 절
  - how는 형용사나 부사를 꾸며 주기도 한다.
- 의문사 + to부정사는 명사절 자리에 오며 '의문사 + S + should + V'로 바꿀 수 있다.
  - to부정사 앞의 의문사 자리에 whether가 올 수도 있다.

#### 5.18.5 명사절 접속사 4 : 복합관계대명사 who(m)ever, whatever, whichever

- 복합관계대명사가 이끄는 명사절은 문장에서 주어와 목적어로 쓰이며, 이때 복합관계대명사는 '대명사+관계대명사'의 역할을 한다.
  - whoever(anyone who), whomever(anyone whom), whatever(anything that), whichever(anything that, anyone who)
- 복합관계대명사가 이끄는 절은 부사절로 쓰이기도 한다.
- 대명사는 절을 이끌 수 없으므로, 복합관계대명사 대신 쓰일 수 없다.
- 복합관계대명사가 명사절 내에서 주어이면 주격, 목적어이면 목적격을 쓴다.
  - whomever는 whoever로 대체 가능
- whatever와 whichever는 뒤에 나오는 명사를 꾸미면서 복합관계형용사로 쓰일 수 있다.
- 복합관계대명사는 그 자체가 명사절의 주어나 목적어 역할, 그 뒤에 불완전한 절
- 복합관계사를 쓸지 의문사를 쓸지는 문맥에 따라 결정

#### 5.18.6 what과 that의 구별

- what절은 명사절로만 사용되지만, that절은 명사절, 형용사절(관계절), 부사절로 모두 사용된다.
- what절과 that절이 명사절로 쓰일때, what 다음에는 불완전한 절, that 뒤에는 완전한 절
