## 3.준동사구

### 3.7 to 부정사

#### 3.7.1 to 부정사의 역할

- to 부정사는 명사, 형용사, 부사 역할을 한다.
	- 명사 : 주어, 목적어, 보어 자리에 온다.
	- 형용사 : 명사 수식
	- 부사 : 목적, 이유, 결과
	- 출제 경향
	 	- 진주어, 진목적어 자리
		- 목적을 나타낼 때는 **in order to, so as to**도 사용 가능
		- 사람의 행동 목적을 나타낼 때는 **(전치사)for -ing**는 쓰지 못하고 to 부정사만 가능
- 동사는 to 부정사 자리에 올 수 없다.

#### 3.7.2 to 부정사의 형태와 의미상 주어

- to 부정사는 to R의 형태
- to 부정사의 수동형은 to be pp, 진행형 to be -ing, 완료형 to have pp
	- need, deserve, require의 수동형 to 부정사는 -ing로 쓸 수 있다. to be washed -> washing
- to 부정사의 의미상 주어가 필요할 경우, to 앞에 for+n., for+대명사의 목적격

#### 3.7.3 to부정사를 취하는 동사

- to 부정사를 취하는 동사
	- 동사 +목적어(to 부정사) : want to, need to, wish to, hope to, desire to, expect to,
		plan to, aim to, decide to, offer to, ask to, promise to, agree to, refuse to,
		fail to, serve to, pretend to, afford to, manage to, prefer to, tend to, hesitate to, strive to
	- 동사 + 목적어 + 보어(to 부정사) : want O to, need O to, expect O to, invite O to,
		encourage O to, persuade O to, convince O to, cause O to, ask O to,
		force O to, compel O to, get O to, tell O to, require O to,
		allow O to, permit O to, enable O to, forbid O to, remind O to, warn O to, advise O to
- to 부정사를 취하는 명사 : ability to, authority to, capacity to, chance to, claim to,
	decision to, effort to, need to, opportunity to, plan to, readiness to, right to, time to, way to, wish to
- to 부정사를 취하는 형용사 : be able to, be ready to, be willing to, be likely to, be eager to, be anxious to,
	be pleased to, be delighted to, be easy to, be difficult to, be good to, be dangerous to

#### 3.7.4 to부정사가 아닌 원형 부정사를 목적격 보어로 갖는 동사

- 사역동사 make, let, have + O + 원형 부정사
	- p.p.를 쓰는 경우 : 목적어가 목적격 보어되다라고 수동으로 해석될 때
	- get : 사역의미지만, to 부정사를 갖는다.
- 준 사역동사 help (+ O) + 원형 부정사/to 부정사
- 지각동사 hear, see, watch, notice + O + 원형 부정사 / 현재분사
	- p.p.를 쓰는 경우 : 목적어가 목적격 보어 되다 라고 수동으로 해석될 때

### 3.8 동명사

#### 3.8.1 동명사의 역할, 형태, 의미상 주어

- 명사역할 : 주어, 목적어, 보어
- 동사는 동명사 자리에 올 수 없다.
- 동명사의 수동형은 being p.p., 완료형은 having p.p.
- 동명사의 의미상 주어가 필요할 경우, 동명사 앞에 명사(대명사)의 소유격을 쓴다.

#### 3.8.2 동명사를 목적어로 갖는 동사

- 동명사를 목적어로 갖는 동사들 : enjoy, suggest, recommend, consider,
	finish, quit, discontinue, give up, postpone, dislike, deny, mind, avoid
- 동명사와 to 부정사를 모두 목적어로 갖는 동사들 : like, love, prefer, hate, start, begin, continue, intend, attempt, propose
- 동명사/to 부정사 목적어에 따라 문장의 의미가 변하는 동사 : remember, forget, regret
	- 동명사는 과거의 의미, to부정사는 미래 또는 목적의 의미
- advise, allow, permit, forbid : 동사+동명사 또는 동사+목적어+to부정사

#### 3.8.3 동명사 vs 명사

- 동명사는 목적어를 가질 수 있지만, 명사는 목적어를 가질 수 없다.
	- 목적어가 없는 경우 : 명사 > 동명사
	- -ing 형태의 명사 (동명사와 혼동하지 말 것) : beginning, belongings, broadcasting, findings, gathering,
	lodging, meeting, opening, shipping, training
- 동명사 앞에는 부정관사가 올 수 없지만, 명사 앞에는 올 수 있다.
- 명사와 의미가 다른 명사화된 동명사가 있는 경우, 의미에 맞는 것을 쓴다
	- advertising '광고업' / advertisement '광고'
	- openinig '개장/빈자리/공석' / open '야외'
	- funding '자금 지원/자금 조달' / fund '자금'
	- housing '주택/주택공급' / house '집'
	- mailing '우송' / mail '우편물'
	- marketing '마케팅' / market '시장'
	- planining '계획 수립' / plan '계획'
	- processing '처리/절차/가공' / process '과정/공정'
	- seating '좌석 배치' / seat '좌석'
	- spending '지출/소비' / spend '지출(액), 비용'
	- staffing '직원 배치' / staff '직원'
	- cleaning '청소' / clean '손질'

#### 3.8.4 동명사 관련 표현

- 동명사구 관용 표현 : go -ing, on -ing, it's no use -ing, spend + 시간/돈 + -ing, cannot help -ing,
	be busy (in) - ing, be worth -ing, keep (on) -ing, feel like -ing, have difficulty(trouble, a problem) (in) -ing
- ***(전치사) to +동명사 : contribute to -ing, look forward to -ing, object to -ing, lead to -ing,
	be committed to -ing, be dedicated to -ing, be devoted to -ing, be used to -ing***
	- 접속사로 연결된 경우, 등위성을 위해 전치사to + 동명사를 맞춰주자
	- 전치사to 뒤에는 명사가 올 수도

### 3.9 분사

#### 3.9.1 분사의 역할

- 분사는 형용사 역할을 한다.
	- 수식
	- 보어
	- 분사는 단독으로 쓰일 경우, 주로 명사를 앞에서 수식한다.
		- people questioned, people interviewed, people concerned 등은 뒤에서 수식한다.
- 동사는 분사 자리에 올 수 없다.

#### 3.9.2 분사구문

- 분사구문은 시간, 이유, 조건, 연속 동작 등을 나타내는 부사절 역할을 한다.
	- 뜻을 분명하게 해주기 위해 부사절 접속사가 분사구문 앞에 올 수도 있다.
- 동사나 명사는 분사구문 자리에 올 수 없다.
- 동시에 일어나는 상황을 나타낼 때, with + O + 분사구문

#### 3.9.3 현재분사 vs 과거분사

- 분사가 능동 관계면 현재분사, 수동 관계면 과거분사를 쓴다.
	- 수식하는 명사와,
	- 꾸며주는 주어/목적어와
	- 주절의 주어와
- 분사가 감정을 나타내는 경우, 수식받는 명사가 감정의 원인이면 현재분사, 감정의 주체면 과거분사

#### 3.9.4 현재분사와 과거분사를 혼동하기 쉬운 표현

- 현재분사 + 명사
	- in her opening remarks
	- the maintenance of existing equipment
	- a claim for the missing luggage
	- make a lasting impression on the audience
	- leading research center
	- The presiding officer will introduce the keynote speaker
	- a promising recruit
	- improving skills
- 과거분사 + 명사
	- the preferred means of communication
	- revise the proposed plan
	- hire an experienced(qualified, skilled) applicant
	- designated smoking area
	- the reserved space for the disabled
	- refer to the detailed information
	- receive written consent
	- one-year limited warranty
	- limited items
	- recently purchased office desks
	- a growing market for customized products
	- inspect finished products for defects
	- the high quality of the handcrafted goods
	- repeated absences from work
	- request an explanation of the attached document
	- responsible for the damaged items
