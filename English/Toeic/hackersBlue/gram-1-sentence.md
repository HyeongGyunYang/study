## 1. 문장패턴

### 1.1 주어, 동사

#### 1.1.1 주어 자리에 올 수 있는 것 : 명사 역할을 하는 것

-	주어가 될 수 있는 것은 명사 역할을 하는 것들이다 : 명사구, 대명사, 동명사구, to 부정사구, 명사절
-	동사, 형용사 등은 문장의 주어 자리에 올 수 없다 :
	-	형용사처럼 보이지만 품사가 명사인 단어들 : applicant, delivery, denial, proposal, complaint, arrival, disposal, withdrawal, referral
	-	동사나 형용사로 많이 사용되지만, 명사로도 사용되는 단어 : prospect, respect, request, review, help, charge, raise, check, deal, leave, effect, increase, offer, pay, control, interest, objective, respondent, alternative, representative, normal, individual, potential, original
-	주어는 동사와 수일치

#### 1.1.2 가짜주어 it/there

-	it은 to부정사구, that절과 같은 긴 주어를 대신해서 쓰인다.
	-	명사, 전치사구 등을 강조할 때, **it ~ that 강조구문**
	-	강조되는 것이 사람일 경우 who(m), 사물일 경우 which를 쓸 수도 있다.
-	**there V(be, remain ...) + 진주어(명사구)** ~이 있다를 나타내기 위해 쓰인다.
	-	진주어는 명사형태! 동사는 올 수 없다.
-	가주어 it과 there 자리에 다른 단어는 올 수 없다.

#### 1.1.3 동사 자리에 올 수 있는 것 : (조동사 +) 동사

-	문장의 동사가 될 수 있는 것 **(조동사 +) 동사**
-	**R-ing, to R**은 문장의 동사가 되지 못한다.
-	명사나 형용사 등은 문장의 동사자리에 올 수 없다.
	-	주로 명사나 형용사로 쓰이지만, 동사로도 사용되는 단어 : function, question, name, document, finance, cost, experience, access, process, purchase, place, transfer, contact, demand, feature, schedule, deposit, complete, secure, correct
-	동사는 수, 태, 시제가 맞아야 한다.

#### 1.1.4 명령문은 주어 없이 동사원형으로 시작된다.

-	명령문은 주어 없이 동사원형으로 시작된다.
	-	please와 자주 쓴다.
-	**when절/if절 + 명령문** : ~할 때/~한다면, ~해라
	-	드물게 whatever절
	-	주로 명령문의 동사를 묻는 형태로 출제
-	명령문의 동사 자리에, 원형이 아닌 동사나 준동사, 명사 등은 오지 못한다.

### 1.2 목적어, 보어, 수식어

#### 1.2.1 목적어 자리에 올 수 있는 것: 명사 역할을 하는 것

-	목적어가 될 수 있는 것은 명사 역할을 하는 것 : 명사구, 대명사, 동명사구, to 부정사구, 명사절
-	동사, 형용사 등은 목적어 자리에 올 수 없다

#### 1.2.2 가목적어 it 구문

-	목적격 보어가 있는 문장에서 to 부정사구나 that절이 목적어일 경우, 뒤로 보내고, 그 자리에 가목적어 it을 사용한다.
	-	목적어가 *명사구*일 때는 가목적어를 쓰지 않는다.
-	가목적어 it이 쓰이는 대표적 구문 ***make it possible***
	-	possible 대신 easy, difficult, necessary 등의 형용사

#### 1.2.3 보어 자리에 올 수 있는 것 : 명사 또는 형용사

-	보어가 될 수 있는 것은 명사 또는 형용사 역할을 하는 것
	-	명사 역할 : 명사구, 동명사구, to 부정사구, 명사절
	-	형용사 역할 : 형용사, 분사
-	보어 자리에 동사, 부사는 올 수 없다.
-	주격 보어를 갖는 동사 : be, become, get, seem, remain, turn, taste, feel, look, sound
-	목적격 보어를 갖는 동사 : make, keep, find, consider, call

#### 1.2.4 명사 보어, 형용사 보어

-	보어가 주어나 목적어와 동격 관계를 이루면, 보어 자리에 명사가 온다.
-	보어가 주어나 목적어를 설명해 주면, 보어 자리에 형용사가 온다.
-	**it - to/that**의 보어자리
	-	명사나 형용사 둘 다 올 수 있다.
	-	명사의 경우 한정사를 조심할 것.

#### 1.2.5 수식어 거품

-	수식어 거품이 될 수 있는 것 : 전치사구, to 부정사구, 분사(구문), 관계절, 부사절
-	수식어 거품이 오는 자리
	-	**여기** + 주어 + 동사
	-	주어 + **여기** + 동사
	-	주어 + 동사 ~ + **여기**
-	수식어 거품 파악하기 순서
	1.	문장의 동사 찾기
	2.	문장의 주어 찾기
	3.	수식어 거품에 bracket

#### 1.2.6 수식어 거품 구와 수식어 거품 절을 이끄는 것은 다르다.

-	수식어 거품구는 주어, 동사가 없는 구의 형태 : 전치사구, to 부정사구, 분사(구문)
-	수식어 거품절은 주어, 동사가 있는 형태 : 관계절, 부사절
-	수식어 거품구/절을 이끄는 것 구별하기 : 거품 안에 동사가 있는가?
