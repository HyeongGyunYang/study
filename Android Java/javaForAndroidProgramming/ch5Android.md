Java for android
================

chapter 5 안드로이드 필수 문법
------------------------------

-	memory의 제약으로 외부에서 정보를 가져와야 함
	-	쓰레드, 비동기, IO, 파싱을 많이 사용해야한다.
-	터치 기반 조작
	-	액티비티 간 이동이 많고, 이벤트 작업도 훨씬 많이 필요하다.
-	소스 길이를 줄이기 위해 특이하게 고안된 부분이 많다.

### Section 1 화면 이벤트 처리

#### Section 1-1. 화면 이벤트 핸들러

-	event Handler : 어떤 행위에 대한 요청을 실행하는 것
-	이벤트를 처리하려면 이벤트 소스에 이벤트 핸들러를 등록해야 한다.
	-	이벤트 소스를 생성
	-	리스너를 구현한 핸들러를 구현한다.
	-	핸들러는 핸들러 메서드를 구현해야 한다.고 선언한 리스너를 구현한 다음
	-	메서드를 구현해야 한다.
	-	이벤트 소스에 핸들러 객체를 등록한다.
-	실행 순서
	-	이벤트 발생
	-	이벤트 발생을 액티비티가 버튼에게 알린다.
	-	버튼에 등록된 이벤트 핸들러 객체가 생성되고 핸들러 메서드가 호출된다.
	-	핸들러 메서드가 실행되어 생일을 텍스트뷰에 표시한다.

#### Section 1-2. 이벤트 처리 용어

-	event : 정해진 행위. eg) 버튼을 누른다
-	event source : 이벤트가 발생하는 지점. eg) button
-	event listener, EHL : 리스너에 각 이벤트를 처리할 메서드가 선언되어 있다.
-	event handler object, EHO : 핸들러 메서드를 구현한 객체. 각 이벤트 핸들러 객체는 이벤트 리스너의 처리 메서드를 구현해야 한다.
-	event handler method, EHM : 이벤트를 처리하는 메서드로, 버튼을 누르는 행위가 발생하면 실행된다.

#### Section 1-3. 중요 다섯 가지 이벤트 처리 방법

-	java standard는 1, 5번을 주로 사용
-	android는 주로 4번, 종종 1번도 사용

-	1번: 메인액티비티가 메인 클래스에 이벤트리스너를 구현한다고 선언한다. 메인액티비티가 이벤트 핸들러 객체가 된다. 핸들러 객체 : MainActivity

<pre><code>
public class MainActivity extends AppCompatActivity implements View.OnClickListener {
  protected void onCreate(Bundle savedInstanceState) {
    birthDatePicker.setOnClickListener(this);
  }
  public void onClick(view v){  }
}
</code></pre>

-	2번: 메인액티비티의 멤버 형식으로 내부 클래스를 만든다

### Section 2 어댑터

### Section 3 익명 내부 클래스 사용 예

### Section 4 요청 핸들러

### Section 5 JSON

### Section 6 XML

### Section 7 Parsing

### Section 8 AsynTask

### Section 9 Context

### Section 10 Android Permission

### Section 11 Intent

### Section 12 결과를 갖고 되돌아오는 액티비티

### Section 13 멀티 액티비티

### Section 14 액티비티, 서비스 사이의 이동
