Java For Android Programming
============================

Chapter 2 안드로이드를 위한 자바 기본 문법
------------------------------------------

### Section 1 Type

#### Section 1-1. 기본 타입

-	Def: 산수 및 수학에서 사용하는 데이터이자 값 그 자체
-	선언방법: 저장할 때 용도를 구분하고 표시하기 위해 타입을 앞에 선언

<pre><code> int year = 2016; </code></pre>

-	기본타입 종류 및 용도: boolean, char(한글자), int, double(실수), String(문자열)

<pre><code>
boolean isL=false;
char sult='H';
int year=2016;
double latitude=32.293;
String card="H8";
</code></pre>

#### Section 1-2. 변수

-	Def: 데이터를 임시로 저장하는 공간
-	기본타입의 크기

#### Section 1-3. 데이터 타입

-	기본타입
-	참조타입
-	둘의 차이점
	-	기본은 바로 선언 가능
	-	참조는 객체 생성 이후 가능

#### Section 1-4. 기본타입의 연산과 타입 변환

-	연산: 5칙연산. / 는 몫, % 는 나머지
-	비교연산
-	타입변환
	-	기본타입의 크기 : int < long < float < double
	-	작은 타입은 큰 타입으로 바로 가능 : 자동 변환
	-	큰 타입을 작은 타입으로 바꿀 때는 casting을 사용

<pre><code>
// 작은 타입 to 큰 타입
double earthR = 6371;
// 큰 타입 to 작은 타입
int makeOne = (int)(Math.random() * 10);
// 숫자형 문자 to 정수
int num= '9'-'0'; // 57-48=9. '0'=48, '9'=57
int a=Character.getNumericValue('9');
int b=Character.digit('9',10);
// ASCII to 숫자
int alpha = 'A'; //65
char cAlpha = (char)97; //(int)'a'=97
</code></pre>

-	상수
	-	값이 한 번 결정되면 변경할 수 없다.
	-	final 키워드로 선언
	-	변수와 구분하기 위해 전체 대문자로 선언
	-	클래스의 static member로 선언할 때는 static final로 선언
	-	API로 제공되는 Math.Pi, Math.E는 상수로 선언되어 있다.

<pre><code>
final int PHYSICAL = 23;
public class SwitchMain{
  public static final int EMOTIONAL = 28;
}
</code></pre>

### Section 2 Java Programming

#### Section 2-1. 프로그램 실행 순서

-	조건문: if, switch, 삼항연산자( ? : )

#### Section 2-2. 주석

-	// 한줄 주석
-	/* * / 여러 줄 주석

#### Section 2-3. 조건문

-	if - else

<pre><code>
if (condition) {}
else if (condition) {}
else {}
</code></pre>

-	삼항 연산

<pre><code>(condition) ? TRUE : FALSE</code></pre>

-	switch~case
	-	swich에 해당하는 case를 실행한다.
	-	조건에 따라 분기

<pre><code>
switch(c) {
    case 'c' :
    default :
}
</code></pre>

#### Section 2-4. 반복문

-	for

<pre><code>
for ( int i = 1; i <=100; i++){
    execution
}
</code></pre>

-	while

<pre><code>
while (n>0){
    execution
}
</code></pre>

-	do ~ while : 적어도 한 번은 실행할 때

#### Section 2-5. 연산관용어구

#### Section 2-6. 메서드

-	선언과 호출
	-	define
	-	call
-	특징

#### Section 2-7. 문자열

#### Section 2-8. 배열

-	같은 타입의 나열
-	정적
	-	다시 생성, 다시 초기화가 불가능
-	동적
	-	new 로 생성
	-	크기 변경, 다시 생성, 다시 초기화 가능

#### Section 2-9. 문자열 처리 중요 메서드

-	pp 69.
