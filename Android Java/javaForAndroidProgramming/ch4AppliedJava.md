Java for Android Programming
============================

Chapter 4 Applied Basic Java
----------------------------

### Section 1 변수, 연산, 연산순위를 이용해 하이힐 높이 구하기

### Section 2 조건문 if를 활용해 지진진도와 색상 출력하기

### Section 3 삼항연산자를 활용해 지진진도와 색상 출력하기

### Section 4 switch를 이용해 바이오리듬의 종류를 구분해서 출력하기

### Section 5 String을 이용해 도시이름 출력하기

### Section 6 반복문 for와 타입변환을 이용해 화씨, 섭씨 변환하기

### Section 7 반복문 for, while, do~while의 차이점

### Section 8 상수, API 메서드를 활용해 바이오리듬 구하기

### Section 9 사용자정의 메서드를 활용해 바이오리듬 구하기

### Section 10 메서드를 이용해 두 지점 간의 거리 구하기

### Section 11 문자열 파싱을 이용해 세계 주요 도시의 위도, 경도 제공하기

### Section 12 웹에서 빌보드 차트 읽어오기

### Section 13 파싱 과정을 통해 빌보드 차트 랭킹 가져오기

### Section 14 IO를 이용해 빌보드 차트의 가수 이미지 저장하기

### Section 15 빌보드 목록을 QR코드로 바꾸고 이미지 저장하기

### Section 16 빌보드 차트를 HTML로 저장하기
