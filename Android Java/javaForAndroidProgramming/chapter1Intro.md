Java for Android Programming
============================

Chapter 1. Introduction
-----------------------

### Section 1. 자바 설치

### Section 2. 이클립스 설치

### Section 3. 안드로이드 스튜디오

### Section 4. 안드로이드 앱 개발과 자바

#### Section 4-1. 자바 문법의 네 가지 관점

#### Section 4-2. 안드로이드 프로그래밍의 세 가지 관점

1.	View 관점
	-	activity: 목적한 화면을 보여주는 작업
	-	UI component: 화면 구성, 버튼, 텍스트 뷰 등
	-	layout: 화면의 윤곽
	-	event, event 처리 : 화면을 누르면 발생
	-	사용자 정의 뷰
	-	intent: messanger 역할. activity에서 activity로 넘어갈 때
	-	context: API level과 system level 사이에서 activity 정보를 관리
2.	Data 관점 : UI component에 data를 전달하기 위한 객체(DTO)와 여러 객체들을 선형(list)이나 map으로 저장하거나 전송하는 방법을 제공
	-	Adaptor : data를 layout에 matching시켜 보여준다.
	-	UI thread : sub thread에서 main thread의 data를 반영시킨다.
3.	Processing 관점
	-	IO: data를 internet 등 외부에서 가져옴
	-	thread: data를 동시에 처리
	-	handler: 요청에 대한 처리
	-	sync / async task
	-	parsing
4.	Manifest
	-	activity 등록
	-	permission 등록

#### Section 4-3. 앱 디렉토리 구조

-	app: 최상위 directory
-	AndroidManifest.xml : 기본 환경 정보. permission, activity, service 정보 저장
-	java: app program source
-	res: resource
	-	drawable : image 저장 directory
-	layout : activity, list view, gride view의 item에 대한 layout을 저장
-	menu : 보통 activity 우상단 선택단추
-	values: color, string, style을 xml로 저장

#### Section 4-4. 앞으로 공부할 내용
