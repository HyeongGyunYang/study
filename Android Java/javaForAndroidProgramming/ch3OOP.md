Java for Android Programming
============================

Chapter 3 OOP
-------------

### Section 1 Object

#### Section 1-1. class

-	참조타입 만들기

<pre><code>public class cName {}</code></pre>

#### Section 1-2. package

-	클래스의 목적과 역할을 구분하기 위해. class를 포괄하는 개념

<pre><code>
package pName.pName?;
public class cName {}
</code></pre>

#### Section 1-3. member

-	멤버변수 (멤버, 변수필드): class 내에서 가장 중요한 데이터

<pre><code>
package pName.pName?;
public class cName {
  public double latitude;
  public double longitude;
}
</code></pre>

#### Section 1-4. 멤버변수 초기화

-	기본타입은 0과 관련된 값으로 자동 초기화
-	참조타입은 null로 자동 초기화

#### Section 1-5. 객체 생성

<pre><code>
package pName.pName;
public class cMain{
  public static void main(String[] args){
    cName v = new cName();
    v.latitude = 1212;
    v.longitude = 3435;
    System.out.printf(v.latitude, v.longitude);
    cName v2 = v;
  }
}
</code></pre>

#### Section 1-6. 은닉화와 접근제한자

-	access modifier
	-	public, private...
	-	외부에서 접근할 수 있는지 없는지를 결정

<pre><code>
package pName.pName;
public class cName {
  private double latitude;
  private double longitude;

  public double getLatitude(){
    return latitude;
  }
  public void setLatitude(double latitude){
    this.latitude = latitude;
  }
  public double getLongitude(){
    return longitude;
  }
  public void setLongitude(double longitude){
    this.longitude = longitude;
  }
}
</code></pre>

#### Section 1-7. 레퍼런스 this

-	객체 자신의 주소를 참조하는 reference
-	메서드 인자와 멤버이름이 같은 경우, 메서드 인자가 우선

#### Section 1-8. 생성자

-	객체를 생성할 때, 멤버변수에 값을 넣으려면
-	new로만 호출된다
-	return 없는 void method
-	생성자와 클래스는 이름이 같아야 한다.

<pre><code>
package pName.pName;
public class cName {
  private double latitude;
  private double longitude;

  public cName(double latitude, double longitude){
    this.latitude = latitude;
    this.longitude = longitude;
  }
}
</code></pre>

#### Section 1-9. 생성자 오버로딩 (중복 정의)

-	생성자가 있는데 더 만들겠다.는 뜻
-	오버로딩: 이름이 같고 인자의 개수나 타입이 다른 것

<pre><code>
package pName.pName;
public class cName {
  private double latitude;
  private double longitude;

  public cName(double latitude, double longitude){
    this.latitude = latitude;
    this.longitude = longitude;
  }
  public cName(){} // 인자 없이 생성 가능, 이 경우 위도/경도는 자동초기화
}
</code></pre>

#### Section 1-10. this 생성자

<pre><code>
package pName.pName;
public class cName {
  private double latitude;
  private double longitude;

  public cName(double latitude, double longitude){
    this.latitude = latitude;
    this.longitude = longitude;
  }
  public cName(){
    this(34, 34) // 이런식으로 사용
  }
}
</code></pre>

### Section 2 Data

#### Section 2-1. 전달 객체

-	value object / data transfer object : 데이터를 저장하거나 전달하기 위한 목적

#### Section 2-2. 유동성 인자

-	Var args(...) :배열처럼 동작한다.
-	여러 개의 오버로딩 메서드를 만들 필요가 없다.
-	인자 개수에 상관없이 메서드 하나만 있으면 모든 경우에 대해 실행된다.

#### Section 2-3. 객체 배열

#### Section 2-4. 향상된 for

-	스스로 인덱스를 만들어 실행
-	출력 전용이라서 값을 변경하면 에러

<pre><code>
for(cName gg: oName){
  System.out.printf(gg.latitude, gg.longitude)
}
</code></pre>

#### Section 2-5. list 자료구조

<pre><code>
import java.util.arrayList;
ArrayList<Geo> geolist = new ArrayList<Geo>();
geolist.clear(); // 리스트의 모든 내용을 제거한다.
geolist.add(new Geo(32, 212)); // 객체를 리스트에 넣는다.
System.out.println(geolist.size()); // 리스트의 길이
Geo gt=geolist.get(1); // index 1의 객체 반환
</code></pre>

#### Section 2-6. map 자료구조 : hash table

<pre><code>
import jave.util.HashMap;
HashMap<String, Geo> cities = new HashMap<String, Geo>();

cities.clear();
cities.put("Korea", new Geo(37,124));
System.out.println(cities.size());
System.out.println(cities.containsKey("Austria")); // boolean
Geo geo = cities.get("Austria");
</code></pre>

#### Section 2-7. method overloading

-	메서드 이름은 동일하지만, 인자의 개수나 타입이 달라서 식별이 되는 메서드
-	로직이 비슷할 때 사용

#### Section 2-8. generics

-	저장하려는 타입을 <>를 사용해 한정하는 것
-	자바 7 이후는 <>안의 타입을 비워둔 채 생성해도 된다.

#### Section 2-9. 날짜 관련 변환

-	java.lang.System.currentTimeMillis()
	-	1970.1.1.0.0.0을 기준으로 경과시간을 long type으로 반환
	-	밀리초는 1/1000초
-	java.util.Calendar
	-	.getTimeMillis(): 경과시간을 long으로 반환
	-	.getTime(): Date로 반환
-	java.text.SimpleDateFormat(): Date와 String을 서로 변환할 수 있다.

#### Section 2-10. 예외 처리

-	try{} catch(){} : try하다가 예외가 catch되면 catch

<pre><code>
SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd");
Date d2 = new Date();
try {
  d2 = sdf.parse(st);
}catch (ParseException e) {
  System.out.println(d2);
}catch(exception e) {
  System.out.println(e);
}
</code></pre>

-	finally {}: 예외 발생 여부와 상관없이 반드시 실행

<pre><code>
InputStream inputStream=null;
URL url=null;
try {
  executions
} catch (Exception e) {
  isConnection = false;
} finally {
  try {
    inputStram.close();
  } catch (Exeption e) {}
}
</code></pre>

#### Section 2-11. static

-	객체를 생성하지 않고 사용할 수 있는 키워드
-	static method는 메서드 앞에 static을 붙인다.
	-	호출 할 때는 className.methodName

### Section 3 Hierarchical Structure

#### Section 3-1. 상속

<pre><code>
public class ClockCity extends Geo {
  private String timezoneId="Asia/Seoul"
  private String countryNmae="Korea"
}
</code></pre>

#### Section 3-2. 안드로이드의 상속

-	앱에서 사용할 그림을 그리려면 View를 상속해야 한다.

#### Section 3-3. super generator, super reference

-	super() 생성자 : 자식 생성자에서 부모 생성자를 호출할 때
-	super. 레퍼런스 : 자식 클래스에서 부모의 멤버에 접근할 때
	-	private인 부모의 멤버에는 접근할 수 없다.

<pre><code>
package kr.co.inforpub.j4android;
public class clockCity extends Geo{
  private String timezoneId="Asia/Seoul"
  private String countryNmae="Korea"

  public ClockCity(double lat, double lng, String timezoneId, String countryName){
    super(lat, lng);
    this.timezoneId = timezoneId;
    this.countryName = countryName;
  }
  public ClockCity(String timezoneId, String countryName){
    super();
    this.timeoneId = timezoneId;
    this.countryName = countryName;
  }
  public ClockCity(){
    super();
  }
}
</code></pre>

#### Section 3-4. 계층 구조의 특징

-	안드로이드는 상속을 많이 함
-	View는 모든 component(widget)의 부모
-	계층구조: 자식이 부모보다 더 구체적으로 구현한다.

<pre><code>
// Veiw -> TextView -> Button
public class Button extends TextView {
  public Button(Context context) {
    super((Context)null, (AttributeSet)null, 0, 0);
  }
}
public class TextView extends View implements OnPreDrawListener {
  public TextView(Context context) {
    super((Contet)null, (AttributeSet)null, 0, 0);
  }
}
</code></pre>

#### Section 3-5. overriding

-	오버라이딩 : 자식 클래스에서 부모 클래스의 메서드와 동일한 메서드를 정의하는 것
	-	메서드의 내용을 추가, 변경, 수정

#### Section 3-6. 다형성

-	부모의 메서드가 자식의 종류에 따라 다양한 형태로 나타낼 수 있는 특징

-	부모 타입으로 자식 생성

<pre><code>View mv=new MainView(this);</code></pre>

-	부모타입으로 자식을 받는다.

<pre><code>
MainView mv2=new MainView(this);
View v=mv2;
</code></pre>

-	부모의 메서드로 자식의 메서드를 호출할 수 있다.
	-	생성하거나 간접호출(invalidate())을 이용하여 View의 onDraw()를 호출하면 오버라이딩한 자식의 onDraw()를 호출하여 사람이나 원 또는 시계를 그린다.

#### Section 3-7. instanceof keyword

-	자식이 여러 개인 경우에는 생성된 객체의 인스턴스가 누구 것인지를 확인할 필요가 있다.

#### Section 3-8. 추상 클래스

-	abstract
-	추상 메서드 : 바디({})가 없는 메서드
-	추상 클래스 : 추상 메서드를 한 개 이상 갖는 클래스
-	자식 클래스는 이를 상속해서 구현해야 한다.

<pre><code>
public abstract class AsynTask<Params, Progress, Result> {
  protected abstract Result doInBackground(Params... prams);
}
</code></pre>

#### Section 3-9. interface

-	인터페이스 : 모든 메서드가 구현되지 않은 추상 클래스

<pre><code>
public interface OnClickListener {
  void onClick(View v);
}
</code></pre>

-	인터페이스를 구현할 때는 implements를 사용

<pre><code>
class MYClickListen implements View.onClickListener{
  public void onClick(View v){
    ~implementation~
  }
}
</code></pre>

-	java에서 다중 상속은 불가
-	인터페이스를 이용해서 다중상속을 흉내낼 수 있다.
	-	상속을 한 번 받은 뒤, 인터페이스 구현을 여러 번

#### Section 3-10. parameterized type\*\*

-	파라미터화 된 타입은 메서드가 호출되어야 타입이 결정된다.

<pre><code>
public static <T extends View> T findViewById(View view, int rid) {
  return (T) view.findViewById(rid);
}
Button button = findViewById(view, R.id.button);
</code></pre>

#### Section 3-11. thread

-	그 thread

##### 3-11.1. 쓰레드 계층 구조

-	thread : java.lang.Thread
-	operation : java.lang.Runnable.run()
-	Runnable 인터페이스를 구현한 클래스를 만들거나
	-	Thread 클래스를 상속해서 run() 메서드를 오버라이딩 하면 된다.

##### 3-11.2. 쓰레드를 만들고 실행하는 법

-	Runnable 구현

<pre><code>
// runnable을 구현한 class를 만들고 run() method를 overriding하고 object를 생성
MyRunnable myr = new MyRunnable();
// thread를 생성하면서 runnable object의 주소를 thread의 생성자에 대입
Thread t1 = new Thread(myr);
// thread 작업을 시작
t1.start();
</code></pre>

-	thread를 상속하는 법

<pre><code>
// thread를 상속하고 run() method를 overriding하고 object 생성
MyThread t2 = new MyThread();
//thread 시작
t2.start()
</code></pre>

-	Anonymous Nested Class : 안드로이드에서는 이름없이 구현하기도 한다.

<pre><code>
for (SovereignFlag flag:aa) {
  new Thread( new Runnable() {
    @Override
    public void run() {
      save(flag.getShortname().toLowerCase(), size(flag.getFlag(), 32*6));
    }
  }).start();
}
</code></pre>

#### Section 3-12. IO

-	read text in web

<pre><code>
URL url = new URL(newUrls); // address
// search and connect
HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
// 빨대 꽂기
InputStream inputStream = new BufferedInputStream(urlConnection.getInputStream());
// 호스 연결
BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream, "UTF-8"), 8);

StringBuilder sb = new StringBuilder();
String line = "";
while ((line = reader.readLine()) != null) { // 한줄씩 가져오기
  sb.append(line+"\n"); // 문자열 붙이기
}

String jsonString = sb.toString();
</code></pre>

-	read image in web

<pre><code>
// address
URL url = new URL(ss);
// 주소 찾고 연결
HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
// 빨대 꽂기
InputStream inputStream = urlConnection.getInputStream();
// 이미지 읽기
Bitmap bmp = BitmapFactory.decodeStream(inputStream);
</code></pre>
