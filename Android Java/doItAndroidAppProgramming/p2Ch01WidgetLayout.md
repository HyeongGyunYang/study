Do It Android App Programming
=============================

Part 2
------

Chapter 1 기본 위젯과 레이아웃
------------------------------

### Section 1. 안드로이드 스튜디오

### Section 2. View and View Group

-	view (control, widget): ui 구성요소. 사용자의 눈에 보이는 화면의 구성요소
-	view group : view들의 set. but, view에서 상속을 받아오면 view group도 view로 표현할 수 있다.
-	Composite Pattern : view group이 view들의 집합이면서 view를 상속. 뛰어난 확장성
-	Widget : view 중에서 일반적인 컨트롤 역할을 하는 것.
-	Layout : view 중에서 내부에 view들을 포함하고 있으면서 그것들을 배치하는 역할을 하는 것.
-	기본/핵심 view class
	-	object <- view
	-	TextView <- Button
	-	ViewGroup <- LinearLayout

#### Section 2-1. View의 Attribution

-	기본/핵심 View attribution
	-	layout_width, layout_height : 뷰의 폭과 높이 설정
	-	id : view의 아이디 지정
	-	background : 뷰의 배경 설정

##### Section 2-1-1. layout_width, layout_height

-	view는 view group이 가지고 있는 여유 공간 중 일부 또는 전부에 놓이게 된다.
-	match_parent: 전부
-	wrap_content: 내용물의 크기에 따라
-	int: 크기를 고정된 값으로
	-	px
	-	dp / dip\** : 해상도에 비례해 비슷한 크기로 보이게 하려면
	-	sp / sip : view말고 텍스트에 사용. 글꼴을 기준으로
	-	in
	-	mm
	-	em

##### Section 2-1-2. id

-	XML layout에 정의된 뷰 참조하기
	-	inflation: 자동으로 java 객체가 생성
	-	java에서는 메모리의 어디에 객체가 생성되었는지 알 수 없다.
	-	id를 지정한 뒤에 사용
-	토스트 메시지 출력 : 버튼이 클릭되었는지 확인하는 방법

<pre><code>
button1.setOnClickListener(new OnClickListener() {

  @override
  public void onClick(View v) {
    Toast.makeText(getApplicationContext(), "버튼이 클릭되었습니다.",
                    Toast.LENGTH_LONG).show();
  }

  })
</code></pre>

##### Section 2-1-3. background

-	색상
	-	format
	-	#RGB
	-	#ARGB
	-	#RRGGBB
	-	#AARRGGBB
-	이미지
	-	이미지 리소스위 위치를 값으로 준다.

### Section 3. Layout

-	attribution
	-	fill model : 뷰를 부모뷰의 여유 공간에 어떻게 채울 것인지 설정
	-	orientation : 뷰를 추가하는 방향을 설정
	-	gravity : 뷰의 정렬 방향 설정
	-	padding : 뷰의 여유 공간 설정
	-	weight : 뷰가 차지하는 공간의 가중치 값 설정
-	layout의 종류
	-	linear 레이아웃: box model
	-	사각형 영역들을 이용해 화면을 구성하는 방법
	-	표준 자바의 BoxLayout과 유사
	-	상대 레이아웃: 규칙기반 모델
	-	부모 컨테이너나 다른 뷰와의 상대적 위치를 이용해 화면을 구성하는 방법
	-	프레임 레이아웃: 기본 단위 모델
	-	하나의 뷰만을 보여주는 방법
	-	가장 단순하지만 여러 개의 뷰를 추가하는 경우 충첩시킬 수 있으므로 뷰를 중첩한 후 각 뷰를 전화하여 보여주는 방식으로 사용할 때 유용함
	-	테이블 레이아웃: 격자 모델
	-	격자 모양의 배열을 이용하여 화면을 구성하는 방법
	-	HTML에서 많이 사용하는 정렬 방식과 유사하여 실용적임
	-	스크롤 뷰:
	-	스크롤이 가능한 컨테이너
	-	뷰 또는 뷰그룹이 들어갈 수 있으며 화면 영역을 넘어갈 때 스크롤 기능 제공

### Section 4 Linear Layout

#### Section 4-1. 방향 설정하기

-	뷰를 차례로 추가할 때, 가로/세로로 추가할지 결정

#### Section 4-2. 자바코드에서 화면 구성하기

#### Section 4-3. 정렬 방향 설정하기

#### Section 4-4. 여유공간 설정하기

#### Section 4-5. 공간가중치 설정하기
