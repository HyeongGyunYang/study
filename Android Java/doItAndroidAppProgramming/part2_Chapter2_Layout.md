Do It Android App Programming
=============================

Part2 Chapter2 Layout
---------------------

### Section 1 다양한 레이아웃

-	constraint layout : 디폴트
-	linear layout* : 박스 모델
-	relative layout* :
-	frame layout* : 중첩
-	table layout : 격자형. grid view를 더 많이 쓴다.
-	scroll view :

-	view의 영역

	-	box
	-	margin
	-	border
	-	padding

-	background 속성

	-	#AARRGGBB

### Section 2 Linear Layout

\-

### Section 3 Relative Layout

-	constraint보다 기능이 적다.

### Section 4 Frame Layout

-	중첩
-	숨겨둔 뷰를 상위로
-	숨겨뒀다 보여주는 효과가 있다.
-	frame layout + visibility 같이 사용

### Section 5 Table Layout

-	gallery
