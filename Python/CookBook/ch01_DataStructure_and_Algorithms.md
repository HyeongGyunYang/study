## 1. Unpacking a Sequence into Separate Variables

-	[unpacking all iterable object](ch01/iter.py)

## 2. Unpacking Elements from Iterables of Arbitrary Length

- [unpacking remain elements](ch01/iter.py)

## 3. Keeping the Last N Items

-	[collections.deque](https://docs.python.org/3/library/collections.html)
-	[implementation](ch01/deque.py)

## 4. Finding the Largest or Smallest N Items

-	[heapq](https://docs.python.org/3.0/library/heapq.html)
-	[heapq nlargest(), nsmallest(), heapify()](ch01/heapq.py)

## 5. Implementing a Priority Queue

-	[priority queue class](ch01/priorityQueue.py)
  -	idea : (prior, item)을 묶어서 처리하면 우선순위를 비교할 수 있다.

## 6. Mapping Keys to Multiple Values in a Dictionary

-	[collections.defaultdict](ch01/defaultdict.py)

## 7. Keeping Dictionaries in Order

-	[collections.OrderedDict](ch01/orderedDict.py)
-	일반 딕에 비해서 용량이 두 배 이상 크다. 추가적인 연결리스트를 생성하기 때문
  - Doubly LL 유지

## 8. Calculating with Dictionaries

-	[dict, zip, key](ch01/dict.py)
- zip() create an iterator that can only be consumed once

## 9. Finding Commonalities in Two Dictionaries

-	dict의 .keys()나 .values(), .items() method는 set을 반환해줘서 &,-,|로 연산 가능
- 즉, dict is a mapping btw a set of keys and values

## 10. removing Duplicates from a Sequence while Maintaining Order

-	[ordered set - dedupe class](ch01/orderedSet.py)
- set() doesn't preserve any kind of ordering

## 11. Naming a Slice

-	[slice(start,stop,step)](ch01/slice.py)
  - all iter

## 12. Determining the Most Frequently Occurring Items in a Sequence

-	[collections.Counter](ch01/counter.py)
- hashable

### 13. Sorting a List of Dictionaries by a Common Key

- [sorting dict - imtegetter, lambda](ch01/dict.py)

### 14. Sorting Objects Without Native Comparison Support

-	[operator.attrgetter()](ch01/attrgetter.py)
-	람다를 쓰던, 속성게터를 쓰던 맘대로지만, 속성게터가 좀 더 빠르고 범용성 있음

### 15. Grouping Records Together Based on a Field

-	[itertools.groupby](ch01/group.py)

### 16. Filtering Sequence Elements

-	[filtering skills](ch01/filter.py)

### 17. Extracting a Subset of a Dictionary

-	[filtering skills](ch01/filter.py)

### 18. Mapping Names to Sequence Elements

-	[collections.namedtuple](ch01/nameTuple.py)

### 19. Transforming and Reducing Data at the Same Time

-	[filtering skills](ch01/filter.py)

### 20. Combining Multiple Mappings into a Single Mapping

-	collections.ChainMap : 각 키에 값들이 연결되어 있음
-	체인맵은 참조를 묶은 거라서, 체인맵에 사용된 값을 변경하면 같이 변경됨
-	update를 사용하면 별개의 딕셔너리가 생겨서 그렇지 않음
-	[chainMap](ch01/chainMap.py)
