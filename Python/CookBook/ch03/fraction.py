from fractions import Fraction

a = Fraction(5,4)
b = Fraction(7,16)
print(a+b)
print('a', a.numerator, a.denominator)
x = 3.75
print(Fraction(*x.as_integer_ratio()))
print(float(b))
print(b.limit_denominator(8))
