x = 1234.567799
print(format(x, '0,.1f'))
print(format(x, '0.2E'))
print(format(x, 'e'))
