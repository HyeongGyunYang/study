from decimal import Decimal
a = Decimal('1.3')
b = Decimal('2.1')
print(a+b) # Decimal('6.3')
print((a+b)==Decimal('6.3'))

from decimal import localcontext
with localcontext() as ctx:
    ctx.prec = 3
    print(a/b)

import math
nums = [1.23e+18, 1, -1.23e+18]
print(math.fsum(nums))
