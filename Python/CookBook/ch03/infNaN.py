import math
a = [float('inf'), float('-inf'), float('nan')]
for i in a:
    print(math.isinf(i), math.isnan(i))
print(a[0]*10, 10/a[0], a[0]+a[1], a[0]/a[0], a[0]-a[1])
k = float('nan')
print(a[-1]==k)
print(k==None)
