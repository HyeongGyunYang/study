# data .... byte type..

len(data) # 16
x = int.from_bytes(data, 'little') # long long int

data1 = x.to_bytes(16, 'big')
data2 = x.to_bytes(16, 'little')

import struct
hi, lo = struct.unpack('>QQ', data)

# too big
nbytes, rem = divmod(x.bit_length(), 8)
if rem : nbytes+=1
x.to_bytes(nbytes, 'little')
