from collections import namedtuple

Subscriber = namedtuple('Subscriber', ['addr','joined'])
sub = Subscriber('jone', '2012')
print(sub.addr)
print(Subscriber)
