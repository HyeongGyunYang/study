from collections import defaultdict

d = defaultdict(list)
d['a'].append(1)

d = defaultdict(set)
d['a'].add(1)

d={}
d.setdefault('a', []).append(1)
