from collections import ChainMap
# a,b : dict
a = {'x':1, 'y':2}
b = {'y':3, 'z':4}
c = ChainMap(a,b)
print(c)
d = dict(a)
print(d)
d.update(b)
print(d)
print(c['y'])
