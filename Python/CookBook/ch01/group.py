from operator import itemgetter
from itertools import groupby

# rows: list of dictionaries
rows.sort(key=itemgetter('date'))

for date, items in groupby(rows, key=itemgetter('date')):
    print(date)
    for i in items:
        print(' '*4, i)
