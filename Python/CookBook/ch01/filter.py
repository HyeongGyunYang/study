# mylist
[n for n in mylist if n>0]
gen = (n for n in mylist if n>0)
def is_int(val):
    try:
        x = int(val)
        return True
    except ValueError:
        return False

filter(is_int, mylist)

from itertools import compress
yourlist = [n>5 for n in mylist]
list(compress(mylist, yourlist))


{key:value for key,value in dict.items() if value>200}

sum(x*x for x in lists)
