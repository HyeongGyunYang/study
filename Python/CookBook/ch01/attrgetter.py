class User:
    def __init__(self, user_id):
        self.user_id = user_id

    def __repr__(self):
        return 'User({})'.format(self.user_id)

users = [User(34),User(23),User(33)]
sorted(users, key=lambda u: u.user_id)

from operator import attrgetter
sorted(users, key=attrgetter('user_id'))
