import binascii

h = binascii.b2a_hex(s)
s = binascii.a2b_hex(h)

import base64

h = base64.b16encode(s)
s = base64.b16decode(h)

print(h.decode('ascii'))
