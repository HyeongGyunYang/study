doc.find('content/{http://www.w3.org/1999/xhtml}html') # work
doc.find('content/html') # doesn't work
doc.findtext('content/{http://www.w3.org/1999/xhtml}html/'
    '{http://www.w3.org/1999/xhtml}head/{http://www.w3.org/1999/xhtml}title') # works
doc.findtext('content/{http://www.w3.org/1999/xhtml}html/head/title') # doesn't work

class XmLNamespaces:
    def __init__(self, **kwargs):
        self.namespaces={}
        for name, url in kwargs.items():
            self.register(name,uri)
    def register(self, name, uri):
        self.namespaces[name] = '{'+uri+'}'
    def __call__(self, path):
        return path.format_map(self.namespaces)

for xml.etree.ElementTree import iterparse

for evt, elem in iterparse('ns2.xml', ('end', 'start-ns', 'end-ns')):
    print(evt, elem)
