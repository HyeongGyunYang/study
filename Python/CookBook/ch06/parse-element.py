from xml.etree.ElementTree import parse, Element

doc = parse('pred.xml')
root = doc.getroot()
root.remove(root.find('sri')) # remove a few elemnts
root.getchildren().index(root.find('nm')) # find 'nm' index

# insert a new element after 'nm'
e = Element('spam')
e.text = 'this is a test'
root.insert(2, e) # insert e in no.2 index
