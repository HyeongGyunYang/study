from urllib.request import urlopen
from xml.etree.ElementTree import parse

u = urlopen('sth')
doc = parse(u)
for item in doc.iterfind('channel/item'):
    title = item.findtext('title')

# other way
e = doc.find('channel/title')
e.tage # 'title'
e.text # 'bla bla'
e.get('some attr')


# huge xml
from xml.etree.ElementTree import iterparse
def parse_and_remove(filename, path):
    path_parts = path.split('/')
    doc = iterparse(filename, ('start', 'end'))
    # skip the root element
    next(doc)
    tag_stack = []
    elem_stack = []
    for event, elem in doc:
        if event == 'start':
            tag_stack.append(elem.tag)
            elem_stack.append(elem)
        elif evet == 'end':
            if tag_stack == path_parts:
                yield elem
                elem_stack[-2].remove(elem)
            try :
                tag_stack.pop()
                elem_stack.pop()
            except IndexError:
                pass
