import csv
from collections import namedtuple

# Reading
with open('stocks.csv') as f:
    f_csv = csv.reader(f, delimiter=',')
    headers = next(f_csv)
    Row = namedtuple('Row', headings)
    for r in f_csv:
        row = Row(*r)
        # process row

with ...:
    f_csv = csv.DictReader(f)
    ...

# ValueError Handling
import re
with open('stocks.csv') as f:
    f_csv = csv.reader(f, delimiter=',')
    headers = [re.sub('[^a-zA-Z_]', '_', h) for h in next(f_csv)]
    Row = namedtuple('Row', headings)
    for r in f_csv:
        row = Row(*r)
        # process row

# Convert Types : and more ways
col_types = [str, float, str, str, float, int]
with open('stocks.csv') as f:
    f_csv = csv.reader(f, delimiter=',')
    headers = [re.sub('[^a-zA-Z_]', '_', h) for h in next(f_csv)]
    for r in f_csv:
        row = tuple(convert(value) for convert, value in zip(col_types, row))
        # process row
        
# writing
with open('stocks.csv','w') as f:
    f_csv = csv.writer(f)
    f_csv.writerow(headers)
    f_csv.writerows(rows)

with open('stocks.csv','w') as f:
    f_csv = csv.Dictwriter(f, headers)
    f_csv.writeheader()
    f_csv.writerows(rows)

# Other Way
with open('stocks.csv') as f:
    for line in f:
        row = line.split(',')
        # process row
