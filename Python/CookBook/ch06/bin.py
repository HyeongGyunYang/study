from struct import Struct

def write_records(records, format, f):
    record_struct = Struct(format)
    for r in records:
        f.write(record_struct.pack(*r))

if __name__=='__main__':
    records = [(...) ...]

    with open('data.b', 'wb') as f:
        write_records(records, '<idd', f)

def read_records(format, f):
    record_struct = Struct(format)
    chunks = iter(lambda : f.read(record_struct.size), b'')
    return (record_struct.unpack(chunk) for chunk in chunks)
    
