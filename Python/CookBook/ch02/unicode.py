import unicodedata
s1 = 'a\u00f1o'
s2 = 'an\u0303o'
print(s1, s2, s1==s2, len(s1), len(s2))

t1 = unicodedata.normalize('NFC', s1)
t2 = unicodedata.normalize('NFC', s2)
t3 = unicodedata.normalize('NFD', s1)
t4 = unicodedata.normalize('NFD', s2)

print(t1==t2, t3==t4, t1, t2, t3, t4)
