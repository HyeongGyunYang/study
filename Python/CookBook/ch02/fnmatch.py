from fnmatch import fnmatch, fnmatchcase
print(fnmatch('foo.txt', '*.txt'))
fnmatchcase('foo.txt', '*.TXT') # False
