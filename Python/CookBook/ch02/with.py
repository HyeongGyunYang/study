import os
filenames = os.listdir('.')
for filename in filenames:
    print(filename.endswith('.py'))
    print(filename.startswith('0'))

from urllib.request import urlopen
def read_data(name):
    if name.startswith(('http:', 'https:', 'ftp:')):
        return urlopen(name).read()
    else:
        with open(name) as f:
            return f.read()
url = 'http://www.python.org'
read_data(url)

import re
re.match('http:|https:|ftp:', url)
