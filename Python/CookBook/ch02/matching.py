'yes no'.find('no') # 4

import re
datepat = re.compile(r'\d+/\d+/\d+')
text1 = '11/27/2012'
text2 = 'Nov 27, 2012'
print(datepat.findall(text1))
datepat2 = re.compile(r'(\d+)/(\d+)/(\d+)')
m = datepat2.match(text1)
print(m.group(0))
print(m.groups())
