import re
text = 'foo = 23 + 42'

NAME = r'?P<NAME>[a-zA-Z_][a-zA-Z_0-9]*)'
EQ = r'(?P<EQ>=)'
masterPat = re.compile('|'.join([NAME, EQ]))
scanner = masterPat.scanner('foo=42')
