## 1. reading and writing csv data

- [csv module](ch06/csv.py)
- [csv docs](https://docs.python.org/3/library/csv.html)

## 2. reading and writing json data

- [json module](ch06/json.py)
- [json docs](https://docs.python.org/3/library/json.html)
- [pprint docs](https://docs.python.org/3/library/pprint.html)
- [urllib docs](https://docs.python.org/3/library/urllib.html)

## 3. parsing simple xml data

- [xml](ch06/xml.py)

## 4. parsing huge xml files incrementally

- [huge xml](ch06/xml.py)

## 5. turning a dictionary into xml

- [dic to xml](ch06/dic-to-xml.py)

## 6. parsing, modifying, and, rewriting xml

- [xml parse](ch06/parse-element.py)

## 7. Parsing XML Documents with Namespaces

- [xml w/ namespaces](ch06/xml-namespace.py)

## 8. interacting with a relational database

- [RDB, sqlite3 module](ch06/rdb.py)
- [sqlite3 docs](https://docs.python.org/3/library/sqlite3.html)

## 9. decoding and encoding hexadecimal digits

- [hexadeciaml digits](ch06/hexadecimal.py)

## 10. decoding and encoding base64

- [base 64](ch06/hexadecimal.py)

## 11. reading and writing binary arrays of structures

- [bin array](ch06/bin.py)

## 12. reading nested and variable-sized binary structures

- binary-encoded data : [struct module](https://docs.python.org/3/library/struct.html) can be used to decode and encode almost any kind of bin

## 13. summarizing data and performing statistics

- [pandas](https://pandas.pydata.org/)
