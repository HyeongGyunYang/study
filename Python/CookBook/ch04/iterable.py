from collections import Iterable

def flatten(items, ignore_types=(str, bytes)):
    for x in items :
        if isinstance(x, Iterable) and not isinstance(x, ignore_types):
            yield from flatten(x)
        else:
            yield x

# iter in iter version
def flatten2(items, ignore_types=(str, bytes)):
    for x in items :
        if isinstance(x, Iterable) and not isinstance(x, ignore_types):
            for i in flatten(x):
                yield i
        else:
            yield x
