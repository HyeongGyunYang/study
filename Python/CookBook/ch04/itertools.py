import itertools

itertools.islice(it, start, end)

for line in itertools.dropwhile(lambda line : ine.startswith('#'), f):
    print(line, end='')
# 주석 부분(#로시작하면 drop한다)을 스킵하고 본문부터 출력하는 코드

from itertools import permutations, combinations
items = list(range(3))
for p in permutations(items, 2):
    print(p) # grouped 2

for c in combinations(items, 2):
    print(c)

from itertools import chain
for x in chain(a,b):
    print(x) # return elements of a after that b ones
