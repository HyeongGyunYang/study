## 1. Manually Consuming an Iterator

-	next() : exception StopIteration

## 2. Delegating Iteration

- [iter](ch04/iter.py)
-	\_\_iter\__() : delegates iteration to the internally held container
-	\_\_next\__() : carry out the actual iteration

## 3. Creating New Iteration Patterns with Generators

-	[yield](ch04/yield.py)

## 4. Implementing the Iterator Protocol

-	[iteration](ch04/iter.py)

## 5. Iterating in Reverse

- reversed(iter)

## 6. Defining Generator Functions with Extra State

- [line history](ch04/generator.py)
-	with generators, it is easy to fall into a trap of trying to do everything with functions alone.

## 7. Taking a Slice of an Iterator

- [iter slice](ch04/itertools.py)
- iterators and generators can't normally be sliced
- itertools.islice() : it works like discarding items before start

## 8. Skipping the First Part of an Iterable

- [itertools.dropwhile()](ch04/itertools.py)

## 9. Iterating over all posiible combinations or permutations

- [itertools.combinations(), .permutations()](ch04/itertools.py)

## 10. Iterating over the index value pairs of a sequence

- [enum](ch04/enum.py)
- 묶인 값 풀어줄 때 주의할 것.

## 11. Iterating over multiple sequences simultaneously

- [zip](ch04/zip.py)
- zip_longest 안하면 짤려서 iter
- 기본적으로 튜플로 반환

## 12. Iterating on Items in Separate Containers

- [itertools.chain()](ch04/itertools.py)

## 13. Creating data processing pipelines

- it works in specific case. it just show

## 14. flattening a nested sequence

- [collections.Iterable()](ch04/iterable.py)

## 15. Iterating in sorted order over merged sorted iterables

- [heapq.mearge()](ch04/heapq.py)
- it works like itertools.chain() AND SORTED

## 16. Replacing infinite while loops with an iterator

- nothing to incredible
