## 1. reading and writing text data

- [Official open() docs](https://docs.python.org/3/library/functions.html#open)
- [built in open(r, w)](ch05/open.py)
  - [Additional topic of print(file=)](https://docs.python.org/3/library/functions.html#print)
  - open() : text mode - default option

## 2. printing to a file

- Redirect : in [\#1](ch05/open.py) already do that

## 3. printing with a different separator or line ending

- [print(\*args)](ch05/print.py)

## 4. reading and writing binary data

- [Using open](ch05/open.py)
- [encoding - decoding](ch05/bin.py)
- bin I/O is that objects such as arrays and C structures can be used for writing
  without any kind of intermediate conversion to a bytes objects
- [Additional topic of array library](https://docs.python.org/3/library/array.html)

## 5. Prevent Overwrite : writing to a file that doesn't already exist

- using open() 'x' mode
  - [same as..](ch05/overwrite.py)

## 6. performing I/O operations on a string

- file-like object : [io.StringIO(), io.BytesIO()](ch05/io.py)
- they can work like file without real file

## 7. reading and writing compressed datafiles

- [gzip, bz2 + open()](ch05/zip.py)
  - bite type default
- collaborate with normal open(), these modules can work with various file-like objects

## 8. iterating over fixed-sized records

- [neat trick : iter(), functools.partial()](ch05/iter.py)
- functools.partial() creates callable
- iter() creates iters

## 9. reading binary data into a mutable buffer

- [bin data directly into a mutable buffer](ch05/bin.py)

## 10. Memory mapping binary files

- [memory map : mmap](ch05/meMap.py)
- pass

## 11. Manipulating pathnames

- [pathnames : os.path](ch05/pathname.py)

## 12. testing for the existence of a file

- [os.path, time](ch05/osPath.py)

## 13. Getting directory listing

- [os.listdir()](ch05/osPath.py)
  - return all files, subdirectories, symbolic links, and so forth

## 14. Bypassing Filename Encoding

- [os.listdir()](ch05/filenameEncoding.py)

## 15.Printing bad filenames

- crashed UnicodeEncodeError exception and a "surrogates not allowed"
- printing filenames of unknown origin - just handle errors

## 16. Adding or Changing the Encoding of an Already Open File

- [w/o closing opened file first](ch05/openedFile.py)

## 17. Writing Bytes to a Text File

- write raw bytes to a file opened in text mode
- [sys.stdout.buffer.write()](ch05/writeByte.py)

## 18. Wrapping an Existing File Descriptor As a File Object

- [os.open()](ch05/lowLebel.py)
- hard to understand 'Discussion' part

## 19. Making Temporary Files and Directories

- [tempfile module](ch05/tempfile.py)

## 20. Communicating with Serial Ports

- [serial module](ch05/serial-ports.py)

## 21. Serializing Python Objects

- [pickle](ch05/pickle.py)
