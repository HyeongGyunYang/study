## 1. Rounding Numerical Values

-	[format('0.2f'), round(num, 3)](ch03/round.py)

## 2. Performing Accurate Decimal Calculations

-	[Decimal, fsum](ch03/accurate.py)
-	Module : decimal
	-	Decimal()
	-	localcontext().prec
-	Module : math
	-	fsum()

## 3. Formatting Numbers for Output

-	[format](ch03/format.py)
-	foramt : e, ','

## 4. Working with Binary, Octal, and Hexadecimal Integers

-	[bin, octal, hexa](ch03/bin.py)
-	be careful! syntax error

## 5. Packing and Unpacking Large Integers from Bytes

-	[byte int](ch03/byte.py)
-	if you handle 16-element byte string (128-bit integer value)
- struct module : ch6.11
- how to handle the too large int

## 6. Performing Complex-Valued Math

-	[complex, cmath](ch03/complex.py)
-	complex(real, imag)
	-	.real
	-	.imag
-	1+1j
-	Module : numpy, cmath에서 complex number 동작

## 7. Working with Infinity and NaNs

-	[inf, nan](ch03/infNaN.py)
-	float('inf'/'-inf'/'nan')
-	calculating of special values
-	safe way to test for a NaN value : math.isnan()
- fpectl module : if you want to raise error when it returns inf or NaN

## 8. Calculating with Fractions

-	[fractions](ch03/fraction.py)
-	Module : fractions
	-	class : Fraction(a, b)
	-	.numerator, .denominator
	-	.limit_denominator() # 분모의 크기 제한
-	float() : fraction -> float
-	float.as_integer_ratio() : float -> fraction

## 9. Calculating with Large Numerical Arrays

-	[numpy](ch03/np.py)
- list : can't calculate with int/float
- np array : can do it
-	difference btw list and numpy array : scalar/vector calculation

## 10. Performing Matrix and Linear Algebra Calculations

- [np.matrix(), numpy.linalg](ch03/np.py)
- matrices :  follow linear algebra rules for computation
-	linalg : can use linear algebra calculations

## 11. Picking Things at Random

- [random](ch03/random.py)
- [random module official docs](https://docs.python.org/3/library/random.html)
-	Mersenne Twister Algorithm
-	[ssl module docs](https://docs.python.org/3/library/ssl.html)
	-	use for cryptography
	-	ssl.RAND_bytes()

## 12. Converting Days to Seconds, and Other Basic Time Conversions

-	[datetime module docs](https://docs.python.org/3/library/datetime.html)
-	[dateutil module docs](https://pypi.org/project/python-dateutil/)
	-	time zones, fuzzy time ranges, calculating the dates of holidays, and so forth

## 13. Determining Last Friday's Date

- [last Friday](ch03/friday.py)

## 14. Finding the Date Range for the Current Month

- [calendar module docs](https://pypi.org/project/python-dateutil/)

## 15. Converting Strings into Datetimes

- datetime.datetime.strptime()

## 16. Manipulating Dates Involving Time Zones

-	[pytz module docs](https://pypi.org/project/pytz/)
-	convert all dates to UTC time
