-	built-in methods
-	regular expressions
-	creation of a full-fledged parser
-	a tricky aspects of working with Unicode

## 1. Splitting Strings on Any of Multiple Delimiters

-	[re module official Docs](https://docs.python.org/3/library/re.html)
-	[jump to python - re](https://wikidocs.net/4308)
-	[re.split(), str.join](ch02/re.py)

## 2. Matching Text at the Start or End of a String

-	[str.startswith, str.endswith, re.match](ch02/with.py)
	- <code>re.match()</code> is often overkill for simple matching

## 3. Matching Strings Using Shell Wildcard Patterns

-	[fnmatch](ch02/fnmatch.py)
	-	wildcard : \*, ?

## 4. Matching and Searching for Text Patterns

-	[str.find(), re.compile(), re.compile().findall(), re.compile().match().groups()](ch02/matching.py)

## 5. Searching and Replacing Text

-	[str.replace(), re.sub(), re.subn()](ch02/replacing.py)
	- subn : 몇 번 substitution 되었는지
- [calendar Library](https://docs.python.org/3/library/calendar.html)

## 6. Searching and Replacing Case-Insensitive Text

-	[re.IGNORECASE flag](re-ignorecase.py)

## 7. Specifying a Regular Expression for the shortest Match

-	[re with short match](re-short.py)
-	\* : longest , \*?를 사용하면 깔끔하게 마감

## 8. Writing a Regular Expression for Multiline Patterns

-	[multiline patterns](multiline.py)
- re.DOTALL : .(dot) matched every characters

## 9. Normalizing Unicode Text to a Standard Representation

-	[unicodedata Library](https://docs.python.org/3/library/unicodedata.html)
-	[unicodedata.normalize(), .combining()](ch02/unicode.py)
-	in unicode, certain characters can be represented by more than one valid sequence of code points

## 10. Working with Unicode Characters in Regular Expressions

## 11. Stripping Unwanted Characters from Strings

-	string.lstrip(), .rstrip(), .strip(), .replace(' ', ''), re.sub('\s+',' ',string_obj)

## 12. Sanitizing and Cleaning Up Text

- [re.translate()](ch02/remap.py)
-	re_obj.translate(remap) : 여기서 remap은 dict 형식으로 대체될 말을 저장해 놓은 것.
-	unicodedata.combining
- [dict.fromkeys()](http://www.w3big.com/ko/python/att-dictionary-fromkeys.html)

## 13. Aligning Text Strings

- [align](ch02/align.py)
- left, right, center align

## 14. Combining and Concatenating Strings

- [.join()](ch02/join.py)

## 15. Interpolating Variables in Strings

- [format](ch02/strFormat.py)
- format, format_map + class

## 16. Reformatting Text to a Fixed Number of Columns

- [number of characters in each columns](ch02/textwrap.py)
- textwrap.fill(), os.get_terminal_size().columns

## 17. Handling HTML and XML Entities in Text

- [html, xml](ch02/html.py)
- [html module](https://docs.python.org/ko/3/library/html.html)
- xml 관련 modules :

## 18. Tokenizing Text

- [tokenizing](ch02/tokenizing.py)
- Be careful the order of compile

### 19. Writing a Simple Recursive Descent Parser

- using in grammar rule

### 20. Performing Text Operations on Byte Strings

- string methods work with byte strings, byte arrays
- re with byte compile -> working on byte strings
