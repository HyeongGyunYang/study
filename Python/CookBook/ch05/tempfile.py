from tempfile import TemporaryFile, TemporaryDirectory
import tempfile

with TemporaryFile('w+t', endocing='', errors='', delete='') as f:
    # read and write to the file
    f.write('Hello World\n')
    f.write('Testing\n')
    print('filename is : ', f.name)
    # seek back to beginning and read the data
    f.seek(0)
    data = f.read()

# other way
f = TemporaryFile('w+t')
# Use the temporary file
f.close()

with TemporaryDirectory() as dirname:
    print('dirname is : ', dirname)

tempfile.mkstemp() # bla bla bla
