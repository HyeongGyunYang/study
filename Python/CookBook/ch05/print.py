print('sth', sep=',', end='!')

row = iterator
# using string methods
print(','.join(str(x) for x in row))

# using iterator
print(*row, sep=',')
