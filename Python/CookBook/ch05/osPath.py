import os

path = '/Users/file.bin'
os.path.basename(path) # 'file.bin'
os.path.dirname(path) # '/Users'

os.path.join('tmp', os.path.basename(path)) # 'tmp/file.bin'
path = '~/file.bin'
os.path.expanduser(path) # '/Users/file.bin'
os.path.splitext(path) # '/Users/file', '.bin'

os.path.exists('/tmp/spam')
os.path.isfile('/tmp/spam')
os.path.isdir('/tmp/spam')
os.path.realpath('tmp/spam')

os.path.getsize()
os.path.getmtime()
import time
time.ctime(os.path.getmtime())

names = os.listdir('dir')
names = [name for name in os.listdir('dir') if os.path.isfile(os.path.join('dir', name))]
import glob
pyfiles = glob.glob('dir/*.py')
from fnmatch import fmatch
pyfiles = [name for name in os.listdir('dir') if fnmatch(name, '*.py')]

# os.listdir() additional topics
os.listdir('.') # current dir
os.listdir(b'.') # current dir with byte string 
