# read the entire file as a single string
with open('somfile.txt', 'r') as f:
    data = f.read()

# iterate over the lines of the file
with open('somefile.txt', 'r') as f:
    for line in f:
        # process line

# write chunks of text data
with open('somefile.txt', 'w') as f :
    f.write(text1)
    f.write(text2)
    ...

# dredirected print statement
with open('somefile.txt', 'w') as f:
    print(line1, file=f)
    print(line2, file=f)

# encoding
with open('filename', 'r', encoding='latin-1') as f:

## handling uncorrect encodings
open('file', 'r', encoding='ascii', errors='replace')
'ignore'

# newline translation disabled
open('file', 'r', newline='')

# do not with 'with'
f = open()
data = f.read()
f.close()
