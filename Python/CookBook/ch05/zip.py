import gzip, bz2

with gzip.open('file.gz', 'rt') as f:
    text = f.read()

with bz2.open('file.bz2', 'wt', compresslevel = 5) as f:
    f.write(text)

# other usage
f = open('file.gz', 'rb')
with gzip.open(f, 'rt') as g :
    text = g.read()
