from functools import partial

recordSize = 32

with open('somefile.data', 'rb') as f:
    records = iter(partial(f.read, recordSize), b'')
    for r in records: # processing
