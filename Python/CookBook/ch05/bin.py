with open('file.bin', 'rb') as f:
    data = f.read(16)
    text = data.decode('utf-8')

with open('file.bin', 'wb') as f :
    text = 'Hello World'
    f.write(text.encode('utf-8'))

# bin I/O objects are written directly w/o bin convertor
import array
nums = array.array('i', [1,2,3,4])
with open('file.bin', 'wb') as f:
    f.write(nums)

# directly into a mutable buffer
import os.path

def read_into_buffer(filename):
    buf = bytearray(os.path.getsize(filename))
    with open(filename, 'rb') as f:
        f.readinto(buf)
    return buf

recordSize = 32
buf = bytearray(recordSize)
with open('somefile', 'rb') as f :
    while True :
        n = f.readinto(buf)
        if n < recordSize:
            break
