import pickle
# common usage
data = ...
f = open('somefile', 'wb')
pickle.dump(data, f)

# to dump an object to a string
s = pickle.dumps(data)

# restore from a file
f = open('somefile', 'rb')
data = pickle.load(f)

#restore from a string
data = pickle.loads(s)

# ADVANCED USAGE
