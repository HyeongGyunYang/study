## 1. writing functions that accept any number of arguments

- '''python
def foo(*args):
'''
  - tuple
- '''python
def foo(**kwargs):
'''
  - dict

## 2. writing functions that only accept keyword arguments

- '''python
def foo(a, *, b):
'''
  - b를 명시해서 입력하게 된다.

## 3. attaching informational metadata to function arguments

- '''python
def add(x:int, y:int) -> int:
  return x+y
'''

## 4. returning multiple values from a function

- default : tuple

## 5. defining functions with default arguments

- '''python
def foo(x=30):
'''
- AVOID to set mutable to default arguments

## 6. defining anonymous or inline functions

- '''python
add=lambda x, y : x+y
'''
- Typically, lambda is used in the context of some other operation
