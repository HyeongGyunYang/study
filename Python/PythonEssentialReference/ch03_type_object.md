### 1. 용어

-	Object는 identity, type(class), value를 가진다.
	-	identity : memory를 가리키는 pointer라고 생각할 수 있다.
		-	변수명은 그 포인터를 가리키는 이름!
	-	type
		-	객체의 내부적인 표현 형태와 객체가 지원하는 메서드 및 연산들을 설명
-	instance
	-	특정 타입의 객체를 해당 타입의 instance라고 부르기도 함
	-	신원과 타입은 변경불가
	-	객체의 값의 변경 가능 여부에 따라 (im)mutable
-	Container / collection
	-	다른 객체에 대한 참조를 담는 객체
-	Object는 대부분 attribution과 method로 특정지어진다.

### 2. 객체 신원과 타입

-	id(): 객체의 신원을 나타내는 정수를 반환
	-	항상 메모리 주소인 것은 아니다.
-	is : 두 객체의 신원을 비교
	-	== : 값을 비교
-	type 검사
	-	is로 비교
	-	isinstance(obj, type)
		-	상속받은 클래스를 검사하므로, '리스트와 유사한지' 검사하기는 힘들다.

### 3. 참조 횟수와 쓰레기 수집

-	sys.getrefcount() : 참조횟수 반환
-	참조횟수가 0이면 garbage collection 수행
-	circular dependency가 발생해서 접근 불가능하지만 참조횟수가 0이 아닌 메모리 누수 발생
	-	cycle detector 구동

### 4. 참조와 복사

-	<code>a=b</code>같은 대입이 이뤄지면
	-	immutable에 대해서는 복사가 생성되는 것처럼 동작
	-	mutable에 대해서는 참조를 공유한다.
-	<code>copy.deepcopy()</code>

### 5. 1급 객체

-	파이썬의 모든 객체는 1급 객체
-	식별자로 명명될 수 있는 모든 객체는 동일한 지위를 갖는다.
-	이를 활용하면 많은 일을 영리하게 해결할 수 있다.

### 6. 데이터 표현을 위한 내장 타입

#### 6-1. None

-	값을 반환하지 않는 함수에 의해서 반환된다.
-	생략해도 되는 인수의 기본 값으로 흔히 사용되어, 함수에서 호출자 쪽에서 해당 인수에 대한 값을 지정하였는지의 여부를 검사할 수 있게 한다.
-	아무 속성도 없다
-	False로 평가됨

#### 6-2. Numeric

-	Boolean
-	int
	-	fraction과의 호환을 위해 .numerator, .denominator 속성을 갖는다.
	-	complex와의 호환을 위해 .real, .imag, .conjugate()
-	float
	-	64비트만 지원
	-	정확히 제어하고 싶으며 numpy를 활용
	-	complex와의 호환을 위해 .real, .imag, .conjugate()
	-	fraction과의 호환을 위해 .as_integer_ratio()
	-	정수 검사 : .is_integer()
-	complex
-	Module decimal
-	Module fractions

#### 6-3. Sequence

-	음수가 아닌 정수로 색인되는 순서 있는 객체들의 모음
-	문자열, 리스트, 튜플 등
-	모든 순서열은 반복을 지원

##### 6-3-1. 모든 순서열에 공통적인 연산

-	slice, len, min, max, sum, all, any
-	Mutable의 추가 연산
	-	slice : 항목 대입
	-	del

#### 6-4. 리스트

#### 6-5. 문자열

-	어느 메서드도 문자열 자체를 변경하지는 않는다.

#### 6-6. 매핑타입

-	거의 임의적인 키 값으로 색인되는 임의의 객체들
-	순서 X
-	숫자, 문자열 및 기타 다른 객체로 색인 가능

##### 6-6-1. Dict

#### 6-7. set

### 7. 프로그램 구조를 나타내는 내장 타입

#### 7-1. Callable type

-	함수 호출 연산을 지원하는 객체

##### 7-1-1. User defined function

-	기본 속성 : doc, name, dict, code, defaults, globals, closure

##### 7-1-2. Method

-	instance method
	-	type : type.MethodType
	-	주어진 클래스에 속하는 인스턴스에 대해 수행되는 메서드
	-	(self) 첫번째 인수로 인스턴스가 전달된다.
-	class method
	-	\@classmethod
	-	type : type.MethodType
	-	클래스 자체를 객체로 보고 여기에 대해 수행되는 메서드
	-	(cls) 첫번째 인수로 클래스 객체가 전달된다.
-	static method
	-	\@staticmethod
	-	클래스 안에 함께 묶인 함수
	-	첫번째 인수로 인스턴스나 클래스를 받지 않는다.
-	객체에서 속성이나 메소드를 찾는 연산과 그것이 수행되는 연산은 별개의 단계로 수행된다.

-	bound method

	-	type : type.MethodType
	-	method와 instance를 둘 다 감싸는 callable object
	-	자동으로 method를 첫 번째 인수로 전달

	<pre><code>
	# Foo : class
	f = Foo() # f : instance
	meth = f.instance_method
	meth()
	</code></pre>

-	unbound method

	-	적절한 타입의 인스턴스가 호출될 것을 기대.
	-	그냥 함수 객체를 얻게 됨
	-	self에 대한 타입 검사를 실시하지 않음

	<pre><code>
	# Foo : class
	umeth = Foo.instance_method
	umeth()
	</code></pre>

-	type.MethodType

	-	attr : doc, name, class, func, self(없으면 None)

##### 7-1-3. 내장 함수와 메서드

-	type.BuiltinFunctionType
-	attr : doc, name, self

##### 7-1-4. 호출가능한 클래스와 인스턴스

-	callable class : \_\_init\__()으로 전달
-	callable instance : \_\_call\__()을 정의해서 함수를 흉내낼 수 있다.

#### 7-2. class, type, instance

-	class
	-	type: type
	-	attr : doc, name, bases, dict, module, abstractmethods
-	instance
	-	type : 해당 클래스
	-	attr : class, dict
	-	dict대신 slots을 사용하면 더 효율적일 수도

#### 7-3. Module

-	name space를 정의한다.
-	attr : dict, doc, name, file, path

### 8. 인터프리터의 내부를 나타내는 내장 타입

-	도구를 제작하거나 프레임워크를 설계할 때 유용하게 쓸 수 있다.

#### 8-1. 코드 객체

-	바이트 컴파일된 미가공 실행 코드

#### 8-2. 프레임 객체

-	실행 프레임을 나타낸다
-	역추적 객체에서 주로 사용된다.

#### 8-3. 역추적 객체

-	예외가 발생할 때 생성
-	스택 추적 정보를 담는다.

#### 8-4. 생성기 객체

-	생성기 함수가 호출되면 생성
-	yield가 사용되면 생성기 함수가 정의
-	반복자의 역할과 생성기 함수 자체에 대한 정보를 담는다

#### 8-5. 분할 객체

-	slicing할 때 생성
-	attr : start, stop, step
-	method
	-	indices() : 길이를 받아서 그 길이의 순서열에 분할이 어떻게 적용되는지를 알려주는 튜플을 반환

#### 8-6. Ellipsis

-	색인 검색에서 생략 부호가 있다는 것을 나타내는 데 사용된다.

### 9. 객체의 작동 방식과 특수 메서드

-	모든 기본적인 인터프리터 연산은 특수한 객체 메서드에 의해 구현
-	각 데이터 타입의 작동 방식은 온전히 그 타입이 구현하고 있는 특수한 메서드들의 집합에 따라 결정된다.

#### 9-1. 객체 생성 및 파괴

-	new, init, del

#### 9-2. 객체의 문자열 표현

-	format, repr, str

#### 9-3. 객체 비교와 순서 매기기

-	객체 검사 및 해싱을 위한 특수 메서드
-	비교를 위한 메서드

#### 9-4. 타입 검사

#### 9-5. 속성 접근

#### 9-6. 속성 감싸기 및 기술자

#### 9-7. 순서열 및 매핑 메서드

#### 9-8. 반복

#### 9-9. 수학연산

#### 9-10. 호출가능 인터페이스

#### 9-11. 호출가능 인터페이스

#### 9-12. 컨텍스트 관리 프로토콜

#### 9-13. 객체 검사와 dir()
