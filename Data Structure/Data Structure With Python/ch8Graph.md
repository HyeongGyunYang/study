Data Structure with Python
==========================

Chapter 8 Graph
---------------

-	광범위한 분야에 쓰이는 자료구조
-	curriculum
	-	그래프 용어
	-	그래프 기본 연산 : 깊이우선탐색, 너비우선탐색
	-	연결성분 찾는 법, 위상정렬
	-	이중연결성분, 강연결성분
	-	최소신장트리를 찾기 위한 알고리즘
	-	최단경로를 찾는 알고리즘

### Section 1 그래프

#### Section 1-1. 그래프 용어

-	graph는 vertex와 edge의 집합
	-	하나의 간선은 두 개의 정점을 연결
	-	G = (V,E)로 표현
	-	V는 정점의 집합, E는 간선의 집합
-	종류
	-	directed graph : 간선에 방향이 있는 그래프
	-	undirected graph : 방향이 없음
	-	weighted graph : 간선에 가중치 부여
	-	subgraph : 주어진 그래프의 정점과 간선의 일부분으로 이루어진 그래프
	-	tree : 싸이클이 없는 그래프
	-	spanning tree : 주어진 그래프가 하나의 연결성분으로 구성되어 있을 때, 그래프의 모든 정점들을 싸이클 없이 연결하는 부분그래프
-	간선
	-	(a, b) : a와 b를 연결하는 무방향 그래프
	-	<a, b> : a -> b로 방향이 있는 그래프
-	용어
	-	degree : 정점 a에 인접한 정점의 수
	-	in-degree / out-degree : 방향 그래프에서는 구분
	-	path : 시작 정점 u부터 도착점 v까지의 정점들을 나열하여 표현
	-	simple path : 경로 상의 정점들이 모두 다른 경로
	-	cycle : 시작점과 도착점이 동일한 단순경로
	-	connected component : 그래프에서 정점들이 서로 연결되어 있는 부분

#### Section 1-2. 그래프 자료구조

-	adjacency matrix: n*n 2d list에 저장
	-	mat[i][j]에 i와 j의 간선 유무 또는 가중치 저장
-	adjacency list: 각 정점마다 1개의 단순연결리스트를 이용하여 인접한 각 정점을 노드에 저장한다.
	-	희소그래프를 효율적으로 저장
-	파이썬에서는 둘을 혼합해서 표현 가능
	-	1d list에 정점에 해당하는 인덱스에 연결된 정점들의 리스트를 저장해서 2d list로 표현
-	그 외에도 여러가지 표현법이 있다.

-	희소그래프와 조밀 그래프

	-	sparse graph : 정점의 차수가 작다.
	-	실세계의 대부분 그래프
	-	dense graph : 간선의 수가 최대 간선 수에 근접한 그래프

### Section 2 그래프 탐색

-	모든 정점에 방문하기

#### Section 2-1. 깊이우선탐색

-	임의의 정점에서 시작하여 이웃하는 하나의 정점을 방문하고, 방금 방문한 정점의 이웃 정점을 방문하며, 이웃하는 정점을 모두 방문한 경우에는 이전 정점으로 되돌아 가서 탐색을 수행하는 방식으로 진행된다.
-	Core Idea : 실타래를 풀면서 미로의 출구를 찾는 것
-	<a href="ch8/dfs.py">dfs.py</a>
-	depth first spanning tree: 입력 그래프가 하나의 연결성분으로 되어 있을 때, dfs를 수행하며 실선으로 만들어지는 트리
	-	dfs방문 순서대로 정점을 그리면 트리가 만들어진다.
	-	back edge: 탐색 중 이미 방문된 정점에 도달한 경우
-	수행시간
	-	각 정점을 한 번씩 방문 + 각 간선을 1번씩 사용 : O(N+M)

#### Section 2-2. 너비우선탐색

-	임의의 정점 s에서 시작하여 s의 이웃 정점을 모두 방문한 뒤, 방문한 정점들의 이웃정점을 방문하는 방식
	-	이진트리의 레벨순회와 유사
-	Core Idea : 연못에 돌을 던졌을 때, 물결이 동심원을 그리며 퍼져나가는 모양
-	<a href="ch8/bfs.py">bfs.py</a>
-	breadth first spanning tree : 입력 그래프가 하나의 연결성분으로 되어 있을 때, bfs를 수행하며 실선으로 만들어지는 트리
	-	cross edge : 탐색 중 이미 방문된 정점에 도달한 경우
-	수행시간: O(N+M)
-	응용
	-	신장트리, 연결성분, 경로, 싸이클 찾기
	-	BFS: 최소 간선을 사용하는 경로 찾기
	-	DFS: 위상정렬, 이중연결성분, 강연결성분 찾기

### Section 3 기본적인 그래프 알고리즘

-	무방향 그래프에서 연결성분 찾기
-	싸이클이 없는 방향 그래프에서 위상정렬 찾기
-	이중연결성분 및 강연결성분에 대해

#### Section 3-1. 연결성분 찾기

-	여기서는 dfs 이용
-	Core Idea : 주어진 정점으로부터 dfs를 수행하여 방문되는 정점들을 리스트로 모아서 1개의 연결성분을 만든다. 다음엔 방문 안된 정점으로부터 같은 과정을 반복하여 다른 연결 성분들을 추출한다.
-	<a href="ch8/cc.py">연결성분찾기</a>
-	수행시간
	-	O(N+M)

#### Section 3-2. 위상정렬

-	topological sort : 싸이클이 없는 방향 그래프(Directed Acyclic Graph, DAG)에서 정점들을 선형순서(일렬)로 나열하는 것
	-	<u, v>에서 u가 v보다 앞서 나열되어야 한다.
-	주어진 그래프에 따라 여러 개의 위상정렬이 존재할 수 있다.
-	작업(task)들 사이에 의존관계가 존재할 때 수행 가능한 작업 순서를 도식화하는 데에 위상정렬을 사용
-	수행 방법
	1.	순방향 방법 : 그래프에서 진입 차수가 0인 정점v로부터 시작하여 v를 그래프에서 제거하는 과정을 반복하는 방법
		-	각 정점의 진입 차수를 알기 위해서는 인접리스트를 각 정점으로 진입하는 정점들의 인접리스트로 바꾸어야 하는 단점
	2.	역방향 방법 : 진출차수가 0인 정점 v를 출력하고 v를 그래프에서 제거하는 과정을 반복하여 얻은 출력 리스트를 역순으로 만들어 결과를 얻는 방법
-	역방향 Core Idea : DFS를 수해앟며 각 정점 v의 인접한 모든 정점들의 방문이 끝나자마자 v를 리스트에 추가한다. 리스트가 완성되면 리스트를 역순으로 출력한다.
-	<a href='topological_sort.py'>위상정렬</a>
-	수행시간
	-	O(N+M)

##### Section 3-2-2. 이중연결성분

-	Biconnected Component : 무방향그래프의 연결성분에서 임의의 두 정점들 사이에 적오도 2개의 단순경로가 존재하는 연결성분
-	특징
	-	정점 하나가 삭제되도 연결성분 내에 정점들 사이의 연결이 유지
-	응용
	-	통신 네트워크 보안, 전력 공급 네트워크 등에서 네트워크의 견고성을 분석하는 방법
-	반대 개념
	-	articulation point / cut point : 하나의 정점을 삭제했을 때 2개 이상의 연결성분들로 분리될 때의 삭제된 정점
	-	Bridge : 한 간선을 제거했을 때 두 개 이상의 연결성분들로 분리될 때의 삭제된 간선
	-	특징
	-	단절 정점은 이웃한 이중연결성분들에 동시에 속한다.
	-	다리 간선은 그 자체로 하나의 이중연결성분
-	수행시간 : 이중연결성분을 찾는 알고리즘 O(N+M)

##### Section 3-2-3. 강연결성분

-	strongly connected component: 방향그래프에서 연결성분 내의 임의의 두 정점 u와 v에 대해 정점 u에서 v로 가는 경로가 존재하고 동시에 반대의 경로도 존재하는 연결성분
-	응용
	-	sns에서 community를 분석하는데 활용
	-	인터넷의 웹 페이지 분석
-	수행시간 : 강연결성분을 찾는 알고리즘 O(N+M)

### Section 4 최소신장트리

-	minmum spanning tree, MST : 하나의 연결성분으로 이루어진 무방향 가중치 그래프에서 간선의 가중치 합이 최소인 신장트리

#### Section 4-1. Kruskal 알고리즘

-	kruskal algorithm : 간선들을 가중치가 감소하지 않는 순서로 정렬한 후, 가중치가 가장 작은 간선이 싸이클을 만들지 않으면 트리 간선으로 선택하며, 이 과정을 반복하여 n-1개의 간선이 선택되었을 때 알고리즘을 종료한다.
-	pseudo code

<pre><code>
[1] 가중치가 감소하지 않는 순서로 간선 리스트 L을 만든다
[2] while 트리의 간선 수 < (N-1):
[3]   L에서 가장 작은 가중치를 가진 간선 e를 가져오고, e를 L에서 제거한다.
[4]   if 간선 e가 T에 추가하여 싸이클을 만들지 않으면:
[5]     간선 e를 T에 추가
</code></pre>

-	<a href="ch8/kruskal.py">Kruskal algorithm</a>
	-	추가하려는 간선이 싸이클을 만드는지 여부는 union과 find를 통해 판별
	-	union 연산: 2개의 집합을 하나의 집합으로 만드는 연산
	-	find 연산: 인자가 속한 집합의 대표노드(root)를 찾는 연산
		-	path compression: 모든 자식노드가 root를 가리키게 한다.
-	disjoint set: 서로 중복된 원소를 가지고 있지 않은 집합들

	-	1d list로 표현 가능
	-	각 원소를 0~n-1로 놓고 인덱스로 활용
	-	각 집합은 루트가 대표한다.
	-	루트의 리스트 원소에는 루트 자신이 저장
	-	루트가 아닌 노드의 원소에는 부모노드가 저장

-	수행시간

	-	간선을 정렬하는데 O(MlogN)
	-	트리에 간선을 추가하려할 때 find와 union을 수행하는 시간 O((M+N)log*N)
	-	즉 O(MlogN)

#### Section 4-2. Prim 알고리즘

-	Prim Algorithm : 임의의 시작 정점에서 가장 가까운 정점을 추가하여 간선이 하나의 트리를 만들고, 만들어진 트리에 인접한 가장 가까운 정점을 하나씩 추가하여 최소신장트리를 만든다.
-	Pseudo Code

<pre><code>
[1] 리스트 D를 inf로 초기화. 단, 임의의 시작 정점 s의 D[s]=0
[2] while T의 정점 수 < N:
[3]         T에 속하지 않은 각 정점 i에 대해 D[i]가 최소인 정점 min_vertex를 찾아 T에 추가
[4]         for T에 속하지 않은 각 정점 w에 대해서 :
[5]                 if 간선 (min_vertex, w)의 가중치 < D[w] :
[6]                         D[w] = 간선 (min_vertex, w)의 가중치
</code></pre>

-	<a href="ch8/prim.py">Prim Algorithm</a>
-	수행시간
	-	그냥은 O(N2)
	-	min_vertex를 찾을 때, bin-heap을 사용하면 O(NlogN)

#### Section 4-3. Sollin 알고리즘

-	Solling Algorithm : 각 정점을 독립적인 트리로 간주하고, 각 트리에 연결된 간선들 중에서 가장 작은 가중치를 가진 간선을 선택한다. 이 때 선택된 간선을 두 개의 트리를 하나의 트리로 만든다. 같은 방법으로 1개의 트리가 남을 때까지 각 트리에서 최소 가중치 간선을 선택하여 연결한다.
-	장점
	-	Parallel Algorithm으로 구현이 쉽다.
-	Pseudo

<pre><code>
[1] 각 정점은 독립적인 트리이다.
[2] repeat
[3]         각 트리에 닿아 있는 간선들 중에서 가장 작은 가중치를 가진 간선을 선택하여 트리를 합친다.
[4] until (1개의 트리만 남을 때까지)
</code></pre>

-	수행시간

	-	O(MlogN)

### Section 5 최단경로 알고리즘

-	시작점에서 나머지 n-1개의정점까지의 최단경로를 찾는 Dijkstra

-	모든 쌍의 정점에 대해 최단경로를 찾는 Floyd-Warshall

#### Section 5-1. Dijkstra 알고리즘

-	Prim과 유사하지만, 다른 점
	-	Dijkstra는 출발점이 주어지지만, Prim은 아니다.
	-	Prim에서는 D[i]에 간선 (i, v)의 가중치가 저장되지만, Dijkstra에서는 D[i]에 출발점으로부터 정점 i까지의 거리가 저장된다.
-	Pseudo Code

<pre><code>
[1] D를 inf로 init. D[s]=0으로 init.
[2] for k in range(N):
[3]     방문 안된 각 정점 i에 대해 D[i]가 최소인 정점 min_vertex를 찾고, 방문한다.
[4]     for min_vertex에 인접한 각 정점 w에 대해:
[5]         if w가 방문 안된 정점이면:
[6]             if D[min_vertex]+간선(min_vertex, w)의 가중치 < D[w]:
[7]                 D[w] = D[min_vertex] + 간선(imn_vertex, w)의 가중치 # 간선완화
[8]                 previous[w] = min_vertex
</code></pre>

-	Core Idea : min_vertex를 찾아서 방문하고, min_vertex의 방문 안된 인접한 정점들에 대한 간선완화를 수행한다.
-	각 정점의 최단 경로는 previous를 역추적하여 출력한다.
-	<a href = "ch8/dijkstra.py">Dijkstra Algorithm</a>
-	수행시간
	-	O(N2)
	-	bin-heap을 사용하면 O(NlogN)
	-	Greedy Algorithm
-	음수 가중치
	-	입력 그래프에 음수 가중치가 있으면 최단경로 찾기에 실패할 수도 있다.

#### Section 5-2. Floyd-Warshall 알고리즘

-	Floyd-Warshall Algorithm / All Pairs Shortest Paths Algorithm
-	Core Idea :입력 그래프의 정점들에 0~n-1로 id를 부여하고, 정점 id를 증가시키며 간선완화를 수행한다.
-	Pseudo Code

<pre><code>
[1] D = 입력 그래프의 인접행렬
[2] for k in range(N):
[3]     for i in range(N):
[4]         for j in range(N):
[5]             if D[i][j] > D[i][k]+D[k][j]:
[6]                 D[i][j] = D[i][k] + D[k][j]
</code></pre>

-	<a href="ch8/floyd_warshall.py">Floyd Warshall Algorithm</a>
-	수행시간
	-	O(N3)
	-	Dynamic Programming Algorithm
