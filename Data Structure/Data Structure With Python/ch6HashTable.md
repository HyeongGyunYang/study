Data Structure With Python
==========================

Chapter 6 Hash Table
--------------------

### Section 1 해시테이블

-	Core Idea : O(lgN)보다 빠른 연산을 위해 key와 1D list의 index 관계를 이용하여 key를 저장한다.
-	개념
	-	Hashing
		-	key를 1D list의 index로 그대로 사용하면 memory 누수가 발생
		-	그래서 키를 간단한 함수를 사용해 변환한 값을 리스트의 인덱스로 이용
	-	Hash Function : hashing에 이용하는 함수
	-	Hash Value or hash address: hash function이 계산한 값
	-	Hash Table : item이 hash value에 따라 저장되는 1D list
-	Collision
	-	서로 다른 키들이 동일한 해시값을 가질 때
	-	Open Addressing
		-	충돌된 키들을 해시테이블 전체를 열린 공간으로 여기어 비어 있는 곳을 찾아 항목을 저장하는 방식.
		-	충돌이 발생한 키를 원래의 해시값과 다른 곳에 저장
	-	Closed Addressing
		-	해시값에 대응되는 해시테이블 원소에 반드시 키를 저장
		-	충돌이 발생한 키들을 동일한 해시주소에 저장

### Section 2 해시함수

-	단순하면서 키를 균등하게 변환하는 함수를 만들자
	-	현실 세계의 키들은 그 자체가 의미를 가지고 있는 경우가 많아서, 키의 앞/뒤 몇자리를 해시값으로 취하는 방식은 충돌이 일어나기 쉽다.
	-	해시함수는 키를 간단한 계산을 통해 뒤죽박죽 만든 후 해시테이블의 크기에 맞도록 해시값을 계산한다. 이때 연산이 신속성을 잃으면 해시의 장점이 사라진다.
-	Mid-square : 키를 제곱한 뒤, 적절한 크기의 중간부분을 해시값으로 사용
-	Folding
	-	큰 십진수가 키일 경우
	-	몇자리씩 일정하게 끊어서 더한 후, 테이블의 크기에 맞는 숫자를 취한다.
-	Multiplicative
	-	h(key) = ((key * delta) % 1) * M
		-	delta <1, m = 해시테이블의 크기
	-	delta는 (sqrt(5)-1)/2를 추천
-	Division
	-	실제로 많이 씀
	-	h(key) = key % prime, prime은 소수
	-	소수를 쓰면 키를 균등하게 인덱스로 변환시킨다.
-	연산 중심 해시함수의 특징
	-	키의 모든 자리 숫자가 계산에 참여해서 원래 키에 부여된 의미나 특성을 찾아볼 수 없게 된다.
	-	계산 결과를 해시테이블 크기에 따라 일정부분만 사용

### Section 3 개방주소방식

#### Section 3-1. 선형조사

-	Def : 충돌이 일어난 원소부터 순차적으로 검색하여 처음 발견한 empty 원소에 충돌이 일어난 키를 저장
-	Core Idea : 충돌이 나면 바로 다음 원소를 검사하자
-	1차 군집화 Primary Clustering : 해시테이블이 키들이 빈틈없이 뭉쳐지는 현상
	-	문제 : 탐색, 삽입, 삭제 시, 군집된 키들을 순차적으로 방문해야한다.
	-	해시테이블에 empty 원소가 없을수록 심각해지며, 성능은 극단적으로 낮아짐
-	<a href='ch6/linear_prob.py'>Implementation</a>
-	삭제연산
	-	삭제할 원소에 실제 키들과 구별되는 값을 저장
	-	None으로 초기화 시키면, 탐색 시 실패로 간주되므로 불가

#### Section 3-2. 이차조사 Quadratic Probing

-	Core Idea : 충돌이 일어날수록 더 멀리 떨어진 원소를 검사하자
-	2차 군집화가 발생 Secondary Clustering
-	empty 원소가 있음에도 저장에 실패하는 경우가 발생
-	<a href="ch6/quad_prob.py">Implementation : Quadratic Probing</a>

#### Section 3-3. 랜덤조사 Random Probing

-	점프 시퀀스를 무작위화하여 empth 원소를 찾는 충돌 해결 방법
-	Core Idea : 충돌이 일어나면 일정한 규칙 없이 비어 있는 원소를 검사하자
-	의사 난수 생성기를 사용
-	3차 군집화 발생 Tertiary Clustering
-	응용 : 파이썬의 dictionary
-	<a href="ch6/rand_prob.py">Implementation : Random Probing</a>

#### Section 3-4. 이중해싱 Double Hashing

-	Core Idea : 충돌이 일어나면 다른 해시함수의 해시값을 이용하여 원소를 검사하자
-	방법
	-	$(h(key) + j*d(key)) mod M, j = 0, 1, 2, ...$
	-	$d(key)$는 점프 크기를 정하는 함수이므로 0을 리턴해선 안된다.
	-	$d(key)$의 값과 해시테이블의 크기 M이 서로소 관계일 때 좋은 성능
		-	M을 소수로 선택하면 조건은 자연 충족
-	장점
	-	모든 군집화를 해결하는 방법
	-	해시 성능을 저하시키지 않는 동시에 해시테이블에 많은 키들을 저장할 수 있다.
-	<a href="ch6/double_hashing.py">Implementation : Double Hashing</a>

### Section 4 폐쇄주소방식 Closed Addressing

-	키에 대한 해시값에 대응되는 곳에만 키를 저장한다.
-	Core Idea : 충돌이 난 키들을 같은 곳에 모아 놓자
-	Chaining 또는 Separate Chaining
	-	해시테이블 크기인 M개의 단순연결리스트를 가진다.
	-	키를 해시값에 대응되는 연결리스트에 저장하는 해시방식
-	단점
	-	레퍼런스가 차지하는 공간이 추가로 필요
	-	테이블 크기인 M이 항목 수인 N보다 너무 크면 많은 연결리스트들이 empty
	-	M이 N보다 너무 작으면 연결리스트들의 길이가 너무 길어져 해시 성능이 매우 낮아진다.
-	장점
	-	군집화 X
	-	구현이 간결
	-	실제로 많이 씀
-	<a href="ch6/chaining.py">Implementation : Closed Addressing - Chaining</a>

### Section 5 기타 해싱

#### Section 5-1. Two-way Chaining

-	Core Idea: 2개의 해시함수로 계산된 두 체인을 검사해서 짧은 체인에 새 항목을 삽입하자
-	단점
	-	2개의 해시함수를 계산
	-	연결리스트의 길이를 비교
	-	추후 탐색을 위해서 두 연결리스트를 탐색
-	장점
	-	연결리스트의 평균 길이가 O(loglogN)

#### Section 5-2. Cuckoo Hashing 뻐꾸기 해싱

-	Core Idea : 2개의 해시함수와 각 함수에 대응되는 해시테이블을 이용해 충돌이 발생하면 그 곳에 있는 키를 쫓아내자

---

1.	key = new_key
2.	h(hey) = i를 계산하여, htable[i]에 key를 저장한다.
3.	**if** key가 저장된 원소가 비어 있으면:
	1.	삽입을 종료한다.
4.	**else**: # key가 저장되면서 그 자리에 있떤 키를 쫓아낸 경우

	1.	key 때문에 쫓겨난 키를 old_key라고 하자
	2.	**if** old_key가 있던 테이블이 htable이면 :
		1.	d(old_key) = j를 계산하여, dtable[j]에 old_key를 저장한다.
	3.	**else**: # old_key가 있었던 테이블이 dtable이면
		1.	h(old_key) = j를 계산하여, htable[j]에 old_key를 저장한다.
	4.	key = old_key, go to step[3] ---

5.	단점

	-	싸이클이 발생하면 무한 루프 -> re-hash

6.	장점

	-	탐색과 삭제를 O(1) 시간에 보장
	-	삽입은 높은 확률로 O(1)

#### Section 5-3. Rehash

-	해시테이블에 비어있는 원소가 적으면 삽입에 실패하거나 해시 성능이 급격히 낮아진다.
-	Rehash : 해시테이블을 확장시키고 새로운 해시함수를 사용하여 모든 키들을 새로운 해시테이블에 다시 저장
-	off-line에서 수행
-	O(N)
-	재해시 수행여부
	-	Load Factor a : $a = N / M$
	-	$a >= 0.75$ : 크기를 2배로
	-	&a <= 0.25& : 크기를 1/2fh

#### Section 5-4. Dynamic Hashing

-	대용량 DB를 위한 해시방법
-	재해시를 수행하지 않고 동적으로 해시테이블의 크기를 조절

##### Section 5-4-1. Extendible Hashing

-	directory를 main memory에 저장하고, 데이터는 disk block 크기의 bucket 단위로 저장한다.
-	bucket : 키를 저장하는 곳
-	버킷에 overflow가 발생하면 새 버킷을 만들어 나누어 저장하며, 이때, 이 버킷들을 가리키던 디렉터리는 2배로 확장된다.

##### Section 5-4-2. Linear Hashing

-	디렉토리 없이 삽입할 때, 버킷을 순서대로 추가하는 방식
-	추가되는 버킷은 삽입되는 키가 저장되는 버킷과 무관하게 순차적으로 추가된다.
-	장점
	-	디렉터리를 사용하지 않는다
	-	인터렉티브 응용에 적합

### Section 6 해시방법의 성능 비교 및 응용

-	탐색/삽입을 수행할 때 성공/실패한 경우를 각각 분리하여 측정
-	선형조사
	-	적재율이 너무 작으면 해시테이블에 empty원소가 너무 많다.
	-	적재율이 너무 크면 군집화 심화
-	개방주소방식
	-	$a = 0.5, M = 2N$일 때 $O(1)$
-	체이닝
	-	$M=prime, a=10$일 때 $O(1)$
