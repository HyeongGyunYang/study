adj_list = [[2, 1], [3, 0], [3, 0], [9, 8, 2, 1],
            [5], [7, 6, 4], [7, 5], [6, 5], [3], [3]]

def dfs(v):
    visited[v] = True # 정점 v 방문
    lret.append(v) # 정점 v 출력
    for w in graph[v]: # 정점 v에 인접한 각 정점에 대해
        if not visited[w]:
            dfs(w, lret) # v에 인접한 방문 안된 정점 재귀호출
    return lret

def cc(g):
    ret = list()
    global graph
    global visited
    global lret
    graph = g
    N = len(graph) # 그래프 정점 수
    visited = [False for x in range(N)] # 방문되면 True로
    for i in range(N):
        lret = list()
        if not visited[i]:
            ret.append(dfs(i))
    return ret

cc = cc(adj_list)
