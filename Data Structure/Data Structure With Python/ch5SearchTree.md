Data Structure with Python
==========================

Chapter 5 Search Tree
---------------------

### Section 1. Binary Search

-	binary search : 1D list에 data가 ***정렬*** 되어 있을 때, 효율적으로 원하는 data를 찾는 algorithm

<pre><code>
def binary_search(a, left,right,t):
    if left>right: return None
    mid = (left+right)//2
    if a[mid]==t: return mid
    if a[mid]>t: return binary_search(a, mid-1, right,t)
    else: return binary_search(a, left, mid+1, t)
</code></pre>

-	O(logN)

-	단점: 삽입과 삭제가 빈번하면 정렬을 유지하기 위해 시간이 오래 걸린다.

-	삽입/삭제 시: O(N)

### Section 2. Binary Search Tree

-	Def: binary search tree condition: bin-tree로서 각 node가 다음과 같은 조건을 만족한다. 각 node n의 key가 n의 왼쪽 sub-tree의 keys보다 크고, n의 오른쪽 sub-tree의 nodes의 keys보다 작다.

-	binary search tree = binary search + tree

-	bin-search를 수행하기 위해 SLL을 변형시킨 자료구조

-	BST를 투영하면 1D list

<pre><code>
class BST:
    def __init__(self):
        self.root = None

    class node:
        def __init__(self, key, value, left=None, right=None):
            self.key = key
            self.value = value
            self.left = left
            self.right = right

    def get(self, key): # section 2-1.

    def put(self, key, value): # section 2-2.

    def min(self): # section 2-3.

    def delete_min(self): # section 2-4.

    def delete(self, key): # section 2-5.
</code></pre>

#### Section 2-1. 탐색연산

<pre><code>
def get(self, k):
    return self.get_item(self.root,k)

def get_item(self, n, k):
    if n == None:
        return None
    if n.key>k:
        return self.get_item(n.left, k)
    elif n.key<k:
        return self.get_item(n.right, k)
    else:
        return n.value
</code></pre>

#### Section 2-2. 삽입연산

<pre><code>
def put(self, key, value):
    self.root = self.put_item(self.root, key, value)

def put_item(self, n, key, value):
    if n == None:
        return self.node(key, value)
    if n.key >key:
        n.left = self.put_item(n.left, key, value)
    elif n.key <key:
        n.right = self.put_item(n.right, key, value)
    else:
        n.value = value
    return n
</code></pre>

#### Section 2-3. 최솟값 찾기

<pre><code>
def min(self):
    if self.root ==None:
        return None
    return self.minimum(self.root)

def minimum(self, n):
    if n.left ==None:
        return n
    return self.minimum(n.left)
</code></pre>

#### Section 2-4. 최솟값 삭제 연산

<pre><code>
def delete_min(self):
    if self.root == None:
        return None
    self.root = self.del_min(self.root)

def del_min(self, n):
    if n.left == None:
        return n.right
    n.left = self.del_min(n.left)
    return n
</code></pre>

#### Section 2-5. 삭제 연산

-	임의의 key를 가진 node 삭제하기
	1.	삭제할 node 찾기
	2.	삭제된 node의 parent와 children을 연결
		-	child==0 : parent가 n을 가리키던 reference를 None으로
		-	child==1 : child를 parent에 연결
		-	children==2: n을 root로 하는 sub-tree를 inorder traversal해서 n 다음 방문하는 node(inorder-successor)를 n의 자리로 옮긴다.

<pre><code>
def delete(self, k):
    self.root = self.del_node(self.root, k)

def del_node(self, n, k):
    if n==None:
        return None
    if n.key > k:
        n.left = self.del_node(n.left, k)
    elif n.key<k:
        n.right = self.del_node(n.right, k)
    else:
        if n.right == None:
            return n.left
        if n.left == None:
            return n.right
        target = n
        n = self.minimum(target.right)
        n.right = self.del_min(target.right)
        n.left = target.left
</code></pre>

-	수행시간
	-	탐색, 삽입, 삭제: height에 비례
		-	complete binary tree: O(lgN)
		-	bias binary tree: O(N)

### Section 3. AVL Tree

-	tree가 한쪽으로 치우쳐 자라나는 현상을 방지하여 tree 높이의 균형을 유지하는 BST

-	balanced bin-tree의 장점: search, insertion, deletion이 O(lgN)

-	Core Idea: 균형이 깨지면, 회전연산을 통해 균형을 회복

-	Def: 임의의 node n에 대해, n의 왼쪽 sub-tree의 높이와 오른쪽 sub-tree의 높이 차이가 1을 넘기지 않는 BST

-	AVL-tree의 높이는 lgN

-	fibonacci와의 관계: pass

#### Section 3-1. AVL tree의 회전 연산

-	rotate right
	-	pp162. 그림5-20

<pre><code>
def rotate_right(self, n):
    x = n.left
    n.left = x.right
    x.right = n
    n.height = max(self.height(n.left), slef.height(n.right))+1
    x.height = max(self.height(x.left), self.height(x.right))+1
    return x
</code></pre>

-	rotate left
	-	pp163. 그림 5-21

<pre><code>
def rotate_left(self, n):
    x = n.right
    n.right = x.left
    x.left = n
    n.height = max(self.height(n.left), self.height(n.right))+1
    x.height = mzx(self.height(x.left), self.height(x.right))+1
    return x
</code></pre>

-	LL rotation

-	RR rotation

-	LR rotation

-	RL rotation

-	각 회전 연산의 수행시간이 O(1)

-	회전 후의 모습이 같다

#### Section 3-2. 삽입연산

1.	BST와 동일하게 새 node 삽입

2.	새 node로부터 root로 거슬러 올라가며 각 node의 sub-tree 높이 갱신

3.	가장 먼저 불균형한 node 발견하면, rotation 실행

<pre><code>
class AVL:
    def __init__(self):
        self.root = None

    class node:
        def __init__(self, key, value, height, left=None, right=None):
            self.key = key
            self.value = value
            self.height = height
            self.left = left
            self.right = right

    def height(self, n):
        if n==None:
            return 0
        return n.height

    def balance(self, n): # handle an unbalanced node
        if self.bf(n)>1: # 왼쪽이 높을 때
            if self.bf(n.left)<0: # 왼쪽 자식은 오른쪽이 높을 때
                n.left = self.rotate_left(n.left)
            n = self.rotate_right(n)
        elif self.bf(n)<-1: # 오른쪽이 높을 때
            if self.bf(n.right)>0: # 오른쪽 자식은 왼쪽이 높을 때
                n.right = self.rotate_right(n.right)
            n=self.rotate_left(n)
        return n

    def bf(self, n):
        return self.height(n.left) - self.height(n.right)

    def put(self, key, value): # BST와 높이 계산, 균형 유지가 다르다.
        self.root = self.put_item(self.root, key, value)

    def put_item(self, n, key, value):
        if n == None:
            return self.node(key, value, 1)
        if n.key > key:
            n.left = self.put_item(n.left, key, value)
        elif n.key < key:
            n.right = self.put_item(n.right, key, value)
        else:
            n.value = value
            return n
        n.height = max(self.height(n.left), self.height(n.right))+1
        return self.balance(n)

    def rotate_right(self,n):
    def rotate_left(self, n):
    def delete(self, key):
    def delete_min(self):
    def min(self):
</code></pre>

#### Section 3-3. 삭제연산

-	단계

	1.	BST와 동일하게 삭제연산

	2.	삭제된 node로 부터 root로 거슬러 올라가며 불균형이 발생한 경우, 적절한 회전 연산 수행

-	Core Idea : 삭제 후 불균형이 발생하면 반대쪽에 삽입이 이루어져 불균형이 발생한 것으로 생각

	-	pp172. 그림 5-28

-	수행시간 및 평가

	-	균형잡기는 O(1)이므로, 탐색, 삽입, 삭제는 O(lgN)
	-	AVL은 잘 안쓴다.
	-	AVL은 거의 정렬된 data를 삽입한 후에 random order로 data를 탐색하는 경우 성능이 좋다.
	-	BST는 random order의 data를 삽입한 후에 random order로 data를 탐색하는 경우 가장 성능이 좋다.

### Section 4. 2-3 Tree, Red-Black Tree

#### Section 4-1. 2-3 tree

-	개념
	-	B-tree의 일종
	-	Def: 2-3tree는 내부노드의 차수가 2 or 3인 균형탐색 트리
-	구성
	-	2node, 3node : 차수가 각각 2, 3인 node
	-	root로부터 각 leaf까지 경로의 길이가 같고, 모든 이파리가 같은 층에 있는 완전균형트리
	-	2node로만 구성되어 있으면 포화이진트리
-	Core Idea : 2-3tree는 이파리노드들이 동일한 층에 있어야 하므로 트리가 위로 자라나거나 낮아진다.
	-	2-node의 key가 k1일때,
		-	왼쪽에는 k1보다 작은 키, 오른쪽에는 큰 키
	-	3node의 key가 k1, k2일 때,
		-	... 가운데에는 k1보다 크고 k2보다 작은 키
-	높이 : lg_2(N+1) ~ lg_3(N) = 0.63 * lg_2(N)
-	traversal : inorder와 유사한 방법
	-	3node에서 왼-k1-중-k2-오 순으로 방문
	-	따라서, 2-3 node에서 순회 결과 : 정렬된 key 순

##### section 4-1-2. 2-3-4 tree

-	장점
	-	2-3보다 높이가 낮아서 탐색, 삽입, 삭제가 빠르다.
	-	삽입 연산 시, 4node를 만날 때 미리 분리 연산을 수행할 수 있기 때문에 다시 이파리에서 위로 올라갈 때 분리 연산을 수행할 필요가 없다.
	-	삭제 시, 내려가면서 2node를 만날 때 미리 통합 연산을 수행해서 다시 올라올 때 수행할 필요가 없다.
-	단점
	-	어차피 O(lgN)

#### Section 4-2. red-black tree

-	개념
	-	실제로 AVL 보다 많이 쓰임
	-	2-3tree의 개념을 기반으로 함
	-	node에 색을 부여하여 트리의 균형을 유지
-	장점
	-	탐색, 삽입, 삭제 연산의 수행시간이 O(lgN)을 넘지 않는다.
-	단점
	-	균형을 유지하기 위해 삽입과 삭제 시 상당히 많은 경우를 고려해야 한다.
-	높이
	-	2 * lgN보다 작거나 같다.
	-	red node가 없으면 h = lgN
-	응용
	-	반드시 제한된 시간 내에 연산이 수행되어야 하는 경우에 매우 적합
	-	항공관제, 원자로 제어, 심박장치
	-	Java : java.util.TreeMap, java.util.TreeSet의 기본 자료구조
	-	C++: map, multimap, set, multiset
	-	Linux OS scheduler

##### section 4-2-2. Left-Leaning Red-Black Tree

-	Def
	-	root, None은 black
	-	연속 red link 규칙: root로부터 각 None까지 2개의 연속된 red link는 없다.
	-	동일 black linke 수 규칙: root로부터 각 None까지의 경로에 있는 black link 수는 모두 같다.
	-	red link 좌편향 규칙: red link는 왼쪽으로 기울어져 있다.
-	Core Idea : LLRB tree는 2-3 tree에서 각 3node에 있는 2개의 key를 두 node로 분리 저장하고 하나는 red, 다른 하나는 black으로 만든 형태와 같다.
	-	왼쪽은 red, 오른쪽은 black
	-	부모와 자식을 연결하는 link의 색은 자식의 색을 따르므로, 따로 저장할 필요가 없다.
-	장점
	-	삽입/삭제 시, 고려해야 할 점이 적어서 RB에 비해 1/5 길이의 코드
	-	AVL, 2-3, 2-3-4, RB보다 우수한 성능

### Section 5. B-Tree

-	개념
	-	다수의 키를 가진 노드로 구성되어 다방향 탐색이 가능한 완전균형트리
	-	대용량 데이터를 위해 고안된 자료구조
	-	DB의 기본 자료구조로 활용
-	Core Idea : 노드에 수백에서 수천 개의 키를 저장하여 트리의 높이를 낮추자
-	Def : 차수가 m인 b-tree는 다방향 탐색트리로서
	-	모든 이파리노드들은 동일한 깊이를 갖는다.
	-	각 내부노드의 자식 수는 [m/2] 이상 m 이하이다.
	-	루트의 자식 수는 2 이상이다.
-	특징
	-	최대 m-1개의 키를 저장할 수 있고,
	-	최대 m개의 서브트리를 가질 수 있다.
	-	노드의 키들은 정렬되어 있다.
	-	서브트리의 키들은 그 사이 값의 키를 가진다.
-	성능
	-	높이 : lg_m/2(N)
	-	O(lg_m/2(N))
	-	비교 횟수보다 몇 개의 디스크 페이지를 메인 메모리로 읽혀 들이는지가 중요
		-	1개의 노드가 1개의 디스크 페이지에 맞도록 차수m을 정한다.
		-	디스크와 메인 메모리 사이의 블록 이동 수를 최소화하기 위해서
		-	루트는 항상 메인 메모리에 상주시키는 것이 좋다.
-	응용
	-	HPFS High Performance File System : Windows OS file system
	-	HFS, HFS+ Hierarchical File System : Macintosh OS file system
	-	ReiserFS, SFS, Ext3FS, JFS : Linux OS file system
	-	DB : Oracle, DB2, Ingres, PostgreSQL

#### Section 5-1. 탐색연산

-	root에서 시작해서 적절한 서브트리를 골라서 탐색
-	각 노드에서 키를 고를 때는 이진탐색

#### Section 5-2. 삽입연산

-	기본적으로, 탐색연산과 동일하게 삽입되어야할 이파리노드를 찾는다.
-	새 키를 수용할 공간이 있다면 정렬상태를 유지하게 삽입
-	분리연산: 이미 m-1개의 키를 가지고 있다면, 새로운 키를 포함한 m개 중 중간값을 갖는 키를 부모노드로 올려보내고 남은 m-1개의 키를 반으로 나눠 저장한다.

#### Section 5-3. 삭제연산

-	삭제는 항상 이파리노드에서
-	삭제 대상이 이파리가 아니라면, BST처럼 중위선행자나 중위후속자를 삭제할 키와 교환한 후에 이파리노드에서 삭제를 수행
-	transfer 연산
	-	underflow: 키가 삭제 된 후에 키의 수가 m/2-1보다 작으면
	-	underflow발생시, 좌우의 적당한 형제노드로부터 1개의 키를 부모노드를 통해 이동
-	fusion 연산
	-	underflow발생 시, 이동연산이 불가능한 경우, 해당 노드와 형제노드를 1개의 노드로 통합하고 그 둘의 분기점 역할을 하던 부모노드의 키를 통합된 노드로 끌어내린다.

#### Section 5-4. B-tree의 확장

-	B*-tree
	-	b-tree로서 root를 제외한 다른 노드의 자식 수가 2/3M~M이어야 한다.
	-	b-tree보다 노드의 공간을 효율적으로 사용
-	B+-tree
	-	개념
		-	실제로 널리 쓰임
		-	오히려 이를 b-tree라고 부르기도 한다.
	-	차이
		-	처음 제안된 b-tree는 노드에 키와 해당 레퍼런스(포인터) 쌍인 인덱스를 저장
		-	반면에 b+는 키로만 b-tree를 구성하고 이파리에 키와 관련된 정보를 저장
	-	특징
		-	b-tree부분은 관련 이파리노드를 빠르게 찾을 수 있는 안내자 역할
		-	각 이파리노드는 전체 레코드를 순차적으로 접근할 수 있도록 연결리스트로 구성

### Exercise

1.	4번: root로부터 이파리노드까지의 평균, 가장 긴, 가장 짧은 길이가 lgN
2.	2번, 4번: 전체를 순회하는 거는 아마 리스트가 더 빠를 듯
3.	2번: 최소힙에서 최댓값을 찾는데 얼마나 걸리지? 엄청 뒤져야 하지 않나?
4.	2번 O(lgN)
5.	2번 O(lgN)
6.	2번 O(lgN)
7.	- 2번 O(lgN) : 균형될 경우
	-	3번 O(N) : 편향될 경우
8.	2번 O(lgN)
9.	3번 O(N) : AVL을 생각해보면 최대 n번 회전이 일어나야 한다.
10.	4번 O(NlgN) : 어떻게 정렬할 것인가
11.	2번 O(NlgN) : n번 삽입, 삽입 때마다 높이만큼 비교연산
12.	3번 이진탐색트리
13.	2번 O(lgN)
14.	5번 b-tree는 이파리노드들이 같은 층에 있다.
	-	참고로 2-3tree도 이파리노드들이 같은 층에 있다.
15.	5번 75 : 오타인듯. 직접 그려보면 된다.
16.	2번 4
17.	AVL tree는
	-	1번: 균형이진탐색트리이다
	-	2번: 최우선순위인 항목이 root가 아니므로 heap이라고 할 순 없다.
	-	3번, 4번 : 2-3tree나 red-black tree는 다른 개념
18.	3번 AVL트리는 서브트리의 높이 차가 1이하이다.
19.	1번 45: 2-3트리의 삽입방법에 대해 책에 기술되어 있지는 않지만 상관 없을 거 같은데
20.	3번 4 : 19랑 상동
21.	50의 오른쪽 자식노드
22.	우편향, 좌편향 되어서 6개의 노드를 방문해야한다.
23.	(중위후속자를 옮긴다.)50이 root, 50의 자리였던 70의 왼쪽 자식에 60
24.	<a href='ch5/bst.py'>bst.py</a>

<pre><code>
def search_max(self):
    curr = self.root
    while curr.right != None:
        curr = curr.right
    return curr

def delete_max(self):
    del self.search_max()
</code></pre>
25. <a href='ch5/bst.py'>bst.py</a>

<pre><code>
def trav_inorder(self, k, node=self.root):
    if node.left !=None:
        trav_inorder(k, node.left)
    cnt += 1
    if cnt == k : return node
    if node.right != None:
        trav_inorder(k, node.right)

def kth_smallest(self, k):
    cnt = -1
    return trav_inorder(k).key
</code></pre>

1.	15번과 유사해서 pass
2.	중위후속자를 삭제된 node위치로 보낸 뒤, 삭제한 위치부터 거슬러 올라가며 불균형 검사를 통해 균형을 맞춘다.
3.	<a href='avl.py'>avl.py</a>
4.	2-3 tree는 설명이 부실해서 좀 더 자료/공부가 필요할 듯. 삽입/삭제 연산에 관해서
5.	상동
6.	상동
7.	2-3-4 tree의 삭제 연산
8.	- red-black tree : 균형을 유지하기 위해 삽입/삭제 시 여러가지 경우를 고려해야 한다.
	-	LLRB: 4가지 조건을 추가함으로서 1/5분량의 코드만으로 구현 가능
9.	b-tree의 삽입연산 : 분리연산이 불가능하면 어떤 연산을 수행해야하지??
10.	fusion연산 수행
11.	Splay tree란?
