<!doctype html>
<html>
  <head>
    <meta charset = "utf-8">
    <title>
      <?php
        print_title()
      ?>
    </title>
    <?php
      $data_dir = scandir('./data');
      function print_title(){
        if (isset($_GET['id'])) {
          if (in_array($_GET['id'], $data_dir)) {
            echo $_GET['id'];
          }
        } else {
          echo "Welcome!";
        }
      }
      function print_index(){
        for ($i = 0; $i<count($data_dir);$i++){
          if (!in_array($data_dir[$i], array('.', '..'))) {
            echo "<li><a href=\"parameter.php?id=$data_dir[$i]\">$data_dir[$i]</a></li>\n";
          }
        }
      }
      function print_contents(){
        if (isset($_GET['id'])) {
          echo file_get_contents("data/".$_GET['id']);
        } else {
          echo "";
        }
      }
    ?>
  </head>
  <body>
    <h1><a href="parameter.php">Web</a></h1>
    <ol>
    <?php
      print_index()
    ?>
    </ol>
    <h2>
      <?php
        print_title()
      ?>
    </h2>
    <?php
      print_contents()
    ?>
  </body>
</html>
