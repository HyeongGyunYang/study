### 1. 장고 설치 등등

### 2. 프로젝트 만들기

-	프로젝트 생성 : <code>django-admin startproejct project_name</code>
-	장고 서버 실행 : <code>python manage.py runserver</code>

### 3. APP

-	하나의 사이트는 여러 앱으로 구성 : 여러 기능으로 구성
-	앱 만들기 : <code>python manage.py startapp app_name</code>
	-	<a href="myweb/blog">블로그 앱</a>
	-	앱 생성 이후, 장고에 사용한다고 알려줘야 함
		-	setting.py : installed_apps

### 4. 모델 만들기

-	모든 모델 객체는 models.py에서 선언됨
-	<a href="myweb/blog/models.py">블로그 모델</a>
-	db에 post model을 추가하자
	-	모델 생성 : <code>$ python manage.py makemigrations blog</code>
	-	실제 db에 model 추가를 반영 : <code>$ python manage.py migrate blog</code>

### 5. 장고 관리자

-	언어 세팅 : language_code
-	superuser 생성
	-	<code>$ python manage.py createsuperuser</code>
-	관리자 페이지에서 만든 모델 보기
	-	<a href="myweb/blog/admin.py">blog/admin</a>
