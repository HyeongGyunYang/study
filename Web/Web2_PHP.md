### Section 1 Intro

### Section 2 PHP 설치 및 설정

-	환경설정
	-	php.ini
		-	display_errors
		-	opcache
	-	aparche server restart

### Section 3 PHP 원리

-	web browser가 web server에 요청
-	web server에서 php코드를 읽으면 php에 처리 요청
-	php가 처리해서 web server에 반환
-	web server가 web browser의 요청에 응답

### Section 4 PHP 데이터 타입

-	official document

#### Section 4-1. 숫자

-	echo
-	integer : 그냥 쓰면된다
	-	http://php.net/manual/en/language.types.integer.php
-	arithmetic operator : 5칙 연산 +,-,/,%,\*

#### Section 4-2. String

-	http://php.net/language.types.string
-	escape : 따옴표 안에 특수 문자 string 처리 : \\
-	concatenation : .

#### Section 4-3. boolean

-	True, False
-	comparison operators : http://php.net/manual/kr/language.operators.comparison.php
	-	== : equal
	-	=== : identical. equal and same type
	-	!= : unequal
	-	<> : unequal
	-	!== : unidentical
	-	<, >, <=, >=

### Section 5 Variable

-	\$를 앞에 붙여서 사용
-	atom 기능 : lorem

#### Section 5-1. Array

-	array()

### Section 6 URL parameter

-	\$\_GET['parameter']로 받아온다
-	URL : ...php?param1=x&param2=y

### Section 7 Function

-	built in
	-	echo
	-	nl2br() :http://php.net/manual/en/function.nl2br.php
	-	file_get_contents(): http://php.net/manual/en/function.file-get-contents.php
	-	var_dump()
	-	strlen(String) : http://php.net/manual/en/function.strlen.php
	-	isset()
	-	unset()
-	user defined
	-	function fName(args){statement}

### Section 8 제어문

#### Section 8-1. 조건문

-	if (expression) {statement}
-	else {statement}

#### Section 8-2. 반복문

-	while (expression) {statement}
-	do {statement} while (expression)
-	for (expr1; expr2; expr3) {state}
-	foreach (array_expression as $value) {statement}

### Section 9 refactoring

-	require(path);
-	php는 함수 재정의가 안됨
-	require_once(path); 중복 호출 방지
