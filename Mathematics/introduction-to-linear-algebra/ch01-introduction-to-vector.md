- 2 operations : add & combination
- the vectors $`cv`$ lie along a line, when $`w`$ is not on that line, **the combinations $`c\mathbf{v}+d\mathbf{w}`$ fill the whole 2-D plane
  - likewise, n vectors combination -> n-d space

## 1.1 Vectors and Linear Combinations

- Core Idea : $`\mathbf{v}+\mathbf{w}, c\mathbf{v}+d\mathbf{w}`$

### 1.1.1 vector addition

### 1.1.2 scalar multiplication
- zero vector
- linear combination : scalar multiplication + vector addition

### 1.1.3 Vectors in 3-D

- column vector, row vector
  - $`\mathbf{v}=(1,1,-1)`$ 표기 시, 헷갈리지 말 것.

### 1.1.4 The Important Questions

- Adding all $`c\mathbf{v}`$ on one line to all $`d\mathbf{u}`$ on the other line fills in the plane

## 1.2 Lengths and Dot Products

- Core Idea : $`\mathbf{v}\cdot\mathbf{w}, ||\mathbf{v}||=\sqrt{\mathbf{v}\cdot\mathbf{v}}`$
- dot product (inner product) : $`\mathbf{v}\cdot\mathbf{w}=v_1w_1+v_2w_2`$
- 2 vectors are *perpendicular* : dot product is zero
  - zero dot means "the books balance"
- The dot product $`\mathbf{v}\cdot\mathbf{w}`$ equals $`\mathbf{v}\cdot\mathbf{w}`$

### 1.2.1 Lengths and Unit Vectors

- length : $`||\mathbf{v}||=sqrt{\mathbf{v}\cdot\mathbf{v}}`$
  - Core Idea : Pythagoras formula
- unit vector : $`\mathbf{v}\cdot\mathbf{v}=1`$
- $`\mathbf{u} = \mathbf{v}/||\mathbf{v}||`$ : u is a unit vector in the same direction as any vector v

### 1.2.2 The Angle Between Two Vectors

- Right angles : the dot product is $`\mathbf{v}\cdot\mathbf{w}=0`$ when v is perpendicular to w
- less than $`90^{\circ}`$ : dot product >0, positive
- above $`90^{\circ}`$ : dot product <0, negative
- Unit vectors u and U at angle theta have $`\mathbf{u}\cdot\mathbf{U}=\cos\theta, |\mathbf{v}\cdot\mathbf{U}|\le 1`$
- **Cosine Formula** : v and w are nonzero vectors, $`\frac{\mathbf{v}\cdot\mathbf{w}}{||\mathbf{v}|| ||\mathbf{w}||}=\cos\theta`$
  - **Schwarz Inequality** : $`|\mathbf{v}\cdot\mathbf{w}|\le||\mathbf{v}|| ||\mathbf{w}||`$
  - **Triangle Inequality** : $`||\mathbf{v}+\mathbf{w}||\le||\mathbf{v}||+||\mathbf{w}||`$
- $`angle = arccosine(cosine)`$

## 1.3 Matrices

- Core Idea : $`Ax=b`$, solution $`x=A^{-1}b`$
  - but, some matrices don't allow $`A^{-1}`$
- $`Ax = \begin{bmatrix}
1 & 2 \\
3 & 4
\end{bmatrix} \begin{bmatrix}
x_1 \\
x_2
\end{bmatrix}`$
is a combination of the columns
$`Ax = x_1 \begin{bmatrix}
1 \\
3
\end{bmatrix} + x_2 \begin{bmatrix}
2 \\
4
\end{bmatrix}`$

### 1.3.1 Linear Equations

- in $`Ax=b`$, b as known and look for x
- inverse problem
- solution : 연립방정식으로 생각해서 풀기
- Invertible : $`Cx=0, C\not=0, x\not=0`$ - NOT invertible

### 1.3.2 the Inverse matrix

- $`x=A^{-1}b`$를 만족하는 matrix
- the Sums of $`A^{-1}b`$ become the *integral* of $`b(t)`$ : **Sums of differences are like integrals of derivatives**
  - ***integration is the inverse of differentiation***
  - $`\frac{dx}{dt} = b and x(t)=\int_0^t b dt`$
  - **Backward** : $`x(t)-x(t-1)`$ (forward differences, centered difference passs)

### 1.3.3 Cyclic Differences

- Infinitely many solutions
- No solutions
- All linear combinations $`x_1u+x_2v`$ lie on the plane given by $`b_1+b_2+b_3=0`$

### 1.3.4 Independence and Dependence

- Key question is whether the third vector is in that plane
- independence : w is not in the plane of u and v
  - No combination except 0u+0v+0w=0 gives b=0
  - Ax=0 has one solution. A is an invertible matrix
- dependence : w\* is in the plane of u and v
  - Other combinations like u+v+w\* give b=0
  - Cx = 0 has many solutions. C is a sigular matrix
