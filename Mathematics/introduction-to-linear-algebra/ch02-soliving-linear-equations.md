## 2.1 Vectors and Linear Equations

- solve a system of equations
- Rows : show 2 lines meeting at a single point (general solution)
  - we can find x, y
- Columns : combines the column vectors (linear algebra solution)
  - $`x\begin{bmatrix}
  x_1 \\
  x_2
  \end{bmatrix} = y\begin{bmatrix}
  y_1 \\
  y_2
  \end{bmatrix} = b`$
- **Coefficient matrix** : express a system of equations by matrix

### 2.1.1 three equations in three unknowns

### 2.1.2 the matrix form of the equations

- ***Identity matrix*** : exceptional

### 2.1.3 matrix notation

### 2.1.4 programming languages for mathematics and statistics

- '''python
dot(A, x)
'''

## 2.2 the idea of elimination

- ***upper triangular system***
- on coordinate, horizon or vertical

### 2.2.1 breakdown of elimination

- $`0y=a`$ : no solution
  - parallel
- $`0y=0`$ : infinitely many solutions
  - y is free

### 2.2.2 three equations in three unknowns

## 2.3 Elimination Using matrices

- *elimination matrix* : identity matrix + alpha

### 2.3.1 matrix multiplication

- $`EA=\begin{bmatrix}
1 & 0 \\
-2 & 1
\end{bmatrix} \begin{bmatrix}
b_1 \\
b_2
\end{bmatrix} = \begin{bmatrix}
b_1 \\
b_2 - 2b_1
\end{bmatrix}`$

### 2.3.2 The matrix $`P_{ij}`$ for a row exchange

- $`\begin{bmatrix}
0 & 1 \\
1 & 0
\end{bmatrix} \begin{bmatrix}
b_1 \\
b_2
\end{bmatrix} = \begin{bmatrix}
b_2 \\
b_1
\end{bmatrix}`$

## 2.4 rules for matrix operations

- 1st way : the entry in row i and column j of AB is $`(row i of A)\cdot(column j of B)`$
- 2nd way : A times every column of B
- 3rd way : every row of A times matrix B
- 4th way : multiply columns 1 to n of A times rows 1 to n of B. add thoes matrices

### 2.4.3 the laws for matrix operations

- $`AB\not=BA`$
- $`A(B+C)=AB+AC`$
- $`(A+B)C=AC+BC`$
- $`A(BC)=(AB)C`$

### 2.4.4 Block matrices and block multiplication

- block multiplication
- block elimination : $`\begin{bmatrix}
I & 0 \\
-CA^{-1} & I
\end{bmatrix} \begin{bmatrix}
A & B \\
C & D
\end{bmatrix} = \begin{matrix}
A & B \\
0 & D-CA^{-1}B
\end{bmatrix}`$
  - ***Schur complement*** : $`D-CA^{-1B} like d-cb/a`$

## 2.5 inverse matrices

- the inverse exists if and only if elimination produces n pivots
- $`ad-bc`$ is not zero : invertible

### 2.5.1 the inverse of a product AB

- $`(AB)^{-1} = B^{-1}A^{-1}`$
- $`B^{-1}A^{-1}`$ is quick

### 2.5.2 calculating inverse by Gauss-Jordan elimination

### 2.5.3 singular versus invertible

### 2.5.4 recognizing an invertible matrix

- is it invertible? : some case
- **diagonally dominant matrices are invertible** : $`|a_{ii}| > \sum_{j\not=i} |a_{ij}|`$
