## 1. 행렬의 대각화

- 대각행렬 diagonal matrix : 대각성분을 제외한 모든 성분이 0인 행렬
- 대각화 diagonalization : 적절한 기저변환을 통해 주어진 행렬을 대각행렬로 변형하는 것
- 행렬을 대각화하려면 선형사상 $`L_A(v)=\lambda v`$ 가 되는 v와 람다를 구해야 한다.
  - $`Av = \lambda v`$를 풀어야 한다.
- 고유벡터 eigenvector : v, 0일 수 없다.
- 고유값 eigenvalue : 람다, 0이 될 수 있다.
- 정리 : 고유값 $`\lambda_1 \cdots lambda_k`$가 모두 다르다면, 고유벡터 $`v_1 \cdots v_k`$는 일차독립이다.
- 고유값, 고유벡터 구하기
  - 방정식을 직접 풀어 구할 수 있다.
  - 특성방정식 chracteristic equation $`\det(\lambda-A)=0`$을 풀어서 고유값을 먼저 구할 수 있다.
    - 여기서 좌항을 특성다항식 characteristic polynomial 이라고 한다.

## 2. 대각화 가능성

- complex vector space에서도 대각화가 가능하다.
- 고유공간 eigenspace
