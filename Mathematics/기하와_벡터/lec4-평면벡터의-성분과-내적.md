## 1. 위치벡터

- 위치벡터 : 평면에서 정해진 점 O를 시점으로 하는 벡터 $`\overrightarrow{OA}`$를 점 A의 위치벡터라고 한다.
- 위치벡터의 덧셈, 뺄셈
- 위치벡터의 활용
  - 평면 위의 벡터의 위치벡터로의 표현
  - m:n 내분점 : $`\frac{m\overrightarrow{b}+n\overrightarrow{a}}{m+n}`$
  - 외분점 : $`\frac{m\overrightarrow{b}-n\overrightarrow{a}}{m-n}`$
  - 무게중심 : $`\frac{\overrightarrow{a}+\overrightarrow{b}+\overrightarrow{c}}{3}`$

## 2. 평면벡터의 성분

- 벡터의 성분 : 위치벡터의 x,y좌표
- norm : $`\sqrt{a^2_1+a^2_2}`$
- 두 벡터가 같을 조건 : 성분이 같음
- 덧셈 : 성분 덧셈
- 뺄셈 : 성분 뺄셈
- 실수배: 성분 실수배

## 3. 벡터의 내적

- 두 벡터의 내적 : $`\overrightarrow{a}\cdot\overrightarrow{b} = |\overrightarrow{a}|\cdot|\overrightarrow{b}|\cos\theta`$
  - $`\overrightarrow{a}\cdot\overrightarrow{b} = a^2_1b^2_1+a^2_2b^2_2`$
- 교환법칙, 분배법칙, 실수배 분배/결합 성립

## 4. 내적의 활용

- 곱셈법칙 성립
  - a와 a의 내적은 노름제곱
  - $`|\overrightarrow{a}+\overrightarrow{b}|^2=|\overrightarrow{a}|^2+ 2\overrightarrow{a}\cdot\overrightarrow{b} +|\overrightarrow{b}|^2`$
  - $`|\overrightarrow{a}-\overrightarrow{b}|^2=|\overrightarrow{a}|^2- 2\overrightarrow{a}\cdot\overrightarrow{b} +|\overrightarrow{b}|^2`$
  - 등등
- 각의 크기
  - $`\cos\theta = \frac{\overrightarrow{a}\cdot\overrightarrow{b}}{|\overrightarrow{a}|\cdot|\overrightarrow{b}|}`$
  - 성분으로도 표현 가능
- 두 벡터가 직교하면 -> 두 벡터의 내적은 영벡터
- 두 벡터가 평행하면 -> 두 벡터의 내적은 두 벡터의 놂의 곱과 같다 (+-)
- 이 모든 건 성분으로도 표현 가능하다
