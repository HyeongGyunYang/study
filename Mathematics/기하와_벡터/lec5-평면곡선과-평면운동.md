## 1. 직선의 방정식

- 방향벡터 $`\overrightarrow{u}`$ : $`\overrightarrow{A B} = t\overrightarrow{u}`$일 때
- 법선벡터 $`\overrightarrow{n}`$ : $`\overrightarrow{A B}\cdot\overrightarrow{n}=0`$
  - AP에 대해 수직
- 직선의 방정식
  - $`\frac{x-x_1}{a} = \frac{y-y_1}{b}`$
    - 조건 : $`A(x_1, y_1), \overrightarrow{u}=(a,b)`$
  - $`\frac{x-x_1}{x_2-x_1} = \frac{y-y_1}{y_2-y_1}`$
    - 조건 : $`A(x_1, y_1), B(x_2, y_2)`$
  - $`a(x-x_1)+b(y-y_1)=0`$
    - 조건 : $`A(x_1, y_1), \overrightarrow{n}=(a,b)`$
- 두 직선의 방향벡터에 대해 코사인 : $`\cos\theta = \frac{|\overrightarrow{u_1}\cdot\overrightarrow{u_2}|}{|\overrightarrow{u_1}||\overrightarrow{u_2}}`$

## 2. 원의 방정식

- 중심이 $`C(x_1, y_1)`$이고 P는 원 위의 점
- $`r = |\overrightarrow{C P}|`$
- 원의 방정식 : $`(x-x_1,y-y_1)\cdot(x-x_1,y-y_1)=r^2`$

## 3. 평면 위를 움직이는 점의 속도와 가속도

- 속도 : $`\overrightarrow{v}=(\frac{dx}{dt}, \frac{dy}{dt})=(f'(t),g'(t))`$
- 속력 : $`|\overrightarrow{v}|=\sqrt{(\frac{dx}{dt})^2+(\frac{dy}{dt})^2}=\sqrt{\{f'(t)\}^2\{g'(t)\}^2}`$
- 가속도 : $`\overrightarrow{a} = (\frac{d^2x}{dt^2}, \frac{d^2y}{dt^2})=(f''(t),g''(t))`$
- 가속도의 놂 : $`|\overrightarrow{a}| = \sqrt{(\frac{d^2x}{dt^2})^2+ (\frac{d^2y}{dt^2})^2}=\sqrt{(f''(t))^2+(g''(t))^2}`$

## 4. 평면 위에서 점이 움직인 거리

- $`P(x,y), t=a`$에서 위치가 $`x=f(t), y=g(t)`$일 때,
- $`t=b`$까지 이동거리 : $`s = \int_a^b|\overrightarrow{v}dt
  = int_a^b\sqrt{(\frac{dx}{dt})^2+(\frac{dy}{dt})^2}dt
  = \int_a^b\sqrt{\{f'(t)\}^2\{g'(t)\}^2}`$

## 5. 곡선의 길이

- 매개변수 t로 나타낸 곡선 x,y가 겹치는 부분이 없을 때
