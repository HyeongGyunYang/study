## 1. 지수함수의 극한

- inf, 점근

## 2. 로그함수의 극한

- inf, -inf

## 3. 무리수 $`e`$의 정의

- $`\lim_{x\rightarrow 0} (1+x)^{\frac{1}{x}}=e`$

## 4. 자연로그의 정의

- $`\log_e x = \ln x`$
- $`y=e^x`$와 역함 관계
- $`e^0 =1, e^`=e, \ln 1=0, ln e=1$

## 5. e의 정의를 이용한 함수의 극한

- $`\lim_{x \rightarrow 0} \frac{\ln (1+x)}{x} = 1`$
- $`\lim_{x \rightarrow 0} \frac{\log_a (1+x)}{x} = \frac{1}{\ln a}`$
- $`\lim_{x \rightarrow 0} \frac{e^x-1}{x} = 1`$
- $`\lim_{x \rightarrow 0} \frac{a^x-1}{x} = ln a`$

## 6. 지수함수의 도함수

- $`y=e^x \rightarrow y'=e^x`$
- $`y=a^x \rightarrow y'=a^x\ln a`$

## 7. 로그함수의 도함수

- $`y=\ln x \rightarrow y'=\frac{1}{x}`$
- $`y=\log_a x \rightarrow y'=\frac{1}{x\ln a}`$
