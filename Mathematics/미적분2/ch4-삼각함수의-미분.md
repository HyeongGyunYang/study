## 1. 사인함수와 코사인함수의 덧셈정리

- $`\sin(x+-y)=\sin x \cos y+-\cos x \sin y`$
- $`\cos(x+-y)=\cos x \cos y +- \sin x \sin y`$

## 2. 탄젠트함수의 덧셈정리

- $`\tan(x+-y) = \frac{\tan x +-\tan y}{1+-\tan x\tan y}`$

## 3. 삼각함수의 극한

- $`\lim_{x\rightarrow 0} \frac{\sin x}{x} = 1`$

## 4. 삼각함수의 미분

- $`(\sin x)'=\cos x, (\cos x)'=-\sin x`$
