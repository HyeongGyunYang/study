## 1. 적분

## 2. 지수함수의 적분

- $`\int e^x dx = e^x+C`$
- $`\int a^x dx = \frac {a^x}{\ln a}+ C`$

## 3. 삼각함수의 적분

- $`\int \sin xdx = -\cos x +C`$
- $`\int\cos xdx=\sin x +C`$
- $`\int\sec^2 xdx=\tan x+C`$
- $`\int\csc^2xdx=-\cot x+C`$

## 4. 치환적분법

- $`\int f(g(x))g'(x)dx=\int f(t)dt`$

## 5. 부분적분법

- $`\int f(x)g'(x)dx=f(x)g(x)-\int f'(x)g(x)dx`$
- $`\int_a^b f(x)g'(x)dx = [f(x)g(x)]_a^b -\int_a^b f'(x)g(x)dx`$

## 6. 정적분으로 표시된 함수의 미분과 극한

- $`\frac{d}{dx}\int_a^x f(t)dt = f(x)`$
- $`\frac{d}{dx}\int_x^{x+a}f(t)dt=f(x+a)-f(x)`$
- $`\lim_{x\to 0}\frac{1}{x-a}\int_a^xf(t)dt=f(a)`$
- $`\lim_{x\to 0}\frac{1}{x}\int_a^{a+x}f(t)dt=f(a)`$

## 7. 정적분과 급수

- $`\lim_{n\to&infin;}\sum_{k=1}^nf(a+\frac{b-a}{n}k)\frac{b-a}{n}=\int_a^bf(x)dx=\int_0^{b-a}f(x+a)dx=(b-a)\int_0^1f((b-a)x+a)dx`$
