## 1. 함수의 몫의 미분법

- $`y=\frac{1}{g(x)}, y'=-\fac{g'(x)}{\{g(x)\}^2}`$
- $`y=\frac{f(x)}{g(x)}, y'=\frac{f'(x)g(x)-f(x)g'(x)}{\{g(x)\}^2}`$

## 2. 여러 가지 삼각함수의 도함수

- $`\tan x : \sec^2 x`$
- $`\sec x : \sec x \tan x`$
- $`\csc x : -\csc x \cot x`$
- $`\cot x : -\csc^2 x`$

## 3. 합성함수의 미분법

- $`y=f(u), u=g(x), y=f(g(x)), \frac{dy}{dx} = \frac{dy}{du} \times \frac{du}{dx}, y'=f'(g(x))g'(x)`$

## 4. 지수함수와 로그함수의 도함수

- $`y=a^x, y'=a^x\ln a`$
- $`y=\ln|x|, y'=\frac{1}{x}`$
- $`y=\log_a |x|, y'=\frac{1}{x\ln a}`$

## 5. 함수 $`y=x^a`$의 도함수

## 6. 역함수의 미분법

- g가 f의 역함수, $`g'(x)=\frac{1}{f'(g(x))}`$

## 7. 이계도함수
