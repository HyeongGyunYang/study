## 1. 일반각과 호도법

- 일반각
- 호도법
  - 1라디안 : 호의 길이가 반지름인 각

## 2. 삼각함수

- $`\sin\theta = \frac{y}{r}, \cos\theta = \frac{x}{r}, \tan\theta = \frac{y}{x}, \csc\theta = \frac{r}{y}, \sec\theta = \frac{r}{x}, \cot\theta = \frac{x}{y}`$
- 삼각함수 값의 부호
- 삼각함수 사이의 관계
  - $`\tan\theta = \frac{\sin\theta}{\cos\theta}, \cot\theta = \frac{\cos\theta}{\sin\theta}`$
  - $`\sin^2\theta+\cos^2\theta = 1`$
  - $`1+\tan^2\theta = \sec^2\theta, 1+\cot^2\theta = \csc^2\theta`$

## 3. 삼각함수의 그래프

- $`\sin(-x)=-\sin x, \sin(2n\pi+x)=\sin x`$
- $`\cos(-x)=\cos x`$

## 4. 삼각함수의 성질

## 5. 삼각함수의 활용
