### 1. 기본 연산자 오버로딩

-	구문적 유혹 : 사람들이 선호하는 약간 다른 형식의 구문
	-	+(1,3) 보다 1+3을 더 좋아함

#### 1-1. 오버로딩의 기본

-	연산자를 오버로딩하면 연산자는 클래스형을 인자로 사용할 수 있다.

#### 1-2. 팁: 생성자는 객체를 리턴한다

-	생성자는 void가 아니다!
-	생성자는 특별한 목적을 가지고 있는 특수 함수.
-	생성자는 "객체"를 리턴한다.

#### 1-3. const 값 리턴

-	상수값 리턴을 하면, 리턴되는 객체를 변경할 수 없다.
-	<code>m3 = m1+m2;</code>에서 m3와 m1+m2는 다르다!!
	-	<code>n=5;</code>
-	클래스형 리턴을 const 리턴하는 것은 좋은 규칙이다.

#### 1-4. 팁: 클래스형의 멤버 변수 리턴

-	우회적인 방법으로 멤버 변수의 값에 접근하는 것을 차단할 수 있다.

#### 1-5. 단항 연산자 오버로딩

-	-, --, ++

#### 1-6. 멤버 함수로서의 오버로딩

-	뭔소리야..

#### 1-7. 팁: 클래스는 모든 자신의 객체에 접근한다

#### 1-8. 함수 호출 연산자 () 오버로딩

#### 1-9. 함정 : &&, || 그리고 콤마 연산자 오버로딩

### 2. 프렌드 함수와 자동 형 변환

-	멤버 함수가 아니지만, 멤버 함수의 특권을 모두 가지고 있다.

#### 2-1. 자동 형 변환에 대한 생성자

#### 2-2. 함정: 멤버 연산자와 자동 형 변환

-	멤버 연산자로 오버로딩 된 경우
	-	이항 연산자를 멤버 연산자로서 오버로딩할 때, 두 개의 인자는 더 이상 대칭적이지 않다. 하나는 호출 객체, 나머지 하나는 인자
	-	<code>fullAmout = 25+baseAmount;</code> 불가능 : 25는 객체로서 멤버를 호출하지 못함
-	프렌드 함수로서 독립적으로 연산자가 오버로딩 된 경우는 가능

#### 2-3. 프렌드 함수

-	만드는 법 : 클래스 정의에서 프렌드 함수로 명명
-	반대측
	-	oop에서 모든 함수와 연산자는 멤버 함수여야한다.
-	찬성측
	-	프렌드로서의 연산자 오버로딩은 모든 인자에 대하여 자동 형 변환의 실용적인 장점을 제공
	-	연산자 선언이 클래스 정의 내에서 이루어지기 때문에 클래스의 멤버가 아니고 프렌드가 아닌 방법으로 연산자를 오버로딩하는 방법보다 최소한의 캡슐화를 제공한다.

#### 2-4. 프렌드 클래스

-	보통 상호 참조한다
-	전위 선언 : 나중에 정의되는 클래스를 먼저 정의되는 클래스에서 상호 참조하기 위해서 먼저 정의되는 클래스 앞에 선언 <code>class F;</code>

#### 2-5. 함정: 프렌드가 없는 컴파일러

-	컴파일러에 따라 프렌드 함수가 제대로 동작하지 않는다.
	-	...?? 그냥 쓰지 말라는 거 아니냐
-	대처법
	-	accessor, mutator를 같이 사용
	-	멤버로서 연산자 오버로딩

#### 2-6. 연산자 오버로딩 규칙

-	pp341.

### 3. 참조와 다른 오버로딩 연산자

-	어.. 별로 안 궁금한데

#### 3-1. 참조

#### 3-2. 함정: 멤버 변수를 참조 리턴하기

#### 3-3. >>와 \<<의 오버로딩

#### 3-4. 팁: 어떤 형태의 리턴 값을 사용해야 하는가

#### 3-5. 할당 연산자

#### 3-6. 증/감 연산자의 오버로딩

#### 3-7. 배열 연산자 []의 오버로딩

#### 3-8. L-Value vs R-Value 기반의 오버로딩
