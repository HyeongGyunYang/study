### 1. C++ 개요

#### 1-1. C++ 언어의 기원

#### 1-2. C++과 객체지향 프로그래밍

#### 1-3. C++ 특성

#### 1-4. C++ 용어

-	함수 : cpp에서 프로시저와 연관 있는 존재들은 모두 함수라 호칭
	-	타 언어에서 프로시저, 메서드, 함수, 서브프로그램 등등
-	cpp 프로그램 : main으로 호칭되는 함수

#### 1-5. 간단한 c++ 프로그램 예제

### 2. 변수, 식 및 할당문

#### 2-1. 식별자

-	식별자 : 변수의 이름
-	대소문자를 구별함
-	명명 규칙 : 소문자로 시작, 이어지는 단어의 첫글자 대문자
-	일부 컴파일러에서는 너무 긴 식별자 이름이 무시될 수도 있다.
-	키워드, 예약어
-	키워드가 아니더라도 사전에 정의된 문자들을 키워드처럼 취급하는 게 쉽고 안전하다.

#### 2-2. 변수

-	모든 변수는 사용되기 전에 반드시 선언되어야 한다.
	-	cpp는 선언과 정의 개념을 구분한다.
-	short, int, long
	-	unsigned short, unsigned int, unsigned long
-	float, double, long double
-	char
-	bool

#### 2-3. 할당문

-	변수의 값을 가장 직접적으로 변경하는 방법
-	<code>구문 = 식;</code>
-	할당문은 식을 사용하여 표현될 수 있다.
	-	식을 사용할 때, 할당문은 변수에 할당된 값을 리턴
	-	<code>n = (m=2)</code>
		-	m에 2를 할당함과 동시에 2를 리턴하여 n에 2를 할당하고 있다.
	-	할당문을 식으로 표현하지 말 것을 권고
	-	일부 에러를 판별하기 위해 사용
-	<code>n=m=2</code>

#### 2-4. 함정: 초기화되지 않은 변수

-	선언만 하고 정의되지 않은 변수는 아무 의미가 없다. : 초기화되지 않은 변수
-	선언하면서 할당 :
	-	<code>int minimumNumber = 3;</code>
	-	<code>double rate(0.07), time, balance(0.00);</code>

#### 2-5. 팁: 변수 이름 설정하기

-	명명규칙: 이름을 부여 받은 물체의 의미를 반영하는 것이 좋다.

#### 2-6. 추가 할당문

-	<code>count += 2;</code>
-	<code>count = count+2;</code>

#### 2-7. 할당 호환성

-	특정한 형에 속하는 변수의 값을 다른 종류의 형에 속하는 변수의 값으로 저장할 수 없다.
-	컴파일러가 제공하더라도, 프로그래머는 위와 같은 프로그램을 작성하지 말아야 한다.
-	예외
	-	int를 double에 저장하는 경우는 가끔 있다.
	-	char <-> int <-> bool: 바람직하진 않다.
	-	bool : 0이 아닌 정수는 true, 0은 false

#### 2-8. 문자상수(리터럴)

-	constant : 하나의 특정한 값을 가지는 경우
-	값이 변경될 수 없다.
-	double의 과학용 표기 : <code>3.49e-2</code>
-	literal: " "
	-	그냥 char는 ' '

#### 2-9. 백슬래스 문자상수

-	escape sequence
-	ANSI/ISO 표준안에 정의되지 않은 escape을 사용하지 말자

#### 2-10. 상수 이름 짓기

-	<code>const nt branchCount=10;</code>
	-	일단 초기화된 변수의 값을 변경하지 못하도록 한다.
-	변경자 : 변수의 특성을 변경시킨다. const 등
-	coding standard : 각각 다른 줄에서 변수를 선언하는 경우가 더 의미 전달이 잘된다.

#### 2-11. 산술 연산자 및 식

-	+, -, \*, /, %
-	전부 int면 int, double이 섞이면 double
-	coding standard : 연산자의 우선규칙이 있지만, 괄호로 우선순위를 명시하는 게 더 좋다.

#### 2-12. 정수형 및 부동 소수점형 변수 나눗셈

-	int / int는 가우스다
-	피연산자가 음수이면 컴파일러마다 결과가 다르다 (아니;; cpp 왜 이따구임?)

#### 2-13. 함정 : 정수형 변수의 나눗셈

#### 2-14. 형 변환

-	주로 사용되는 방식
	-	<code>double ans = n/static_cast<double>(m)</code>
-	4가지의 방식

	<pre><code>
	cost_cast<Type>(Expression)
	dynamic_cast<Type>(Expression)
	reinterpre_cast<Type>(Expression)
	</code></pre>

-	고전 표기법들

	-	<code>int(9.3)</code>
	-	<code>(double)42</code>

-	강제 형변환 : <code>double d = 5;</code>

#### 2-15. 증가 및 감소 연산자

-	<code>n++, m--</code>
-	v++ : 변수 v의 값을 먼저 사용하고 그 다음에 v의 값이 하나 증가한다
-	++v: 변수 n의 값이 증가된 다음에 이 값을 리턴한다.
-	단일 변수에만 적용된다.

#### 2-16. 함정: 평가 순서

-	대부분의 연산자들에 대하여 식들에 대한 평가 순서는 보장되지 않는다.
-	<code>n + (++n)</code>
	-	기존 n이 2라고 할 때, 5 또는 6을 리턴 받는다.
-	&&, ||은 왼 -> 오 순으로 평가되긴 한다.
-	쨌든, 증가/감소 연산자를 전체 식의 일부로 사용하는 건 지양

### 3. 콘솔 입/출력

-	iostream library 사용

	<pre><code>
	#include \<iostream\>
	using namespace std;
	</code></pre>

#### 3-1. cout을 이용한 출력

<pre><code>
cout << "Hello reader.\n";
     << "Welcom to C++. \n";

cout << numberOfGames << "games played.";

cout << "The total cost is $" << (price+tax);
</code></pre>

-	cout를 따로 따로 명시할 필요가 없다.
-	산술식이 포함될 수 있다.
-	\<< : 삽입연산자

#### 3-2. 새로운 줄에서의 출력

-	개행문자

<pre><code>
cout << "\n";
cout << endl;
</code></pre>

#### 3-3. 팁: \n 또는 endl을 이용한 문장 종료

-	모든 프로그램의 종료 부분에 개행 문자를 사용하는 것은 좋은 프로그래밍 습관
	-	출력문의 마지막 부분이 문자열인 경우는 문자열의 끝부분에 "\n"을 사용
	-	아니면 endl
-	일부 컴파일러들의 경우, 프로그램 종료 부분에 개행 문자가 명시되지 않으면 마지막 줄이 출력되지 않기 때문이다.
-	이전 프로그램의 마지막 출력과 다음 프로그램의 첫 출력이 혼합되어 표현되는 것을 막는다.

#### 3-4. 소수점을 포함하는 숫자 형식 설정

-	12장에서 자세히
-	소숫점 2자리까지 표현

	<pre><code>
	cout.setf(ios::fixed);
	cout.setf(ios::showpoint);
	cout.precision(2);
	</code></pre>

#### 3-5. cerr 출력문

-	표준 에러 출력 문자열을 출력하는데 주로 사용
-	기능상 차이는 없어 보인다.

#### 3-6. cin 객체를 이용한 입력

<pre><code>
cin >> numberOfLanguages;

cin >> dragons >> trolls;
</code></pre>

#### 3-7. 팁: 입/출력에서의 라인 break

### 4. 프로그램 스타일

-	문자상수는 상수로 정의
-	변수 이름을 잘 정의하고, 문장에 적절한 들여쓰기를 사용
-	그래도 의미전달이 불분명하면 주석

#### 4-1. 주석

<pre><code>
//
/*
\*/
</code></pre>

### 5. 라이브러리 및 네임스페이스

-	library들은 네임스페이스에 정의되어 있다.
-	네임스페이스는 뒤에서 다룬다.

#### 5-1. 라이브러리 및 inlude 지시자

-	<code>#include \<Library_Name\></code>
-	#전후와 <>안에 공백을 삽입하지 말자

#### 5-2. 네임스페이스

-	이름을 정의한 집합체
-	동일한 함수 이름일지라도 서로 다른 네임스페이스에서는 다르게 정의될 수 있다.
-	여기서는 모든 표준 라이브러리에 대한 정의문을 std 네임스페이스에 포함시킬 것이다.
-	<code>using namespace std;</code>
-	그 중 일부만 사용하고 싶은 경우 : <code>using std::cin;</code>

#### 5-3. 함정 : 라이브러리 이름과 관련된 문제점

-	예전 표준안을 따르는 컴파일러는 <code>#include <iostram.h></code> 이런식으로 라이브러리 명을 표기한다.

### 6. 프로그래밍 프로젝트

-	<a href="http://www.mycodemate.com">마이 코드 메이트</a>
