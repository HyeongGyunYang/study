### 1. 부울 식

#### 1-1. 부울 식의 구성

-	&&, ||, !

#### 1-2. 함정: 부등식의 연결

-	부등식을 연결해서 쓰지 마라

#### 1-3. 부울 식의 평가

#### 1-4. 우선순위 규칙

#### 1-5. 함정: 정수값도 부울값으로 사용될 수 있다.

### 2. 분기 메커니즘

#### 2-1. if-else 문

<pre><code>
if (hours > 40) {
  grossPay = rate * 40;
} else {
  grossPay = rate * hours;
}
</code></pre>

#### 2-2. 복합문

#### 2-3. 함정: ==의 위치에 = 사용하기

-	<code>x=12</code>이런 식으로 쓰면 12를 반환해서 무조건 참이 된다.

#### 2-4. else의 생략

#### 2-5. 내포된 문장

#### 2-6. 다중 if-else문

#### 2-7. switch문

-	cpp에서 유일한 다중분기문
-	제어식의 반환 값은 bool, enum, int, char
-	case에 명시된 상수는 유일해야 한다.
-	break이 없으면 하나의 case가 두 개의 레이블을 가지는 것과 같은 효과를 가진다.
-	default 생략 가능

<pre><code>
switch (Controlling_Expression){
  case Constant_1:
    Statement_Sequence_1;
    break;
  default:
    Default_Statement_Sequence;
}
</code></pre>

#### 2-8. 함정: switch문에서 break문을 실수로 생략한 경우

-	이어서 다음 케이스를 실행한다.

#### 2-9. 팁: 메뉴 선택을 위한 switch문 사용하기

#### 2-10. 나열형 enumeration type

-	임의의 값을 가지는 다수의 상수를 정의할 수 있다. 둘 이상의 상수가 같은 정수를 가질 수 있다.
-	수를 명시하지 않으면, 나열형의 식별자들에 0부터 연속된 정수를 부여한다.
-	<code>enum MyEnum {ONE = 17, TWO, THREE, FOUR=-3, FIVE}</code>
	-	17, 18, 19, -3, -2

#### 2-11. 조건 연산자 / 3항 연산자 / 산술 if

-	<code>max = (n1>n2) ? n1 : n2;</code>
-	조건 ? true : false

### 3. 순환

#### 3-1. while문과 do-while문

<pre><code>
while (Boolean_Expression) {
  Statement;
}

do {
  Statement;
} while (Boolean_Expression);
</code></pre>

#### 3-2. 증감 연산자의 리뷰

#### 3-3. 콤마 연산자

-	여러 식의 리스트를 평가하고, 마지막 식의 값을 반환한다.
-	권장하지는 않지만, 언제 어디서나 사용해도 괜찮다
-	for문에서 편리하게 사용되기도

#### 3-4. for문

<pre><code>
for (Initialization; Boolean; Update){
  Statement;
}

for (sum=0, n=1; n<=10;sum=sum+n,n++){
  Statement;
}
</code></pre>

-	for문 안에서 선언된 변수를 처리하는 방식은 컴파일러마다 다르니, 3장을 참고하자

#### 3-5. 팁: N번 반복 실행하는 루프

#### 3-6. 함정: for문에서 불필요한 세미콜론

-	아무것도 없는 ;은 empty문, null문 : 아무것도 수행하지 않는 문장

#### 3-7. 함정: 무한루프

#### 3-8. break문과 continue문

#### 3-9. 중첩된 순환문
