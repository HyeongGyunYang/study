### 1. 사전 정의 함수

#### 1-1. 값을 리턴하는 사전 정의 함수

-	한 개의 값을 리턴
-	argument
	-	형이 있다.
-	fuction call / function invocation
-	library
	-	include indicator
	-	using indicator
	-	몇몇 시스템 라이브러리의 경우,
	-	컴파일러에 추가로 명령을 제공해야 한다.
	-	링커를 명시적으로 실행시켜야 한다.

#### 1-2. 사전에 정의된 void 함수 : 값을 리턴하지 않는 함수

#### 1-3. 난수 생성기

<pre><code>
#include <\cstdlib>
#include <\iostream>
using namespace std;

srand(99);
cout << rand();

cout << (RAND_MAX - rand())/static_cast<double>(RAND_MAX);
</code></pre>

-	pseudo random number

### 2. 사용자 정의 함수

-	일단 main과 같은 파일에 함수가 정의되어 있다고 생각하고 정의

#### 2-1. 리턴 값이 있는 함수의 정의

-	첫번째 : function declaration / function prototype
	-	formal parameter / parameter
-	두번째 : function definition

<pre><code>
double func_name(int param1, double param2){
   statement;
 }
</code></pre>

#### 2-2. 여러 가지 함수 선언 형태

<code>double foo(int, double){}</code>

#### 2-3. 함정 : 순서가 잘못된 인자들

#### 2-4. 함정 : 매개변수와 인자 용어의 사용

#### 2-5. 함수를 호출하는 함수

-	유일한 제약사항은 main()에서 함수를 호출하기 전에 함수의 선언이나 함수의 정의가 나와야 한다.

#### 2-6. 예: 반올림 함수

#### 2-7. 부울 값을 리턴하는 함수

#### 2-8. void 함수의 정의

#### 2-9. void 함수의 return문

-	함수 종료의 역할

#### 2-10. 선행조건과 사후조건

-	주석을 잘 작성하는 법.
-	선행조건은 precondition : 함수가 호출될 때 참이라고 가정하는 것이 무엇인지 제시
-	사후조건은 postcondition: 선행조건을 만족시킨 상황에서 함수가 실행된 후 참이 되어야할 것이 무엇인지를 말해준다.
	-	사후조건이 리턴값에 대한 설명뿐이라면, 사후조건이란 단어를 생략하고 무엇을 리턴하는지에 대해서만 서술한다.

#### 2-11. main은 함수이다

-	표준안에는 return이 생략 가능하다
-	하지만, 많은 컴파일러들이 return 선언을 강제한다.
	-	<code>return 0;</code>
-	코드에 main 호출을 포함시키는 것은 절대 안된다.
	-	main은 오직 시스템만 호출할 수 있다.
	-	프로그램을 실행시키면 시스템이 main을 호출한다.

#### 2-12. 재귀적 함수

-	main을 재귀적으로 호출하면 안된다

### 3. 영역 규칙

-	함수는 다른 함수들과 또는 같은 문제를 다루는 어떤 다른 코드와 서로 충돌하지 않는 스스로 완비된 구성 단위여야 한다.

#### 3-1. 지역 변수

#### 3-2. 프로시저의 추상화

-	프로그램을 사용하는 사람은 이 프로그램이 어떻게 코드화되어 있는지를 자세히 알 필요가 없어야 한다.
-	프로그램이 하는 일이 무엇인가를 알아야겠지만, 어떻게 동작하는지는 알 필요가 없다.
-	함수는 작은 프로그램이다!
-	(프로시저가 함수보다 전산학에서 더 일반적인 용어이다.)

#### 3-3. 전역 상수와 전역 변수

-	가독성을 위해서는 include를 모아서, global을 모아서, func_declaration을 모아서 하자
-	그리고 전역변수는 별로다

#### 3-4. 블록 {}

#### 3-5. 중첩된 영역

#### 3-6. 팁: 분기문과 루프문 안에서 함수 호출

#### 3-7. for 루프에 선언된 변수

-	표준안에 따르면, for loop의 init부분의 선언문이 loop본체에 locally
-	근데 컴파일러마다 다르다
-	컴파일러마다 다를 때는, 이식성을 고려해서 코드를 작성해야 한다.
