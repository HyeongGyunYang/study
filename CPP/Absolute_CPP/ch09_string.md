### 1. 스트링을 위한 배열 형식

-	c-string : c에서 계승된 고전적 스트링 표현방식
-	문자형 배열에 문자열을 저장하고, 배열의 끝 부분에 널 문자 '\0'를 삽입하는 방식

#### 1-1. C-string 값 및 C-string 변수

-	<code>char s[10];</code> 최대 9글자를 담을 수 있는 c-string
-	배열의 값을 이용하는 과정에서 일반 배열과 차이가 존재
-	초기화
	-	위의 두가지는 c-string 방식으로 초기화
	-	세번째는 뒤에 널문자를 저장하지 않는다.

<pre><code>
#include < cstring>

char shortString[] = "abc";
char shortString[4] = "abc";
char shortString[] = {'a','b','c'};
</code></pre>

#### 1-2. 함정 : C-string에 대한 = 및 == 연산자 사용

-	=을 이용한 할당문 불가
	-	변수의 값을 초기화하는 방식으로 이용된다.
-	==을 이용한 경우 부적절한 연산 결과
-	strcpy 함수 : c-string에 문자열 할당하기
	-	<code>strcpy(aString, "Hello");</code>
	-	길이를 자동으로 조절해주지 않음
	-	세번째 인자에 문자열의 최대 크기를 넣을 수 있다.
-	strcmp 함수 : 두 c-string의 동일성 여부 확인하기
	-	<code>strcmp(cString1, cString2);</code>
	-	동일하지 않으면 true
	-	두 c-string의 문자가 갖는 정수값의 차이를 리턴하기 때문
	-	세번째 인자에 비교할 최대 문자 개수를 넣을 수 있다.
-	둘 다 전역 네임스페이스에 선언되어 있음

#### 1-3. < cstring>의 다른 함수들

-	cstring 라이브러리는 전역 네임스페이스에 정의되어 있다.
-	pp 376.에서 자주 사용하는 함수들을 확인

#### 1-4. 예: 명령행 인자

-	main 함수에서 명령행 인자 사용하기
-	<code>int main(int argc, char * argv[])</code>
	-	argc는 프로그램에 주어진 인자의 수를 명시
	-	argv는 0부터 차례로 프로그램의 이름, 첫번째 매개변수 ...

#### 1-5. C-string 입/출력

-	cin >>으로 입력하면 공백들은 입력과정에서 생략되고, 공백을 만나면 입력 과정이 중단 됨.
-	getline 함수
	-	<code>cin.getline(a, 80);</code>
	-	a는 c-string, 80은 최대 입력 글자 수.
	-	한 라인의 내용이 모두 입력된다.

### 2. 문자 조작 도구

#### 2-1. 문자 입/출력

#### 2-2. get 및 put 멤버 함수

-	cin.get() : 하나의 문자를 입력하여 char형 변수에 저장할 수 있다.
	-	공백이나 개행문자도 입력받는다.
-	cout.put(): cout \<< 형식으로 사용할 수 없는 곳에는 사용할 수 없다.

#### 2-3. 예: newline 함수를 이용한 입력 확인

#### 2-4. 함정: 예상치 않은 '\n' 입력

#### 2-5. putback, peek 및 ignore 멤버 함수

-	cin.putback()
	-	인자의 값이 입력 스트림에 삽입되어, 다음으로 입력될 문자가 된다.
	-	char형 한 개의 인자
-	cin.peek()
	-	cin에 의해 입력될 다음 문자를 리턴하지만, 입력 스트림에서 문자를 제거하지 않는다.
-	cin.ignore()
	-	특정 문자에 대한 입력을 생략하는 경우

#### 2-6. 문자 조작 함수들

-	toupper, tolower, isupper, islower, isalpha, isdigit, isalnum, isspace, ispunct, isprint, isgraph, isctrl

#### 2-7. 함정: toupper 및 tolower 함수는 int형 값을 리턴한다.

### 3. 표준 스트링 클래스

#### 3-1. 표준 스트링 클래스 소개

<pre><code>
#include < string>
using namespace std;

string s1, s2;
string s3 = s1 + s2;
</code></pre>

-	s3가 s1+s2를 나타내기에 작으면 저장공간이 자동으로 증가한다.

#### 3-2. 스트링 클래스를 이용한 입/출력

<pre><code>
getline(cin, line);
</code></pre>

-	newline은 읽지 않음

#### 3-3. 팁: getline 함수에 대하여 좀 더 알아보자

-	getline(cin, line, '?');
-	cin객체에 대한 참조를 line에 리턴한다.
-	'?'를 만나면 수행 종료

#### 3-4. 함정: cin >>을 통한 입력과 getline 함수를 통한 입력 혼합

#### 3-5. 스트링 클래스를 이용한 스트링 처리

-	pp 408. string member functions

#### 3-6. 예: 회문 테스트

#### 3-7. 스트링 객체와 C-string의 상호 변환
